<?php include 'includes/session.php'; ?>    
<html lang="en">
	<!--<![endif]-->
<head>
            <meta charset="utf-8">
            <title>Contact Us  |  Vibrant Education</title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
              <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				
				<!-- breadcrumb end -->
				
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-9" >
                                             <div class="timeline clearfix" id="blog_details_client">
								
					     </div>
                                         </div>
                                     <div class="col-md-3" style="position:relative;top:80px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Categories</h3> 
                                         <div class="body">
                                            <table class="table table-striped ">
                                                 <tbody id="category_tbody">
                                                     
                                                 
                                                 </tbody>
                                             </table>
                                          
                                             
                                         </div>
                                     </div>
                                      
                                      
                                 </div> 
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
                    
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include 'includes/jslinks.php';?>
                <div class="modal fade" id="demoVideoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header ">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Demo Session at Prwatech</h4>
                            </div>
                            <div class="modal-body">
                               <iframe width="570" height="350" src="https://www.youtube.com/embed/xA5TAalZt40" frameborder="0" allowfullscreen></iframe> 
                              
                            </div>
                            <div class="modal-footer">
                                
                            </div>
                        </div>
                    </div>
                </div>
                
	</body>

        <script>
            blog={}
            temp_date={}
            $(document).ready(function(){
                var cta_id="";
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogCategoryDetails.php",
                      success: function(data) {
//                          alert(data)
                        $("#category_tbody").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                        if(cta_id==""){
                            cta_id=article.blog_category_details_id;
                        }
                            $("#category_tbody").append($('<tr/>')
                                    .append($("<th>").append($("<a>").append(article.category+" ("+article.counter+")").css({'cursor':'pointer'})).click(function (){
                                        onlad(article.blog_category_details_id)
                                    }))
                            );   
                        })
                        
                    }
                })
                if(urlParam('id')==null){
                    onlad("");
                }else{
                    onlad(urlParam('id'));
                }
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
            function onlad(category_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetails.php",
                    data:{'category_id':category_id},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
//                        alert(data)
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client").empty();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                            blog[article.blog_details_id]["link"]=article.link;
                            
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               var cl=""
                                if(count%2==1){
                                    cl=" pull-right"
                                }
                                if(temp_date[article.date]==null){
                                    temp_date[article.date]={}
                                    $("#blog_details_client").append($("<div>").addClass("timeline-date-label clearfix").append(article.date));
                                    cl="";
                                }
                                
                                $("#blog_details_client").append($("<div>").addClass("timeline-item"+cl)
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link")))
                                                    .append($("<header>")
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        .append($("<div>").addClass("post-info")
                                                            .append($("<span>").addClass("post-date")
                                                                 .append($("<i>").addClass("icon-calendar"))
                                                                 .append($("<span>").addClass("month").append(article.date))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-user-1"))
                                                                 .append($("<a>").append("Admin"))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-chat"))
                                                                 .append($("<a>").append("comment"))
                                                             )
                                                        ))
                                                    .append($("<div>").addClass("blogpost-content").append(article.short_description))
                                                    .append($("<footer>").addClass("clearfix")
                                                        .append($("<div>").addClass("link pull-right")
                                                            .append($("<i>").addClass("icon-link"))
                                                            .append($("<a>").append("Read More").css({'cursor':'pointer'}).click(function (){
                                                                 window.location="blogpostdetail.php?id="+article.blog_details_id;
                                                            }))
                                                        ))
                                                ));
                            count++;        
                           }
                          
                        });
                    }
                });
                }

            
</script>

</html>



