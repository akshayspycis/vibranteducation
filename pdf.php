<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>PDF | Vibrant Education </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('gallery')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                        </div>
                            <div id="page-start"></div>
 
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                             <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('../images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center">
							<h2 class="text-center heading-font" style="text-transform:none;">PDF File</h2>
							<div class="separator"></div>						
						</div>
					</div>
					<!-- isotope filters start -->
					<div class="filters text-center" id="gallery_display">
						<ul class="nav nav-pills style-2" id="categry_details">
                                                    <li><a href="#" data-filter=".all">All</a></li>
                                                     <!--Here The List of Gallery Category-->   
						</ul>
					</div>
					<!-- isotope filters end -->
					<div class="isotope-container row grid-space-0" id="gallery_grid">
                                            <div class=" row" id="gallery_pics">
                                                
                                            </div>
						
					</div>
					
				</div>
				
				
			</section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
               
                 <!--<script type="text/javascript" src="ajax/SelGallery.js"></script>-->
                  <script type="text/javascript">
                category={}
                $(document).ready(function() {
                     $.ajax({ 
                        type:"post",
                        url:"server/controller/SelCategoryDetails.php",
                        data:{'type':'PDF'},
                        success: function(data) { 
//                                   alert(data.trim());
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article){ 
                                        $('#gallery_display').find('#categry_details')
                                                        .append($('<li>')
                                                            .append($('<a>').attr({'href':'#','data-filter':".asdas"+article.category_details_id}).append(article.category))
                                                            );  
                                                
    
                                    });//$.each() closed
                        } // Success closed
                    }); // $.ajax closed
                 });  // $(document).ready  closed
                           
                
                 
            </script>
                  <script type="text/javascript">
                $(document).ready(function() {
                     $.ajax({ 
                        type:"post",
                        url:"server/controller/SelFileDetailsClient.php",
                        data:{'type':'PDF'},
                        success: function(data) { 
//                            alert(data.trim());
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article){ 
                                        $('#gallery_grid').find('#gallery_pics').append($('<div class="col-sm-6 col-xs-12 col-md-4 isotope-item all asdas'+article.category_details_id+'" style="margin-left:5px;"><div class="image-box shadow text-center"><div class="image-box team-member style-2 bordered dark-bg mb-20 text-center"><div class="overlay-container overlay-visible"><iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/'+article.path+'&embedded=true" style="width:300px; height:300px;" frameborder="0"></iframe></div><div class="body"><p class="small margin-clear">'+article.title+'</p><div class="separator mt-10"></div><a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#PPT_model'+article.file_details_id+'">View Details <i class="pl-10 fa fa-arrow-right"></i></a><div id="PPT_model'+article.file_details_id+'" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content" style="    border-radius: 30px;"><div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);background-color: #444b65;"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'+article.title+'</h4></div><div class="modal-body"><iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/'+article.path+'&embedded=true" style="width:560px; height:300px;" frameborder="0"></iframe><p >'+article.discription+'.</p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div></div></div></div></div>'));
                                    });//$.each() closed
                                        $.get('assets/plugins/magnific-popup/jquery.magnific-popup.min.js').done(function (){
                                        $.get('assets/js/template.js').done(function (){
                                        
                                        })
                                    }) // For Late Binding 
                                    }// Success closed
                    }); // $.ajax closed
                 });  // $(document).ready  closed
                           
              
                 
            </script>
                <script type="text/javascript">
//                   onLoadGallery();
                </script>
                
	</body>
</html>
