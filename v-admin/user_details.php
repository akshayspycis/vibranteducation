<?php // include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Customers</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
              onlad();
            });  
    </script> 
     
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
    <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
 <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Customers</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Customers</h3>
<!--                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addCustomers">Add Customers</button>
                  </div>-->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        
                        <th width="3%">SN</th>
                        <th width="20%">Name</th>
                        <th width="13%">DOB</th>
                        <th width="10%">Contact</th>
                        <th width="15%">Email</th>
                        <th width="13%">Registered Date</th>
                        <th width="5%">User Type</th>
                        <th  >Action</th>
                        <th >Show</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                      
                    </tbody>
                    
                  </table>
                  </div>         
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
    <?php include 'includes/footer.php';?>
               <div class="modal fade" id="insCustomer" role="dialog">
                    <div class="modal-dialog"><!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Customers </h4>
                            </div>
                            <form id ="insertCustomer" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="user_name" id ="user_name" value="" placeholder="Enter Customer  Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="date"  class="form-control"  name="email" id ="email" value="" placeholder="DD/MM/YYYY">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="pic" id ="pic" value="" placeholder="Customer Email">
                            </div>
                            <div class="form-group col-lg-8">
                            <label for="password">Address</label>
                            <textarea name ="user_type" id="user_type" class="form-control" rows="3" value="" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="dob" name="dob">
                            <option value="" selected>Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="contact_no" name="contact_no" value="" class="form-control" placeholder="Customer Contact" maxlength="10">
                            </div>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="date" name="date" value="" class="form-control" placeholder="Customer Password" maxlength="4">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
                            </form>
                            </div>
                            </div>
                  </div> 
      
               <!--Modal For Update Customer Details-->
  <div class="modal fade" id="editcustomer" role="dialog">
            <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Customer Details </h4>
            </div>
             <form id ="updateCustomer" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="hidden" class="form-control" name="user_id" id ="user_id" value="">
                            <input type="text" class="form-control" name="user_name" id ="user_name" placeholder="Enter Customer  Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="date" class="form-control"  name="email" id ="email"  placeholder="Customer date of Birth">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control"  name="pic" id ="pic" placeholder="Customer Email">
                            </div>
                            <div class="form-group">
                            <label for="password">Address</label>
                            <textarea name ="user_type" id="user_type" class="form-control" rows="3" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="dob" name="dob">
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="contact_no" name="contact_no" class="form-control"  maxlength="10" placeholder="Customer Contact">
                            </div>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="date" name="date" class="form-control" maxlength="10" placeholder="Customer Password">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                            

                            </div>
                            </div>
                            </form>
            </div>
            </div>
  </div>       
     <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Customer </h4>
        </div>
        <form id ="deleteCustomer">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="user_id" id="user_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
                <div class="modal fade" id="show_query" role="dialog">
        <div class="modal-dialog modal-lg">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Queries Details</h4>
        </div>
        <form id ="deleteBlogDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12" >
         <table id="data_query" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </table>
         </div>
        </div>
        
        </div>
       </form>
        </div>
        </div>
       </div> 
               
       
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
<script type="text/javascript" language="javascript">
    /*Code to insert Values of Customers   */
      $(document).ready(function(){
        $("#addCustomers").click(function(){
            $("#insCustomer").modal('show');}); 
                $("#insCustomer").on('shown.bs.modal', function(){
                            $('#insertCustomer').submit(function() {
                                   
                                     var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insertCustomer').find('#user_name').focus();
                                        return false;
                                       }else if(this.email.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                           $('#insertCustomer').find('#email').focus();
                                         return false;
                                         
                                     }
                                     else if(this.pic.value == "" || !emailfilter.test(this.pic.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insertCustomer').find('#pic').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.user_type.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#user_type').focus();
                                         return false;
                                         
                                     }
                                     else if(this.dob.value == "" || !genderfilter.test(this.dob.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#contact_no').focus();
                                         return false;
                                         
                                     }
                                     else if(this.date.value == "" || !passwordfilter.test(this.date.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insertCustomer').find('#date').focus();
                                         return false;
                                         
                                     }
                                     else{
                                     
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/insertCustomer.php",
                                        data:$('#insertCustomer').serialize(),
                                        success: function(data){  
                                        $('#insertCustomer').each(function(){
                                        this.reset();
                                        location.reload(true);
                                        return false;
                                    });
                                } 
                            });
                   return false;   
                  }
                                     
                    });
                  var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#insCustomer').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
               </script>
  <script type="text/javascript" language="javascript">
     /* Ajax call for Customer Display*/
     cus={}
           function onlad(){
                     $.ajax({
                    type:"post",
                    data:{'user_id':''},
                    url:"../server/controller/selectUsers.php",
                    success: function(data) {
                      alert(data);
                      $("body").append(data);
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cus[article.user_id]={};
                            cus[article.user_id]["user_name"]=article.user_name;
                            cus[article.user_id]["dob"]=article.dob;
                            cus[article.user_id]["email"]=article.email;
                            cus[article.user_id]["contact_no"]=article.contact_no;
                            cus[article.user_id]["date"]=article.date;
                            cus[article.user_id]["pic"]=article.pic;
                            cus[article.user_id]["user_profile_details_id"]=article.user_profile_details_id;
                            cus[article.user_id]["user_type"]=article.user_type;
                            cus[article.user_id]["count"]=article.count;
//                            cus[article.user_id]["password"]=article.password;
                            var img= "../server/controller/"+article.pic;
                            $("#data >tbody").append($('<tr/>')
                                    .append($('<td/>').html((index+1)))
                                    .append($('<td/>').html(article.user_name))
                                    .append($('<td/>').html(article.dob))
                                    .append($('<td/>').html(article.contact_no))
                                    .append($('<td/>').html(article.email))
                                    .append($('<td/>').html(article.date))
//                                    .append($('<td/>').html(img))
                                    //.append($('<td/>').html(article.user_profile_details_id))
                                    
//                                    .append($('<td/>').append($("<p>").append(article.password)).append($("<button>").attr({'type':'button','class':'btn btn-primary btn-xs'}).append($("<i>").addClass("fa fa-edit")).click(function (){
//                                        alert(decodeURIComponent($.md5($(this).parent().find("p").text(), '12345')))
//                                
//                                    })))
                                    .append($('<td/>').html(article.user_type))
//                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-user_id="+article.user_id+"  id=\"edit\" data-toggle=\"modal\" data-target=\"#editcustomer\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-user_id="+article.user_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                    .append($('<td/>').html("<button type=\"button\" class=\"btn btn-default btn-xs\"  \")>Enquery ("+article.count+")</button>")
                                    .click(function (){
                                            if(article.count>0){
                                                $("#show_query").modal('toggle');
                                                 $("#show_query").off('shown.bs.modal');
                                                 $("#show_query").on('shown.bs.modal', function(){
                                                     showUserQuery(article.count,article.contact_no,article.email);
                                                 });
                                            }
                                            
                                     })
                                ));
                        });
                        
                    }
                });
         }
               </script>
<script type="text/javascript" language="javascript">
        /* Ajax call for Edit Customer */
         $(document).on("click", "#edit", function () {
                 var user_id = $(this).data('user_id');
                
                 $("#editcustomer").on('shown.bs.modal', function(){
                                    $("#editcustomer").find("#user_id").val(user_id);  
                                    $("#editcustomer").find("#user_name").val(cus[user_id]["user_name"]); 
                                    $("#editcustnmer").find(".select2").val(cus[user_id]["dob"]);
                                    $("#editcustomer").find("#email").val(cus[user_id]["email"]);  
                                    $("#editcustomer").find("#contact_no").val(cus[user_id]["contact_no"]);  
                                    $("#editcustomer").find("#date").val(cus[user_id]["date"]);  
                                    $("#editcustomer").find("#pic").val(cus[user_id]["pic"]);  
                                    $("#editcustomer").find("#user_profile_details_id").val(cus[user_id]["user_profile_details_id"]);  
                                    $("#editcustomer").find("#user_type").val(cus[user_id]["user_type"]);  
                                    $('#updateCustomer').submit(function() {
                                        
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                      var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     
                                        var passwordfilter = /^\d{4}$/;
                                        if(user_id=="1"){
                                            passwordfilter = /^\d{10}$/;
                                        }
                                     
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#user_name').focus();
                                        return false;
                                       }else if(this.email.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                           $('#updateCustomer').find('#email').focus();
                                         return false;
                                         
                                     }
                                     else if(this.pic.value == "" || !emailfilter.test(this.pic.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#pic').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.user_type.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#user_type').focus();
                                         return false;
                                         
                                     }
                                     else if(this.dob.value == "" || !genderfilter.test(this.dob.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                         $('#updateCustomer').find('#contact_no').focus();
                                         return false;
                                         
                                     }
                                     else if(this.date.value == "" || !passwordfilter.test(this.date.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#date').focus();
                                         return false;
                                         
                                     }
                                     else{
                                            $.ajax({
                                            type:"post",
                                            url:"../server/controller/updateCustomer.php",
                                            data:$('#updateCustomer').serialize(),
                                            success: function(data){ 
                                            $('#updateCustomer').each(function(){
                                            this.reset();
                                            location.reload(true);
                                            $('#editcustomer').modal('hide');
                                            return false;
                                            });
                                            } 
                                            });
                                            return false;
                                     }
                              
                      
                   
    });
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#editcustomer').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});
</script>
<script type="text/javascript" language="javascript">
 /* Ajax call for Delete Customer */
    $(document).on("click", "#delete", function () {
                 var user_id = $(this).data('user_id');
                        if(user_id===1){
                            $("#delete1").on('shown.bs.modal', function(){ 
                                $("#delete1").find(".modal-title").empty();   
                                $("#delete1").find(".modal-title").addClass("text-danger").append("Warning");   
                                $("#delete1").find("#msg").empty();   
                                   $("#delete1").find("#msg").append("Sorry, You can not delete admin.");   
                                   $("#delete1").find("#delete2").css({'display':'none'}); 
                                   
                               return false; 
                            });
                        }else{
                     $("#delete1").on('shown.bs.modal', function(){
                          $("#delete1").find(".modal-title").empty();
                          $("#delete1").find(".modal-title").append("Delete Customer");
                          $("#delete1").find("#msg").empty();  
                          $("#delete1").find("#msg").append("Sure to want to delete ?");  
                          $("#delete1").find(".modal-footer").find("#delete2").css({'display':''}); 
                    $("#delete1").find("#user_id").val(user_id);        
                        $('#deleteCustomer').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteCustomer.php",
                            data:"user_id="+user_id,
                            success: function(data){ 
                                
                                $("#a").append(data);
                                if(data.trim()!="Error"){
                                     $('#deleteCustomer').each(function(){
                                this.reset();
                                        onlad()
                                        $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                }else{
                                     $('#confirm').modal('show');
                                }
                             
                                } 
                  });
                    return false;
    });
    
    });
                        }
       
   
});

  /* Code for Customer Delete end */  
  function showUserQuery(count,contact_no,email){
             $.ajax({
                type:"post",
                data:{'contact_no':contact_no,'email':email},
                url:"../server/controller/SelUserQuery.php",
                success: function(data) {
                  var duce = jQuery.parseJSON(data);
                    $("#show_query").find("#data_query tr:has(td)").remove();
                    $.each(duce, function (index, article) {
                      $("#show_query").find("#data_query").append($('<tr/>')
                            .append($('<td/>').html((index+1)))
                            .append($('<td/>').html(article.name))
                            .append($('<td/>').html(article.email))
                            .append($('<td/>').html(article.contact))
                            .append($('<td/>').html(article.subject))
                            .append($('<td/>').html(article.message))
                            .append($('<td/>').html(article.date))
                         );
                    });
                }
            });
  }
 
</script>
  </body>
</html>

