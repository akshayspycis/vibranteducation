<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>User Course Registration</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
      <script type="text/javascript">
          course_batch_details={}
          $(document).ready(function(){
              onlad();
            });  
      </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">User Course Registration</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">User Course Registration</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-3">
                     
                        </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Profile Pic</th>
                        <th>User Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>DOB</th>
                        <th>Relative Name</th>
                        <th>Relative Contact</th>
                        <th>Relative occupation</th>
                        <th>Applicant occupation</th>
                        <th colspan="3">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

         <div class="modal fade" id="editPayment" role="dialog">
        <div class="modal-dialog modal-lg">
        <div class="modal-content ">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Personal Details</h4>
        </div>
        <div class="modal-body no-border">
        <div class="col-md-6">
                        <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Personal Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-3" id="pic_path"></div>
                <div class="col-md-9">
                    <div class="table-responsive">
                        <table class="table">
                           <tbody>
                              <tr>
                                <th style="width:50%">Name:</th>
                                <td id="user_name"></td>
                              </tr>
                              <tr>
                                <th>Gender</th>
                                <td id="gender"></td>
                              </tr>
                              <tr>
                                <th>DOB</th>
                                <td id="dob"></td>
                              </tr>
                              <tr>
                                <th>Contact</th>
                                <td id="contact_no"></td>
                              </tr>
                              <tr>
                                <th>Email</th>
                                <td id="email"></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                </div>
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
                        <div class="col-md-6">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Relative Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                           <tbody>
                              <tr>
                                <th style="width:50%">Relative Name:</th>
                                <td id="relative_name"></td>
                              </tr>
                              <tr>
                                <th>Contact</th>
                                <td id="relative_contact_no">Contact</td>
                              </tr>
                              <tr>
                                <th>Relative Occupation</th>
                                <td id="relative_occupation">01-01-1993</td>
                              </tr>
                              <tr>
                                <th>Applicant Occupation</th>
                                <td id="applicant_occupation">8817919016</td>
                              </tr>
                              <tr>
                                <th>Referal Name </th>
                                <td id="referal_name">Akshay (8817919016)</td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
                        <div class="col-md-6">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Address Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                            <tbody id="Local">
                              <tr>
                                <th style="width:50%">Type</th>
                                <td id="address_type"></td>
                              </tr>
                              <tr>
                                <th>Address</th>
                                <td id="address"></td>
                              </tr>
                              <tr>
                                <th>Street</th>
                                <td id="street"></td>
                              </tr>
                              <tr>
                                <th>City</th>
                                <td id="city"></td>
                              </tr>
                              <tr>
                                <th>Pincode</th>
                                <td id="pincode"></td>
                              </tr>
                              <tr>
                                <th>state</th>
                                <td id="state"></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
                        <div class="col-md-6">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Address Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                           <tbody id="Permanent">
                              <tr>
                                <th style="width:50%">Type</th>
                                <td id="address_type"></td>
                              </tr>
                              <tr>
                                <th>Address</th>
                                <td id="address"></td>
                              </tr>
                              <tr>
                                <th>Street</th>
                                <td id="street"></td>
                              </tr>
                              <tr>
                                <th>City</th>
                                <td id="city"></td>
                              </tr>
                              <tr>
                                <th></th>
                                <td id="pincode"></td>
                              </tr>
                              <tr>
                                <th>state</th>
                                <td id="state"></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
                        <div class="col-md-12">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Academic qualification</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <table  class="table table-bordered table-hover dataTable" >
                <thead >
                <tr role="row">
                    <th >Class / Graduation</th>
                    <th >School/College</th>
                    <th >Board/University</th>
                    <th >Year of passing</th>
                    <th >Percentage</th>
                </tr>
                </thead>
                <tbody id="qualification_details">
                </tbody>
                <tfoot>
                </tfoot>
              </table>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-6">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Language Proficiency and Other Skills</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                           <tbody >
                              <tr>
                                <th style="width:50%">Additional Qualification</th>
                                <td id="additional_qualification"></td>
                              </tr>
                              <tr>
                                <th>Computer Literacy</th>
                                <td id="computer_literacy"></td>
                              </tr>
                              
                            </tbody>
                        </table>
                      </div>
                <dl class="dl-horizontal">
                <h5 class="box-title">Language Proficiency</h5>
                <table  class="table table-bordered table-hover dataTable" >
                <thead >
                <tr role="row">
                    <th >Language</th>
                    <th >Read</th>
                    <th >Write</th>
                    <th >Speak</th>
                </tr>
                </thead>
                <tbody id="language_proficiency">
                </tbody>
                <tfoot>
                </tfoot>
              </table>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
            <div class="col-md-6">
                        <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Course applicant wish to enroll for</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                    <tr>
                                            <th>S.No.</th>
                                            <th>Course Opted</th>
                                            <th>Tick</th>
                                    </tr>
                            </thead>
                           <tbody>
                              <tr>
                                <th>1</th>
                                <th>Bank PO/MT and Clerk</th>
                                <td id="course_po_mt_and_clerk"></td>
                              </tr>
                              <tr>
                                <th>2</th>
                                <th>Advanced Communicative English</th>
                                <td id="advance_comm_english"></td>
                              </tr>
                              <tr>
                                <th>3</th>
                                <th>SSC and Railways</th>
                                <td id="ssc_and_railways"></td>
                              </tr>
                              <tr>
                                <th>4</th>
                                <th>English for competitive Exams</th>
                                <td id="english_for_competitive_exams"></td>
                              </tr>
                              <tr>
                                <th>5</th>
                                <th>Personal Interview</th>
                                <td id="personal_interviews"></td>
                              </tr>
                              <tr>
                                <th>5</th>
                                <th>MBA entrance exams</th>
                                <td id="mba_entrance_exam"></td>
                              </tr>  
                            </tbody>
                        </table>
                      </div>
                </div>
                <br>
                <br>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
            <div class="col-md-12">
                            <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title"> School/Collage/Work Experience</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                           <tbody >
                              <tr>
                                <th style="width:50%">School</th>
                                <td id="school"></td>
                              </tr>
                              <tr>
                                <th>Collage</th>
                                <td id="college"></td>
                              </tr>
                              <tr>
                                <th>Name and Address of Institution/<br>Company where Applicant <br>is studying/working</th>
                                <td id="address_of_institiute_or_company"></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                <dl class="dl-horizontal">
                <h5 class="box-title">Work Experience (If any) Organisation </h5>
                <table  class="table table-bordered table-hover dataTable" >
                <thead >
                <tr role="row">
                    <th >Organisation</th>
                    <th >Experience(in months till date)</th>
                    <th >Job Profile or Designation</th>
                </tr>
                </thead>
                <tbody id="work_experience">
                </tbody>
                <tfoot>
                </tfoot>
              </table>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
            <div class="col-md-12">
              <div class="box box-solid" style="box-shadow: 0 3px 4px 0 rgba(0,0,0,.14), 0 3px 3px -2px rgba(0,0,0,.2), 0 1px 8px 0 rgba(0,0,0,0.12);">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title"> Bear witness to</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                        <table class="table">
                            <thead>
                                    <tr>
                                            <th>S.No.</th>
                                            <th>Attestation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                            <th>Upload Area</th>
                                    </tr>
                            </thead>
                            <tbody id="bear_witness">
                              
                          </tbody>
                        </table>
                      </div>
                
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        
        </div>
        <div class="modal-footer no-border">
        
        </div>
        </div>
        </div>
       </div>
      
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

      
   
  </body>
  
    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  

//...........................................................................................................

var course_registration={}
function onlad(){ 
    
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
//                    data:{'course_id':course_id},
                    url:"../server/controller/SelCourseRegistration.php",
                    success: function(data) {
                        course_registration={}
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        
                        $.each(duce, function (index, article) {
                            course_registration[article.course_registration_id]=article;
                            var img=$("<img>").attr('src','../server/controller/'+article.pic_path).css({'width': '100%','width': '90px','height': '100px','border-radius': '10%'})
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(img))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.gender))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.dob))
                                .append($('<td/>').html(article.relative_name))
                                .append($('<td/>').html(article.relative_contact_no))
                                .append($('<td/>').html(article.relative_occupation))
                                .append($('<td/>').html(article.applicant_occupation))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs btn-primary").append("More Info").click(function (){
                                    showMore(article.course_registration_id);
                                })))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs btn-primary").append("DELETE").click(function (){
                                    showEdit(article.course_registration_id);
                                })))
                             );
                        });
                    }
                });
                }
var course_registration_id;                
function showMore(c){
course_registration_id=c;
$("#editPayment").modal('toggle');
}                
$("#editPayment").on('shown.bs.modal', function(){
                    $("#editPayment").find("#pic_path").html($("<img>").attr('src','../server/controller/'+course_registration[course_registration_id].pic_path).css({'width': '100%','width': '90px','height': '100px','border-radius': '10%'}));        
                    $("#editPayment").find("#user_name").html(course_registration[course_registration_id].user_name);        
                    $("#editPayment").find("#gender").html(course_registration[course_registration_id].gender);        
                    $("#editPayment").find("#email").html(course_registration[course_registration_id].email);        
                    $("#editPayment").find("#contact_no").html(course_registration[course_registration_id].contact_no);        
                    $("#editPayment").find("#dob").html(course_registration[course_registration_id].dob);        
                    $("#editPayment").find("#relative_name").html(course_registration[course_registration_id].relative_name);        
                    $("#editPayment").find("#relative_occupation").html(course_registration[course_registration_id].relative_occupation);        
                    $("#editPayment").find("#applicant_occupation").html(course_registration[course_registration_id].applicant_occupation);        
                    $("#editPayment").find("#relative_contact_no").html(course_registration[course_registration_id].relative_contact_no);        
                    $("#editPayment").find("#referal_name").html(course_registration[course_registration_id].referal_name+"("+course_registration[course_registration_id].referal_contact_no+")");        
                    $("#editPayment").find("#additional_qualification").html(course_registration[course_registration_id].additional_qualification);        
                    $("#editPayment").find("#computer_literacy").html(course_registration[course_registration_id].computer_literacy);        
                    $("#editPayment").find("#school").html(course_registration[course_registration_id].school);        
                    $("#editPayment").find("#college").html(course_registration[course_registration_id].college);        
                    $("#editPayment").find("#address_of_institiute_or_company").html(course_registration[course_registration_id].address_of_institiute_or_company);        
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelAddressDetails.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $.each(duce, function (index, article) {
                                $("#editPayment").find("#"+article.address_type).find("#address_type").html(article.address_type);        
                                $("#editPayment").find("#"+article.address_type).find("#address").html(article.address);        
                                $("#editPayment").find("#"+article.address_type).find("#street").html(article.street);        
                                $("#editPayment").find("#"+article.address_type).find("#city").html(article.city);        
                                $("#editPayment").find("#"+article.address_type).find("#pincode").html(article.pincode);        
                                $("#editPayment").find("#"+article.address_type).find("#state").html(article.state);        
                            });
                        } 
                    });
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelQualificationDetails.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $("#qualification_details").empty();
                            $.each(duce, function (index, article) {
                                $("#qualification_details").append($('<tr/>').addClass("odd")
                                    .append($('<td/>').html(article.class))
                                    .append($('<td/>').html(article.school_college))
                                    .append($('<td/>').html(article.board_university))
                                    .append($('<td/>').html(article.year_of_passing))
                                    .append($('<td/>').html(article.percentage))
                                );
                            });
                        } 
                    });
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelLanguageProficiency.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $("#language_proficiency").empty();
                            $.each(duce, function (index, article) {
                                $("#language_proficiency").append($('<tr/>').addClass("odd")
                                    .append($('<td/>').html(article.language_p))
                                    .append($('<td/>').html(article.read_p==1?'Yes':'No'))
                                    .append($('<td/>').html(article.write_p==1?'Yes':'No'))
                                    .append($('<td/>').html(article.speak_p==1?'Yes':'No'))
                                );
                            });
                        } 
                    });
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelWorkExperience.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $("#work_experience").empty();
                            $.each(duce, function (index, article) {
                                $("#work_experience").append($('<tr/>').addClass("odd")
                                    .append($('<td/>').html(article.organization))
                                    .append($('<td/>').html(article.time))
                                    .append($('<td/>').html(article.designation))
                                );
                            });
                        } 
                    });
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelBearWitness.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $("#bear_witness").empty();
                            $.each(duce, function (index, article) {
                                $("#bear_witness").append($('<tr/>').addClass("odd")
                                    .append($('<td/>').html(index+1))
                                    .append($('<td/>').html(article.attestation))
                                    .append($('<td/>').html($("<img>").attr('src','../server/controller/'+article.pic_path)))
                                );
                            });
                        } 
                    });
                    $.ajax({
                        type:"post",
                        url:"../server/controller/SelCourseOptedDetails.php",
                        data:{'course_registration_id':course_registration_id},
                        success: function(data){ 
                            var duce = jQuery.parseJSON(data);
                            $("#language_proficiency").empty();
                            $.each(duce, function (index, article) {
                                $("#editPayment").find("#course_po_mt_and_clerk").html(article.course_po_mt_and_clerk==1?'Yes':'No');        
                                $("#editPayment").find("#advance_comm_english").html(article.advance_comm_english==1?'Yes':'No');        
                                $("#editPayment").find("#ssc_and_railways").html(article.ssc_and_railways==1?'Yes':'No');        
                                $("#editPayment").find("#english_for_competitive_exams").html(article.english_for_competitive_exams==1?'Yes':'No');        
                                $("#editPayment").find("#personal_interviews").html(article.personal_interviews==1?'Yes':'No');        
                                $("#editPayment").find("#mba_entrance_exam").html(article.mba_entrance_exam==1?'Yes':'No');        
                            });
                        } 
                    });
    
    });
 
function sendReciept(obj,course_registration_id){
            obj.html($("<img>").attr({'src':'../assets/images/fb_ty.gif'}));
            $.ajax({
                type:"post",
                url:"../server/controller/SendReceipt.php",
                data:{'registered_temp_code':course_registration[course_registration_id].registered_temp_code,
                    'paid':course_registration[course_registration_id].paid,
                    'balance':course_registration[course_registration_id].balance,
                    'user_name':course_registration[course_registration_id].user_name,
                    'email':course_registration[course_registration_id].email,
                    'date':course_registration[course_registration_id].date},
                success: function(data){ 
                    obj.html("Send Reciept");
                } 
            });
 }


</script>


</html>


