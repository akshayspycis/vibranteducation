<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Download Section</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
     #data tr th{
                background:green;
                color:white;
                font-family:verdana;
                font-size:13px;
            }
            #data tr td{
                font-family:verdana;
                font-size:13px;
            }
            #data tr td a{
                color:red;
                text-decoration:underline;
                font-family:verdana;
            }
    </style>
  </head>
  
  <body class="skin-blue  skin-blue  sidebar-mini sidebar-collapse ">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Download Section</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Download Section</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addDownloads">Add File</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>File Path</th>
                        <th>Date</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
         <div class="modal fade" id="insDownloads" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Downloads </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data" >    
        <div class="modal-body">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="Heading">File Title</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Title here">
                                </div>
                                <!-- /.form-group -->
                      
                                <div class="form-group">
                                    <label for="link">Upload File</label>
                                    <input type="file" name="files[]" id="input_files" multiple>
                                    <button type="submit" id="btn_submit" class="form-control">Upload Files!</button>
                                </div>
                            </div><!-- /.col -->
                            <!-- /.col -->
                        </div>
                        <div id="loading_spinner"><i class="fa fa-spinner fa-pulse"></i> Uploading</div>
                        <div id="result"></div><!-- /.row -->
                    </div><!-- /.box-body -->
                      
                      
                </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
   
      <div class="modal fade" id="delDownloads" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <form id ="delDownloadsboard">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="id" id="id" value="" class="form-control"/>
        <input type="hidden"  name="path" id="path" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
    <div id="warningmsg2" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:orangered;color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warning</h4>
                </div>
                <div class="modal-body" id="errormsg">
                    <p id="msg" style="color:#000;font-family:century gothic;font-size:18px;font-weight:bold"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>
  </body>
    <script>
        /* Insert Downloads */ 
      function TransferCompleteCallback(content){
                // we might want to use the transferred content directly
                // for example to render an uploaded image
       }
       var formdata = false;
      $(document).ready(function() {
          $("#addDownloads").click(function(){
            $("#insDownloads").modal('show');}); 
      });
         $("#insDownloads").on('shown.bs.modal', function(){
                  });
//                var input = document.getElementById("input_files");
                
                if (window.FormData) {
                    formdata = new FormData();
                    $("#btn_submit").hide();
                    $("#loading_spinner").hide();
                }
                $('#input_files').on('change',function(event){
                    var i = 0, len = this.files.length, img, reader, file;
                    //console.log('Number of files to upload: '+len);
                    $('#result').html('');
                    $('#input_files').prop('disabled',true);
                    $("#loading_spinner").show();
                    for ( ; i < len; i++ ) {
                        file = this.files[i];
                        //console.log(file);
                        if(!!file.name.match(/.*\.pdf$/)){
                                if ( window.FileReader ) {
                                    reader = new FileReader();
//                                    reader.onloadend = function (e) { 
//                                        TransferCompleteCallback(e.target.result);
//                                    };
                                    reader.readAsDataURL(file);
                                }
                                if (formdata) {
                                    formdata.append("files[]", file);
                                    formdata.append("name", $("#name").val());
                                }
                        } else {
                            $("#loading_spinner").hide();
                            $('#input_files').val('').prop('disabled',false);
                            alert(file.name+' is not a PDF');
                        }
                    }
                   
                    
                   
                });
      
                    
      
         $('#insertform').submit(function() {
//                       alert($("#name").val());
                       if (formdata) {
                        $.ajax({
                            url: "../server/controller/InsDownloads.php",
                            type: "POST",
                            data: formdata,
                            processData: false,
                            contentType: false, // this is important!!!
                            success: function (res){
//                              alert(res.trim());
                                var result = JSON.parse(res);
                                $("#loading_spinner").hide();
                                $('#input_files').val('').prop('disabled',false);
//                                if(result.res === true){
//                                    var buf = '<ul class="list-group">';
//                                    for(var x=0; x<result.data.length; x++){
//                                        buf+='<li class="list-group-item">'+result.data[x]+'</li>';
//                                    }
//                                    buf += '</ul>';
//                                    $('#result').html('<strong>Files uploaded:</strong>'+buf);
//                                } else {
//                                    $('#result').html(result.data);
//                                }
                                // reset formdata
                                formdata = false;
                                formdata = new FormData();
                                $('#insertform').each(function(){
                                this.reset();
                           });
                            onLoad();    
                            } 
                        });
                        }
                        return false;
                    });
  </script>
  <script>
      /* Select Notifications */  
      $(document).ready(function() {
             downloadsection ={};
             onLoad();
       });
      function onLoad(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelDownloadsection.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            downloadsection[article.id]={};
                            downloadsection[article.id]["id"]=article.id;
                            downloadsection[article.id]["name"]=article.name;
                            downloadsection[article.id]["path"]=article.path;
                            downloadsection[article.id]["date"]=article.date;
                            var datafile= "../server/controller/"+article.path;
                            $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.name))
                                .append($('<td/>').html("<a href="+datafile+" target=\"blank\"><img src=\"../assets/images/bg/downloads.png\" height=\"30\"/></a>"))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_row'}).append("Delete").click(function (){
                                   deleteRow(article.id,article.path); 
                                })))
                             );
                        });
                    }
                });   
             }
  </script>

   

  <script>
       function deleteRow(id,path){
           $("#delDownloads").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelDownloadsection.php",
                            data:{"id":id,"path":path},
                            success: function(data){
                            alert(data.trim());
                            onLoad();
                            $("#delDownloads").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>

</html>


