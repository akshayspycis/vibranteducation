<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>File Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>

    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          var category_details_id;
          $(document).ready(function(){
              $(".modal").on('hide.bs.modal', function() {
                location.reload(true);
              });
              $("#category").change(function (){
                    $("#type").val($(this).val());
                    onladCategoryDetails($(this).val());
              });
              $("#category").trigger("change");
            });  
          
      </script>
      
      
      
    <style>
#upload-wrapper {
	    background: #828b82;
    padding: 50px;
    border-radius: 10px;
}
#upload-wrapper h3 {
	padding: 0px 0px 10px 0px;
	margin: 0px 0px 20px 0px;
	margin-top: -30px;
	border-bottom: 1px dotted #DDD;
}
#upload-wrapper input[type=file] {
	padding: 6px;
	background: #FFF;
	border-radius: 5px;
}
#upload-wrapper #submit-btn {
	border: none;
	padding: 10px;
	background: #61BAE4;
	border-radius: 5px;
	color: #FFF;
}
#output{
	padding: 5px;
	font-size: 12px;
}

/* prograss bar */
#progressbox {
	border: 1px solid #CAF2FF;
	padding: 1px; 
	position:relative;
	width:400px;
	border-radius: 3px;
	margin: 10px;
	display:none;
	text-align:left;
}
#progressbar {
	height:20px;
	border-radius: 3px;
	background-color: #CAF2FF;
	width:1%;
}
#statustxt {
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #FFFFFF;
}

    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">File Details & Updates</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <div class="row">
                      <div class="col-lg-2">
                          <h3 class="box-title">File Details & Updates</h3>
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group" >
                          <label>Select Type</label>
                          <select class="form-control select2" id="category" name="category">
                            <option value="Youtube">Youtube</option>
                            <option value="Mp3">Mp3</option>
                            <option value="Mp4">Mp4</option>
                            <option value="Ppt">PPT</option>
                            <option value="PDF">PDF</option>
                            <option value="msword">MS Word</option>
                          </select>
                           </div><!-- /.form-group -->    
                      </div>
                      <div class="col-lg-3">
                          <div class="form-group">
                             <label>Select Category Details</label>
                             <select class="form-control select2" id="category_details" name="category_details">
                            </select>
                          </div><!-- /.form-group -->    
                      </div>
                      <div class="col-lg-4">
                          <div class="pull-right">
                            <button type="button" class="btn btn-success" id="addFileDetails">Add File Details</button>
                  </div>
                      </div>
                  </div>
                  
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                            <th width="5%">S.N.</th>
                            <th>Title</th>
                            <th width="30%">Long Discription</th>
                            <th width="30%">File</th>
                            <th  style="text-align:center">Action</th>
                            <th style="text-align:center">Status</th>
                       </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insFileDetails" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Insert File Details </h4>
            </div>
            <form id="MyUploadForm" action="../server/controller/InsFileDetails.php" method="post" enctype="multipart/form-data" >
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="student name">File Heading</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Heading here">
                            <input type="hidden" class="form-control" name="category_details_id" id="category_details_id">
                            <input type="hidden" class="form-control" name="discription" id="discription">
                            <input type="hidden" class="form-control" name="type" id="type">
                        </div>
                        <div class="form-group col-md-12" >
                            <label>File Long Content</label>
                            <textarea class="form-control" rows="5" id="long_discription"  name="long_discription" style="resize:none" placeholder="Content here"></textarea>
                        </div><!-- /.form-group -->
                        <div  class="col-md-12">
                            <div align="center" id="upload-wrapper">
                                    <h3>File Uploader</h3>
                                    <input name="FileInput" id="FileInput" type="file" />
                                    <img src="../test/ajax/images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                                    <div id="progressbox" ><div id="progressbar"></div ><div id="statustxt">0%</div></div>
                                    
                            </div>
                            <textarea class="form-control" rows="5" id="you_tube_path"  name="you_tube_path" style="display:none" placeholder="Youtube Url"></textarea>
                            <div id="output"></div>
                        </div>
                    </div><!-- /.row -->
                    <div class="modal-footer">
                        <div class="validate pull-left" style="font-weight:bold;"></span></div>
                        <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
        </div>
       </div>
   
      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete File Details</h4>
        </div>
        <form id ="deleteFileDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="file_details_id" id="file_details_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
      
  </body>
  <script type="text/javascript" language="javascript">
      var obj_class=null;
      var text_editor;
      $(document).ready(function(){
        $("#addFileDetails").click(function(){
            $("#insFileDetails").modal('show');
        }); 
        $("#insFileDetails").on('shown.bs.modal', function(){
            text_editor=CKEDITOR.replace("long_discription");
            if($("#type").val()=="Youtube"){
                $("#upload-wrapper").hide();
                $("#you_tube_path").show();
            }
        });
      }); 
         
</script>

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        function deleteFile(file_details_id,path) {
                    $("#delete1").modal("show");
                    $("#delete1").on('shown.bs.modal', function(){
                        $('#deleteFileDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelFileDetails.php",
                            data:{'file_details_id':file_details_id,'type':$("#type").val(),'path':path},
                            success: function(data){ 
                                $("#category_details").trigger("change");
                                $('#deleteFileDetails').each(function(){
                                    this.reset();
                                    $('#delete1').modal('hide');
                                    return false;
                                });
                            } 
                        });
                        return false;
                    });
                });
        }
  /* Code for Product Delete end */  
     
//...........................................................................................................
        function onladCategoryDetails(type){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelCategoryDetails.php",
                    data:{'type':type},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#category_details").empty();
                        $.each(duce, function (index, article) {
                            $("#category_details").append($("<option>").append(article.category).attr({'value':article.category_details_id}));
                        });
                        $("#category_details").trigger("change");
                    }
                    });
                    $("#category_details").change(function (){
                        $("#category_details_id").val($(this).val());
                        onlad($(this).val());
                    });
                    
                }
        function onlad(category_detailsid){
            category_details_id=category_detailsid
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelFileDetails.php",
                    data:{'category_details_id':category_details_id},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                          var type=article.type;
                          var path;
                          switch(type){
                              case 'Youtube':
                                  path=article.path
                              break;    
                              case 'Mp3':
                                  path=$('<audio controls><source src="../server/controller/'+article.path+'" type="audio/mpeg"></audio>')
                              break;    
                              case 'Mp4':
                                  path=$('<video width="400" controls><source src="../server/controller/'+article.path+'" type="video/mp4"></video>')
                              break;    
                              case 'Ppt':
                                  path=$('<iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/'+article.path+'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>')
                              break;    
                              case 'PDF':
                                  path=$('<a href="../server/controller/'+article.path+'" target="_blank">Open</a>');
                              break;    
                              case 'msword':
                                  path=$('<a href="../server/controller/'+article.path+'" target="_blank">Open</a>');
                              break;    
                          }
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.title))
                                .append($('<td/>').html(decodeURIComponent(article.discription)))
                                .append($('<td/>').html(path))
//                                .append($('<td/>').html(article.date))
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-primary btn-xs\" \")><i class=\"fa fa-edit\"></i    ></button>")).click(function (){
//                                    showComment(article.file_details_id)
//                                } )
                                .append($('<td/>').append($("<button></button>").addClass("btn btn-danger btn-xs").append("Delete").click(function(){
                                     deleteFile(article.file_details_id,article.path);
                                     
                                })))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.file_details_id);
                                     
                                })))
                             );
                        });
                    }
                });
                }
                function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdFileStatus.php",
                    data:{'file_details_id':id,'status':value},
                    success: function(data) {
                        
                    }
                  });
              }
                
                
            
</script>
<script type="text/javascript">
$(document).ready(function() { 
	var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			success:       afterSuccess,  // post-submit callback 
			uploadProgress: OnProgress, //upload progress callback 
			resetForm: true        // reset the form after successful submit 
		}; 
	 $('#MyUploadForm').submit(function() { 
             $("#discription").val(text_editor.getData());
              $(this).ajaxSubmit(options);  			
              return false; 
         }); 

//function after succesful file upload (when server response)
function afterSuccess()
{
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button
	$('#progressbox').delay( 1000 ).fadeOut(); //hide progress bar
        text_editor.setData("");
        $("#category_details").trigger("change");
}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
            if($("#type").val()=="Youtube"){
                $('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
            }else{
		if( !$('#FileInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}
		var fsize = $('#FileInput')[0].files[0].size; //get file size
		var ftype = $('#FileInput')[0].files[0].type; // get file type
		//allow file types 
		switch(ftype)
        {
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html':
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
			case 'audio/mp3':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 5 MB (1048576)
		if(fsize>52428800) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
            }
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//progress bar function
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar
    $('#progressbox').show();
    $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $('#statustxt').html(percentComplete + '%'); //update status text
    if(percentComplete>50)
        {
            $('#statustxt').css('color','#000'); //change status text to white after 50%
        }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

}); 

</script>

</html>

