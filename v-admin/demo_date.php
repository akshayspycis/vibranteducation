<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Demo date</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
              cate={}
        </script>
         
      <script type="text/javascript">
          pro={}
          demo_class={}
          var cat_id;
          $(document).ready(function(){
                  $.ajax({
                        type:"post",
                        url:"../server/controller/SelDemoClass.php",
                        success: function(data) { 
                            var duce = jQuery.parseJSON(data); //here data is passed to js object
                        demo_class={}
                        $.each(duce, function (index, article) {
                        demo_class[article.demo_class_id]=article.demo_class;    
                            var a=$("<option></option>").append(article.demo_class).attr({'value':article.demo_class_id});
                            $("#search_demo_class_id").find('#searchdemo_class_id').append(a);                    
                            a=$("<option></option>").append(article.demo_class).attr({'value':article.demo_class_id});
                            $("#insDemoDate").find('#demo_class_id').append(a);                    
                        });
                            $("#search_demo_class_id").find('#searchdemo_class_id').trigger("change");
                        }
                     });
                     $("#search_demo_class_id").find('#searchdemo_class_id').change(function () {
                var id = $( this ).val();
                onlad(id)
             }); 
                }); 
             
            
        </script>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Demo date</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                    
                <div class="box-header">
                    <h2 class="box-title">Demo date</h2>
                    <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Demo date</button>
                  </div>
                    <br>
                    <br>
                  <div class="col-md-3" id="search_demo_class_id">
                        <div class="form-group" id="category">
                                <label>Select Class</label>
                                <select class="form-control select2" id="searchdemo_class_id" name="searchcategory_id">
                                </select>
                        </div><!-- /.form-group -->    
                    </div>
                  
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Blog Category </h4>
        </div>
        <form id ="insDemoDate">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-6">
        <div class="form-group">
         <select class="form-control select2" id="demo_class_id" name="demo_class_id">
                    </select>
        </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
         <input type="date"  name="date" id="date" value="" class="form-control"  placeholder="Enter Category Name">
        </div>
        </div>
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Category </h4>
</div>
<form id ="editCategory">    
<div class="modal-body">
<div class="row">

<div class="col-md-6">
<div class="form-group">
<input type="hidden"  name="demo_date_id" id="demo_date_id" value="" class="form-control"/>
<input type="date"  name="editcate" id="editcate" value="" class="form-control"  placeholder="Enter Category Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="deleteCategory">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="demo_date_id" id="demo_date_id" value="" class="form-control"/>
        <input type="hidden"  name="Category" id="dCategory" value="" class="form-control"/>
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Category?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                            $('#insDemoDate').off("submit");
                            $('#insDemoDate').submit(function() {
                            
                            if(this.date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid date ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#date').focus();
                                $('#insDemoDate').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsDemoDate.php",
                        data:$('#insDemoDate').serialize(),
                        success: function(data){ 
                        onlad();
                        $('#insDemoDate').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    
         function onlad(demo_class_id){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelDemoDate.php",
                    data:{'demo_class_id':demo_class_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.demo_date_id]={};
                            cate[article.demo_date_id]["date"]= article.date;
                            var btn_class="btn-danger";
                            if(article.status=="Enable"){
                               btn_class="btn-success";
                            }
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.demo_date_id+" data-Category="+article.date+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                               .append($('<td/>').html($("<button>").addClass("btn "+btn_class+" btn-xs").attr({'type':'button'}).append(article.status).click(function (){
                                    updatestatus($(this),article.demo_date_id);
                                })))
                            );
                        });
                    }
                });
         }
             function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdDemoDateStatus.php",
                    data:{'demo_date_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
  $(document).on("click", "#edit", function () {
                       var demo_date_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#demo_date_id").val(demo_date_id);  
                        var d=cate[demo_date_id]["date"];
                        d=d.split("-").reverse().join("-");
                        $("#edit1").find("#editcate").val(d);          
                            $('#editCategory').off("submit");
                            $('#editCategory').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(this.editcate.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid date ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        
                        type:"post",
                        url:"../server/controller/UpdDemoDate.php",
                        data:$('#editCategory').serialize(),
                        success: function(data){ 
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var c_Id = $(this).data('id');
                 var Category = cate[c_Id]["date"];;
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#demo_date_id").val(c_Id);        
                    $("#delete1").find("#catname").text(Category); 
                    $('#deleteCategory').off("submit");
                    $('#deleteCategory').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelBlogCategoryDetails.php",
                            data:"demo_date_id="+c_Id,
                            success: function(data){ 
                            $('#deleteCategory').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

