<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Current Openings</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
              cate={}
               $(document).ready(function() {
             onlad();
            });  
            
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Current Openings</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Current Openings</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Openings</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th width="20%">Title</th>
                        <th width="10%">Date</th>
                        <th width="60%">Description</th>
                        <th>Apply</th>
                        <th>Action</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Current Openings</h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label for="student name">Opening Heading</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Heading here">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" id="date" name="date">
                        </div>    
                         <div class="form-group col-md-6" >
                            <label>Opening  Content</label>
                            <textarea class="form-control" rows="5" id="discription" name="discription"  placeholder="Content here" ></textarea>
                         </div>
                        <div class="form-group col-lg-6 col-sm-6">
                            <label for="question">Status</label>
                            <select class="form-control select2" id="status" name="status">
                                <option value="Enable">Enable</option>
                                <option value="Disable">Disable</option>
                            </select>
                        </div>
                        
                <!-- /.form-group -->
                <!-- /.form-group -->
              </div><!-- /.row -->
                
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
          
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Current Openings</h4>
        </div>
        <form id ="deleteCategory">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="current_openings_id" id="current_openings_id" value="" class="form-control"/>
        <p>Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                            $('#insertform').off("submit");
                            $('#insertform').submit(function() {
                            if(this.title.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid title").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#title').focus();
                                return false;  
                            }else if(this.date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text("Please fill Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                (this).focus();
                                return false;
                            }else if(this.discription.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill Discription").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#title').focus();
                                return false;  
                           }else {
                        
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsCurrentOpenings.php",
                        data:$('#insertform').serialize(),
                        success: function(data){ 
                            if(data.trim()!="Error"){
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                location.reload(true);
                            }else{
                                $(".validate").addClass("text-danger").fadeIn(100).text(" There is server error.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                            }
                            
                            
                        
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    
         function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelCurrentOpenings.php",
                   success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.title))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html(decodeURIComponent(article.discription)))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-primary btn-xs\" \")><i class=\"fa fa-edit\"></i    ></button>")).click(function (){
                                    //showComment(article.current_openings_id)
                                } )
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\" \")>Delete</button>").click(function(){
                                     deleteOpening(article.current_openings_id);
                                }))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.current_openings_id);
                                     
                                })))
                             );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var c_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#c_id").val(c_id);  
                        $("#edit1").find("#editcate").val(cate[c_id]["name"]);
                             $('#editCategory').off("submit");
                            $('#editCategory').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(this.cname.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#cname').focus();
                                $('#editCategory').each(function(){
                                this.reset();
                                return false;
                             });
                               
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/UpdImageCategory.php",
                        data:$('#editCategory').serialize(),
                        success: function(data){
//                            alert(data.trim());
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        function deleteOpening(current_openings_id){
            $("#delete1").modal("show")
                 $("#delete1").on('shown.bs.modal', function(){
                     $('#delete2').off("click");
                    $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelCurrentOpenings.php",
                            data:"current_openings_id="+current_openings_id,
                            success: function(data){ 
                            location.reload(true);
                                } 
                  });
                    return false;
    });
    
    });
   
}
 function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdCurrentOpenings.php",
                    data:{'current_openings_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
</script>
</html>

