<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Category Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
              cate={}
         $(document).ready(function() {
             onlad();
            });  
            
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active"> Categories</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title"> Categories</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add CategoryDetails</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Type</th>
                        <th>Category Name</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert CategoryDetails Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Category</h4>
        </div>
        <form id ="insCategoryDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-6">
                   <div class="form-group" id="category">
                        <label>Select Type</label>
                    <select class="form-control select2" id="type" name="type">
                        <option value="Youtube">Youtube</option>
                        <option value="Mp3">Mp3</option>
                        <option value="Mp4">Mp4</option>
                        <option value="Ppt">PPT</option>
                        <option value="PDF">PDF</option>
                        <option value="msword">MS Word</option>
                    </select>
                  </div><!-- /.form-group -->    

        </div><!-- /.col -->
        <div class="col-md-6">
        <div class="form-group">
         <label>Category</label>
         <input type="text"  name="category" id="category" value="" class="form-control"  placeholder="Enter Category Name">
        </div>
        <!-- /.form-group -->

        </div><!-- /.col -->
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert CategoryDetails Insert Modal End-->
       <!--Insert CategoryDetails Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Category Details </h4>
</div>
<form id ="editCategoryDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-6">
<div class="form-group">
<input type="hidden"  name="category_details_id" id="category_details_id" value="" class="form-control"/>
<input type="text"  name="editcate" id="editcate" value="" class="form-control"  placeholder="Enter Category">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert CategoryDetails Edit Modal End-->
         <!--Insert CategoryDetails Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Category Details </h4>
        </div>
        <form id ="deleteCategoryDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="category_details_id" id="category_details_id" value="" class="form-control"/>
        <input type="hidden"  name="category" id="category" value="" class="form-control"/>
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Category Details?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert CategoryDetails Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                            $('#insCategoryDetails').off("submit");
                            $('#insCategoryDetails').submit(function() {
                            if(this.category.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#category').focus();
                                $('#insCategoryDetails').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsCategoryDetails.php",
                        data:$('#insCategoryDetails').serialize(),
                        success: function(data){ 
                        onlad();
                        $('#insCategoryDetails').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    
         function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelCategoryDetails.php",
                    data:{'type':''},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.category_details_id]={};
                            cate[article.category_details_id]["category"]= article.category;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.type))
                                .append($('<td/>').html(article.category))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.category_details_id+" data-CategoryDetails="+article.category+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.category_details_id+" data-CategoryDetails="+article.category+"  id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                            );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var category_details_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#category_details_id").val(category_details_id);  
                        $("#edit1").find("#editcate").val(cate[category_details_id]["category"]);          
                            $('#editCategoryDetails').off("submit");
                            $('#editCategoryDetails').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(this.editcate.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/UpcategoryDetails.php",
                        data:$('#editCategoryDetails').serialize(),
                        success: function(data){ 
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var c_Id = $(this).data('id');
                 var CategoryDetails = cate[c_Id]["category"];;
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#category_details_id").val(c_Id);        
                    $("#delete1").find("#catname").text(CategoryDetails); 
                    $('#deleteCategoryDetails').off("submit");
                    $('#deleteCategoryDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelCategoryDetails.php",
                            data:"category_details_id="+c_Id,
                            success: function(data){ 
                            $('#deleteCategoryDetails').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

