<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Refer Friend</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
              onlad();
            });  
    </script> 
     
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
    <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
 <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Refer Friend</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Refer Friend</h3>
<!--                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addRefer Friend">Add Refer Friend</button>
                  </div>-->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        
                        <th width="3%">SN</th>
                        <th width="20%">Name</th>
                        <th width="15%">Email</th>
                        <th width="10%">Contact</th>
                        <th width="13%">Registered Date</th>
                        <th width="5%">By </th>
                      </tr>
                    </thead>
                    <tbody>
                        
                      
                    </tbody>
                    
                  </table>
                  </div>         
                   
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->
    <?php include 'includes/footer.php';?>
    <?php include 'includes/jslinks.php';?>
  <script type="text/javascript" language="javascript">
     /* Ajax call for Customer Display*/
           function onlad(){
                     $.ajax({
                    type:"post",
                    data:{'user_id':''},
                    url:"../server/controller/SelReferDetails.php",
                    success: function(data) {
                      $("body").append(data);
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            $("#data >tbody").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html(article.main_user_name))
                            );
                        });
                        
                    }
                });
         }
               </script>
<script type="text/javascript" language="javascript">
        /* Ajax call for Edit Customer */
         $(document).on("click", "#edit", function () {
                 var user_id = $(this).data('user_id');
                
                 $("#editcustomer").on('shown.bs.modal', function(){
                                    $("#editcustomer").find("#user_id").val(user_id);  
                                    $("#editcustomer").find("#user_name").val(cus[user_id]["user_name"]); 
                                    $("#editcustnmer").find(".select2").val(cus[user_id]["dob"]);
                                    $("#editcustomer").find("#email").val(cus[user_id]["email"]);  
                                    $("#editcustomer").find("#contact_no").val(cus[user_id]["contact_no"]);  
                                    $("#editcustomer").find("#date").val(cus[user_id]["date"]);  
                                    $("#editcustomer").find("#pic").val(cus[user_id]["pic"]);  
                                    $("#editcustomer").find("#user_profile_details_id").val(cus[user_id]["user_profile_details_id"]);  
                                    $("#editcustomer").find("#user_type").val(cus[user_id]["user_type"]);  
                                    $('#updateCustomer').submit(function() {
                                        
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                      var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     
                                        var passwordfilter = /^\d{4}$/;
                                        if(user_id=="1"){
                                            passwordfilter = /^\d{10}$/;
                                        }
                                     
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#user_name').focus();
                                        return false;
                                       }else if(this.email.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                           $('#updateCustomer').find('#email').focus();
                                         return false;
                                         
                                     }
                                     else if(this.pic.value == "" || !emailfilter.test(this.pic.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                          $('#updateCustomer').find('#pic').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.user_type.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#user_type').focus();
                                         return false;
                                         
                                     }
                                     else if(this.dob.value == "" || !genderfilter.test(this.dob.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                         $('#updateCustomer').find('#contact_no').focus();
                                         return false;
                                         
                                     }
                                     else if(this.date.value == "" || !passwordfilter.test(this.date.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);;
                                         $('#updateCustomer').find('#date').focus();
                                         return false;
                                         
                                     }
                                     else{
                                            $.ajax({
                                            type:"post",
                                            url:"../server/controller/updateCustomer.php",
                                            data:$('#updateCustomer').serialize(),
                                            success: function(data){ 
                                            $('#updateCustomer').each(function(){
                                            this.reset();
                                            location.reload(true);
                                            $('#editcustomer').modal('hide');
                                            return false;
                                            });
                                            } 
                                            });
                                            return false;
                                     }
                              
                      
                   
    });
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#editcustomer').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});
</script>
<script type="text/javascript" language="javascript">
 /* Ajax call for Delete Customer */
    $(document).on("click", "#delete", function () {
                 var user_id = $(this).data('user_id');
                        if(user_id===1){
                            $("#delete1").on('shown.bs.modal', function(){ 
                                $("#delete1").find(".modal-title").empty();   
                                $("#delete1").find(".modal-title").addClass("text-danger").append("Warning");   
                                $("#delete1").find("#msg").empty();   
                                   $("#delete1").find("#msg").append("Sorry, You can not delete admin.");   
                                   $("#delete1").find("#delete2").css({'display':'none'}); 
                                   
                               return false; 
                            });
                        }else{
                     $("#delete1").on('shown.bs.modal', function(){
                          $("#delete1").find(".modal-title").empty();
                          $("#delete1").find(".modal-title").append("Delete Customer");
                          $("#delete1").find("#msg").empty();  
                          $("#delete1").find("#msg").append("Sure to want to delete ?");  
                          $("#delete1").find(".modal-footer").find("#delete2").css({'display':''}); 
                    $("#delete1").find("#user_id").val(user_id);        
                        $('#deleteCustomer').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/deleteCustomer.php",
                            data:"user_id="+user_id,
                            success: function(data){ 
                                
                                $("#a").append(data);
                                if(data.trim()!="Error"){
                                     $('#deleteCustomer').each(function(){
                                this.reset();
                                        onlad()
                                        $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                }else{
                                     $('#confirm').modal('show');
                                }
                             
                                } 
                  });
                    return false;
    });
    
    });
                        }
       
   
});

  /* Code for Customer Delete end */  
  function showUserQuery(count,contact_no,email){
             $.ajax({
                type:"post",
                data:{'contact_no':contact_no,'email':email},
                url:"../server/controller/SelUserQuery.php",
                success: function(data) {
                  var duce = jQuery.parseJSON(data);
                    $("#show_query").find("#data_query tr:has(td)").remove();
                    $.each(duce, function (index, article) {
                      $("#show_query").find("#data_query").append($('<tr/>')
                            .append($('<td/>').html((index+1)))
                            .append($('<td/>').html(article.name))
                            .append($('<td/>').html(article.email))
                            .append($('<td/>').html(article.contact))
                            .append($('<td/>').html(article.subject))
                            .append($('<td/>').html(article.message))
                            .append($('<td/>').html(article.date))
                         );
                    });
                }
            });
  }
 
</script>
  </body>
</html>

