<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Banners</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          image={}
          $(document).ready(function(){
              onlad();
            });  
     </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS banners_idedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  banners_idebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left banners_ide column. contains the banners_idebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Banners</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Banners</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addImage">Add Photos</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Image</th>
                        <th width="50%">Content</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Banners Insert Modal Start-->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Images </h4>
        </div>
        <form id ="insBanners" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                        <label for="image">Image</label>
                        <div class="box-body">
                            <input type="file" name="file" id="file" required />
                        </div>
                     </div>
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->
        <div class="modal-footer">
        <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>

      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Image </h4>
        </div>
        <form id ="deleteimage">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="banners_id" id="banners_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>
  
<script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addImage").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                            $('#insBanners').off("submit");
                            $('#insBanners').submit(function() {
                            if(this.file.value == null){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please upload Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#category').focus();
                                $('#insBanners').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                            type:"post",
                            url:"../server/controller/InsBanners.php",
                            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data){
                                alert(data)
                            onlad();
                            $('#insBanners').each(function(){
                                    this.reset();
                                    return false;
                                 });
                                return false;
                            } 
                        });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    
         
  

</script>
<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var banners_id = $(this).data('banners_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#banners_id").val(banners_id);        
                         $('#deleteimage').off("submit");
                        $('#deleteimage').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelBanners.php",
                            data:"banners_id="+banners_id,
                            success: function(data){ 
                               
                        $('#deleteimage').each(function(){
                                this.reset();
                 onlad();
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................

          function onlad(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelBanners.php",
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            image[article.banners_id]={};
                            image[article.banners_id]["path"]=article.path;
                           var img= "../server/controller/"+article.path;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"200\" width=\"300\"/>"))
                                .append($('<td/>').append($("<input>").attr({'type':'text','id':'content','value':article.content}).css({'width':'80%'})).append($("<p>")).append($("<button>").addClass("btn btn-default btn-xs").append("Update").click(function (){
                                    updContent($(this).parent().find("input").val(),article.banners_id);
                                })))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-banners_id="+article.banners_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
                }
                
                function updContent(content,banners_id){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/updContent.php",
                    data:{'banners_id':banners_id,'content':content},
                    success: function(data) {
                        alert(data)
                    }
                    });
                }
                
</script>


</html>


