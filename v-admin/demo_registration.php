<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Demo Registration</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
      <script type="text/javascript">
          course_batch_details={}
          $(document).ready(function(){
              onlad();
            });  
      </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Demo Registration </li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Demo Registration </h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-3">
                          
                              
                        </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Demo Class</th>
                        <th>Demo Date</th>
                        <th colspan="2">Registration Code</th>
                        <th>Date of Registration</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Feedbacks </h4>
        </div>
        <form id ="deleteFeedback">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="sid" id="sid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>
  
    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var quick_query_id = $(this).data('quick_query_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#quick_query_id").val(quick_query_id);
                       $('#deleteFeedback').off("submit");
                        $('#deleteFeedback').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DellQuickQuery.php",
                            data:"quick_query_id="+quick_query_id,
                            success: function(data){ 
                               
                        $('#deleteFeedback').each(function(){
                                this.reset();
                                onlad();
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................
function onlad(){ 
    
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
//                    data:{'course_id':course_id},
                    url:"../server/controller/SelUserDemoDate.php",
                    success: function(data) {
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           var genrate_code="Generate Code"
                           var genrate_cls="btn-danger"
                           if(article.registered_temp_code!=null){
                               genrate_cls="btn-primary"
                               genrate_code="Resend"
                           } 
                            
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.gender))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.demo_class))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html(article.registered_temp_code))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs "+genrate_cls).append(genrate_code).click(function (){
                                    if(article.registered_temp_code!=null){
                                        reSend($(this),article.contact_no,article.registered_temp_code);
                                    }else{
                                        genrateCode($(this),article.user_registration_for_demo_id,article.contact_no);
                                    } 
                                })))
                                .append($('<td/>').html(article.date_fo_regs))
                             );
                        });
                    }
                });
                }
                
                
 function genrateCode(obj,user_registration_for_demo_id,contact_no){
    var registered_temp_code=Math.floor(100000 + Math.random() * 900000)+""+user_registration_for_demo_id;
    obj.html($("<img>").attr({'src':'../assets/images/fb_ty.gif'}))
            $.ajax({
                type:"post",
                url:"../server/controller/InsRegistrationCode.php",
                data:{'user_registration_for_demo_id':user_registration_for_demo_id,'registered_temp_code':registered_temp_code,'contact_no':contact_no},
                success: function(data){ 
                    onlad();
                } 
                  });
 }
 function reSend(obj,contact_no,registered_temp_code){
            obj.html($("<img>").attr({'src':'../assets/images/fb_ty.gif'}))
            $.ajax({
                type:"post",
                url:"../server/controller/CodeResend.php",
                data:{'registered_temp_code':registered_temp_code,'contact_no':contact_no},
                success: function(data){ 
                    onlad();
                } 
                  });
 }
</script>


</html>


