<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Notifications</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
            #data tr th{
                background:green;
                color:white;
                font-family:verdana;
                font-size:13px;
            }
            #data tr td{
                font-family:verdana;
                font-size:13px;
            }
            #data tr td a{
                color:red;
                text-decoration:underline;
                font-family:verdana;
            }
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Notifications</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Notifications</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addNotifications">Add Notifications</button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                      <table id="data" class="table table-bordered table-hover ">
                          <thead>
                              <tr>
                                  <th width="5%">S.N.</th>
                                  <th>Heading</th>
                                  <th>Link URL Address</th>
                                  <th>Date</th>
                                  <th colspan="2" style="text-align:center">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              
                          </tbody>
                              <tfoot>
                        
                          </tfoot>
                      </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insNotifications" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Notifications </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="Heading">Notifications Heading</label>
                            <input type="text" class="form-control" name="heading" placeholder="Heading here">
                        </div>
                  <!-- /.form-group -->
                 
                        <div class="form-group">
                        <label for="link">Notification Link</label>
                        <input type="text" class="form-control"  name="link">
                        </div>
                      
                </div><!-- /.col -->
                <!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
   <div class="modal fade" id="editNotifications" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit notifications </h4>
        </div>
        <form id ="updateform" enctype="multipart/form-data">    
        <div class="modal-body">
            <div class="box-body">
                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="Heading">Notifications Heading</label>
                            <input type="hidden" class="form-control" name="id" id="id" value="">
                            <input type="text" class="form-control" name="heading" id="heading" placeholder="Heading here" value="">
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group">
                            <label for="link">Notification Link</label>
                            <input type="text" class="form-control"  name="link" id="link"value="">
                        </div>
                    </div><!-- /.col -->
                <!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <div class="modal fade" id="delNotifications" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Notifications</h4>
        </div>
        <form id ="deleteNotifications">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="id" id="id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
    <div id="warningmsg2" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:orangered;color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warning</h4>
                </div>
                <div class="modal-body" id="errormsg">
                    <p id="msg" style="color:#000;font-family:century gothic;font-size:18px;font-weight:bold"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>





<div id="enquirysuccess2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;color:white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:gray;font-family:verdana;font-size:14px;line-height:22px">Notification Inserted Successfully</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  </body>
   <script>
         /* Select Notifications */  
      $(document).ready(function() {
             notifications ={};
             onLoad();
       });
      function onLoad(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelNotifications.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            notifications[article.id]={};
                            notifications[article.id]["id"]=article.id;
                            notifications[article.id]["heading"]=article.heading;
                            notifications[article.id]["link"]=article.link;
                            notifications[article.id]["date"]=article.date;
                            $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.heading))
                                .append($('<td/>').html("<a href="+article.link+" target=\"_blank\">"+article.link+"</a>"))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.id+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editNotifications\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delNotifications\"\")>Delete</button>"))
                             );
                        });
                    }
                });   
             }
  </script>
  <script>
        /* Insert Notifications */  
      $(document).ready(function() {
          $("#addNotifications").click(function(){
            $("#insNotifications").modal('show');}); 
                $("#insNotifications").on('shown.bs.modal', function(){
                
                    
        });
         $('#insertform').submit(function() { 
                 if(this.heading.value === "") {
//                    alert("Hello");
                 $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter 'Heading'.") ;
                      
                  });
                  this.heading.focus();
                  return false;
              }
              else if(this.link.value === "") {
                  //                         alert("Hello");
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter Link.") ;
                      
                  });
                  this.link.focus();
                  return false;
              }
              
              else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsNotifications.php",
                      data:$('#insertform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertform').each(function(){
                              this.reset();
                              
                          });
                          onLoad();
                      }
                      
                  });
              }
              
              return false;
          });
         
      });
  </script>
  <script>
      $(document).on("click", "#edit", function () {
          var id = $(this).data('id');
//          alert(id);
          $("#editNotifications").on('shown.bs.modal', function(){
              $("#editNotifications").find("#id").val(id);  
              $("#editNotifications").find("#heading").val(notifications[id]["heading"]);  
              $("#editNotifications").find("#link").val(notifications[id]["link"]);  
              $('#updateform').off("submit");
              $('#updateform').submit(function() {
                  if(this.heading.value === "") {
                      //                    alert("Hello");
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                          $("#errormsg").find("#msg").empty(); 
                          $("#errormsg").find("#msg").append("Please Enter 'Heading'.") ;
                          
                      });
                      this.heading.focus();
                      return false;
                  }
                  else if(this.link.value === "") {
                      //                         alert("Hello");
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                          $("#errormsg").find("#msg").empty(); 
                          $("#errormsg").find("#msg").append("Please Enter Link.") ;
                        });
                      this.link.focus();
                      return false;
                  }else{
                      $.ajax({
                          type:"post",
                          url:"../server/controller/UpdNotifications.php",
                          data:$('#updateform').serialize(),
                          success: function(data){
//                              alert(data.trim());
                              $('#updateform').each(function(){
                                  this.reset();
                                    $('#editNotifications').modal('hide');
                                    $('#enquirysuccess2').modal('show'); 
                                    $("#enquirysuccess2").on('shown.bs.modal', function(){
                                            $("#errormsg").find("#msg").empty(); 
                                            $("#errormsg").find("#msg").append("Notification Updated Successfully") ;
                                    }); 
                                    setTimeout(function(){
                                    $('#enquirysuccess2').modal('hide')
                                    }, 10000);
                                    $('#updateform').each(function(){
                                    this.reset();
                                    });
                                  onLoad();
                                  return false;
                              });
                          } 
                      });
                  }
                  
                  
                  return false;
              });
   
    });
});
  </script>
  <script>
     $(document).on("click", "#delete", function () {
                 var id = $(this).data('id');
               
                    $("#delNotifications").on('shown.bs.modal', function(){
                    $("#delNotifications").find("#id").val(id);
                        $('#deleteNotifications').off("submit");
                        $('#deleteNotifications').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelNotifications.php",
                            data:"id="+id,
                            success: function(data){ 
                            alert(data.trim()); 
                            $('#deleteNotifications').each(function(){
                               this.reset();
                            $('#delNotifications').modal('hide');
                             onLoad();
                            return false;
                         });
                      } 
                      });
                    
                    return false;
    });
    
    });
   
});
  
  </script>


</html>



