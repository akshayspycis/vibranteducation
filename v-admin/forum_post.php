<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Forum Post</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
    <?php include 'includes/links.php';?>
        <script type="text/javascript">
              cate={}
        topic={}         
            var text_editor;
        </script>
        <script type="text/javascript">
          pro={}
          category={}
          var cat_id;
          $(document).ready(function(){
                  $.ajax({
                        type:"post",
                        url:"../server/controller/SelForumCategoryDetails.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                            var a=$("<option></option>").append(article.forum_category).attr({'value':article.forum_category_details_id});
                            $("#search_category_id").find('#searchcategory_id').append(a);        
                            a=$("<option></option>").append(article.forum_category).attr({'value':article.forum_category_details_id});
                            $("#insForumPost").find('#forum_category_details_id').append(a);                    
                        });
                            $("#search_category_id").find('#searchcategory_id').trigger("change");
                            $("#insForumPost").find('#forum_category_details_id').trigger("change");
                        }
                     });
                     $("#search_category_id").find('#searchcategory_id').change(function () {
                            var id = $( this ).val();
                            showTopic(id,$("#search_topic_id").find('#searchtopic_id'),1)
                     }); 
                     $("#insForumPost").find('#forum_category_details_id').change(function () {
                            var id = $( this ).val();
                            showTopic(id,$("#insForumPost").find('#forum_topic_details_id'),0)
                     }); 
                     $("#search_topic_id").find('#searchtopic_id').change(function () {
                            var id = $( this ).val();
                            onlad(id)
                     }); 
                }); 
            function showTopic(id,that,a){
                $.ajax({
                            type:"post",
                            data:{'forum_category_details_id':id},
                            url:"../server/controller/SelForumTopicDetails.php",
                            success: function(data) { 
                                that.empty();
                            var duce = jQuery.parseJSON(data); //here data is passed to js object
                            $.each(duce, function (index, article) {
                                var a=$("<option></option>").append(article.topic_name).attr({'value':article.forum_topic_details_id});
                                that.append(a);        
                            });
                            if(a==1)
                                that.trigger("change");
                            }
                         });
            } 
            
        </script>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Forum Categories</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Forum Categories</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Post</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    <div class="row">
                <div class="col-md-3" id="search_category_id">
        <div class="form-group" id="">
                    <label>Select Category</label>
                    <select class="form-control select2" id="searchcategory_id" name="searchcategory_id">
                    </select>
                  </div><!-- /.form-group -->    
        </div>
                <div class="col-md-3" id="search_topic_id">
        <div class="form-group" id="">
                    <label>Select Topic</label>
                    <select class="form-control select2" id="searchtopic_id" name="searchtopic_id">
                    </select>
                  </div><!-- /.form-group -->    
        </div>
                          
        </div>
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Post Name</th>
                        <th>Date</th>
                        <th>User Name</th>
                        <th colspan="3">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Post Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Forum Post </h4>
        </div>
        <form id ="insForumPost">    
        <div class="modal-body">
        <div class="row">
        <div class="form-group col-md-6">
            <label for="student name">Forum Category</label>
            <select class="form-control select2" id="forum_category_details_id" name="forum_category_details_id">

            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="student name">Forum Category</label>
            <select class="form-control select2" id="forum_topic_details_id" name="forum_topic_details_id">

            </select>
        </div>
        <div class="form-group col-md-12" >
                    <label>Post</label>
                    <textarea class="form-control" rows="5" id="post"  name="post" style="resize:none" placeholder="Content here"></textarea>
                 </div><!-- /.form-group -->
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Post Insert Modal End-->
       <!--Insert Post Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Post </h4>
</div>
<form id ="editPost">    
<div class="modal-body">
<div class="row">

<div class="form-group col-md-12" >
                    <label>Post</label>
                    <textarea class="form-control" rows="5" id="editcate"  name="editcate" style="resize:none" placeholder="Content here"></textarea>
                 </div><!-- /.form-group -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Post Edit Modal End-->
         <!--Insert Post Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Post </h4>
        </div>
        <form id ="deletePost">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="forum_category_details_id" id="forum_category_details_id" value="" class="form-control"/>
        <input type="hidden"  name="Post" id="dPost" value="" class="form-control"/>
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Post?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Post Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                text_editor=CKEDITOR.replace("post");
                            $('#insForumPost').off("submit");
                            $('#insForumPost').submit(function() {
                              var forum_category_details_id=this.forum_category_details_id.value;
                              var forum_topic_details_id=this.forum_topic_details_id.value;
                            if(text_editor.getData() == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Post").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insForumPost').each(function(){
                                   this.reset();
                                    return false;
                                });
                             return false;  
                           }
                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                            type:"post",
                            url:"../server/controller/InsForumPostDetails.php",
                            data:{'forum_category_details_id':forum_category_details_id,'forum_topic_details_id':forum_topic_details_id,'post':encodeURIComponent(text_editor.getData()),'user_id':'<?php echo $_SESSION['user_id']; ?>','user_name':'<?php echo $_SESSION['user_name']; ?>'},
                            success: function(data){
                                location.reload(true);
                                return false;
                            } 
                        });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
         
         function onlad(forum_topic_details_id){
         topic={}
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelForumPostDetails.php",
                    data:{'forum_topic_details_id':forum_topic_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            topic[article.forum_post_details_id]=decodeURIComponent(article.post);
                            var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                            
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(decodeURIComponent(article.post)))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.forum_post_details_id+" data-Post="+article.topic_name+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.forum_post_details_id+" data-Post="+article.topic_name+"  id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.forum_post_details_id);
                                })))
                            );
                        });
                    }
                });
         }
         function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdForumPostStatus.php",
                    data:{'forum_post_details_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
  $(document).on("click", "#edit", function () {
                       var forum_post_details_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        text_editor=CKEDITOR.replace("editcate");
                        //$("#edit1").find("#forum_topic_details_id").val(forum_topic_details_id);  
                        text_editor.setData(topic[forum_post_details_id])
                            $('#editPost').off("submit");
                            $('#editPost').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(text_editor.getData()== ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }
                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                            type:"post",
                            url:"../server/controller/UpdForumPostDetails.php",
                            data:{'forum_post_details_id':forum_post_details_id,'post':encodeURIComponent(text_editor.getData())},
                            success: function(data){ 
                                 location.reload(true);
                            } 
                        });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var forum_post_details_id = $(this).attr('data-id');
                 var Post = topic[forum_post_details_id];
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#forum_category_details_id").val(forum_post_details_id);        
                    $("#delete1").find("#catname").text(Post); 
                    $('#deletePost').off("submit");
                    $('#deletePost').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelForumPostDetails.php",
                            data:"forum_post_details_id="+forum_post_details_id,
                            success: function(data){ 
                            location.reload(true);
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

