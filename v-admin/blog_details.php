<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Blog Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          blog={}
      </script>
      
      <script type="text/javascript">
          pro={}
          category={}
          var cat_id;
          $(document).ready(function(){
                  $.ajax({
                        type:"post",
                        url:"../server/controller/SelBlogCategoryDetails.php",
                        success: function(data) { 
                            var duce = jQuery.parseJSON(data); //here data is passed to js object
                        category={}
                        $.each(duce, function (index, article) {
                        category[article.category_id]=article.category;    
                            var a=$("<option></option>").append(article.category).attr({'value':article.blog_category_details_id});
                            $("#search_category_id").find('#searchcategory_id').append(a);                    
                            a=$("<option></option>").append(article.category).attr({'value':article.blog_category_details_id});
                            $("#insertform").find('#category_id').append(a);                    
                        });
                            $("#search_category_id").find('#searchcategory_id').trigger("change");
                        }
                     });
                     $("#search_category_id").find('#searchcategory_id').change(function () {
                var id = $( this ).val();
                onlad(id)
             }); 
                }); 
             
            
        </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Blog Details & Updates</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Blog Details & Updates</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addBlogDetails">Add Blog Details</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    <div class="row">
                <div class="col-md-3" id="search_category_id">
        <div class="form-group" id="category">
                    <label>Select Category</label>
                    <select class="form-control select2" id="searchcategory_id" name="searchcategory_id">
                    </select>
                  </div><!-- /.form-group -->    
        </div>
                          
        </div>
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Title</th>
                        <th width="20%">Short Description</th>
                        <th width="30%">Long Description</th>
                        <th>Date</th>
                        <th>Image</th>
                        <th  ><i class="fa fa-comment"></i</th>
                        <th  style="text-align:center">Action</th>
                        <th style="text-align:center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insBlogDetails" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Blog Details </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                
                        <div class="row">
                        <div class="form-group col-md-6">
                            <label for="student name">Blog Category</label>
                            <select class="form-control select2" id="category_id" name="category_id">

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="student name">Blog Heading</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Heading here">
                        </div>
                        <div class="form-group col-md-6">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" id="date" name="date">
                        </div>    
                        <div class="form-group col-lg-6 col-sm-6">
                            <label for="question">Status</label>
                            <select class="form-control select2" id="status" name="status">
                                <option value="Enable">Enable</option>
                                <option value="Disable">Disable</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="image">Add Image</label>
                        <div class="box-body">
                          <div class="uploadArea" id="dragAndDropFiles" style="min-height: 60px;">
                              <h1 style="font-size: 26px;">Drop Images Here</h1>
                                  <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                          </div>
                        </div>
                        <p class="help-block"></p>
                       </div>
                
                 <div class="form-group col-md-6" >
                    <label>Blog Short Content</label>
                    <textarea class="form-control" rows="5" id="short_description" name="short_description" style="resize:none" placeholder="Content here" maxlength="300"></textarea>
                 </div><!-- /.form-group -->
                 <div class="form-group col-md-6" >
                     <label><i class="fa fa-facebook"></i>&nbsp;Facebook Link</label>
                    <textarea class="form-control" rows="5" id="fb" name="fb" style="resize:none" placeholder="Content here" maxlength="300"></textarea>
                 </div><!-- /.form-group -->
                 <div class="form-group col-md-6" >
                    <label><i class="fa fa-twitter"></i>&nbsp;Twitter Link</label>
                    <textarea class="form-control" rows="5" id="tw" name="tw" style="resize:none" placeholder="Content here" maxlength="300"></textarea>
                 </div><!-- /.form-group -->
                 <div class="form-group col-md-12" >
                    <label>Blog Long Content</label>
                    <textarea class="form-control" rows="5" id="long_description"  name="long_description" style="resize:none" placeholder="Content here"></textarea>
                 </div><!-- /.form-group -->
              </div><!-- /.row -->
                
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
   <div class="modal fade" id="editBlog Details" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit blog </h4>
        </div>
        <form id ="updateform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="blog heading">Name</label>
                            <input type="hidden" class="form-control" name="blog_details_id" id="blog_details_id" value="">
                            <input type="text" class="form-control" name="nheading" id="nheading" placeholder="Heading here">
                        </div>
                  <!-- /.form-group -->
                 
                        <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control"  name="ndate" id="ndate">
                        </div>
                        <div class="form-group">
                         <div class="thumbnail col-md-12">
                                <a class="close" id="close_image" data-toggle="tooltip" title="Remove Image" href="#">×</a>
                                <span  id= "imgg" ></span>
                            </div>
                        </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                 <div class="form-group">
                    <label>Blog Details Content</label>
                    <textarea class="form-control" rows="5" id="ncontent"  name="ncontent" style="resize:none" placeholder="Content here"></textarea>
                  </div><!-- /.form-group -->
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Blog Details</h4>
        </div>
        <form id ="deleteBlogDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="blog_details_id" id="blog_details_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
      <div class="modal fade" id="showcooment" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Blog Comment's Details</h4>
        </div>
        <form id ="deleteBlogDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12" >
         <table id='comment_model' class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>User Name</th>
                        <th width="80%">Comment</th>
                        <th style="text-align:center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
         </div>
        </div>
        
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>
  <script type="text/javascript" language="javascript">
      var obj_class=null;
      var text_editor;
      $(document).ready(function(){
        $("#addBlogDetails").click(function(){
            
            $("#insBlogDetails").modal('show');}); 
                $("#insBlogDetails").on('shown.bs.modal', function(){
                 text_editor=CKEDITOR.replace("long_description");
                var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "insertform", // Form ID
                        dragArea: "dragAndDropFiles", // Upload Area ID
                        multiUpload:"multiUpload",
                        url:"../server/controller/InsBlogDetails.php",
                        onlad:onlad,
                        strlenght:25,
                        module:1
                        }
                        if(obj_class==null){
                           obj_class=new $.UploadImg(obj);           
                        }
   
    });
   }); 
         
  /* Code for Product Edit   start */  
  var upd_img=false;
  // this when we want to update image above var is set value;
$(document).on("click", ".close", function () {
    var blog_details_id=$(this).attr('id');
    $(this).next('span').empty();
    var form = $("<form>").attr('id','update_img')
    var a=$("<div>").addClass("uploadArea").attr('id','dragAndDropFiles1').css('min-height','97px')
        .append($("<h1>").css('font-size','16px').append("Drop Images Here"))
        .append($("<input/>").css({'width':'0px','height':'0px','overflow':'hidden'}).attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':'multiUpload1'}))
         var upd_button=$("<button>").attr({'type':'button','class':'btn btn-default btn-sm'}).append($("<i>").addClass("fa fa-upload "))
                 .css({'margin-bottom':'5px'})
         form.append(upd_button)
         form.append(a)
         $(this).next('span').append(form);
         var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "update_img", // Form ID
                        dragArea: "dragAndDropFiles1", // Upload Area ID
                        multiUpload:"multiUpload1",// input field id
                        url:"../server/controller/UpdBlogDetails.php",//location call controller
                        onlad:onlad,
                        blog_details_id:blog_details_id,
                        upd_button:upd_button,
                        strlenght:25,
                        module:1
                        }
                      new $.UploadImg(obj);           
});

  $(document).on("click", "#edit", function () {
                 var blog_details_id = $(this).data('blog_details_id');
                 
                 $("#editBlog Details").on('shown.bs.modal', function(){
                                    $("#editBlog Details").find("#blog_details_id").val(blog_details_id);  
                                    $("#editBlog Details").find("#nheading").val(blog[blog_details_id]["nheading"]);  
                                    $("#editBlog Details").find("#ndate").val(blog[blog_details_id]["ndate"]);  
                                    $("#editBlog Details").find("#nimage").val(blog[blog_details_id]["nimage"]);  
                                    $("#editBlog Details").find("#ncontent").val(blog[blog_details_id]["ncontent"]);  
                                     var img= "../server/controller/"+blog[blog_details_id]["nimage"];
                                     $("#editBlog Details").find("#close_image").attr({'id':blog_details_id});
                                     $("#editBlog Details").find("#imgg").empty();
                                    $("#editBlog Details").find("#imgg").append("<img src="+img+" height=\"100\" width=\"100\"/>");  
                                    $('#updateform').submit(function() {
                        $.ajax({
                        type:"post",
                        url:"../server/controller/updateBlogDetails.php",
                        data:$('#updateform').serialize(),
                        success: function(data){
                            onlad();
                          $('#updateform').each(function(){
                            this.reset();
                            $('#editBlog Details').modal('hide');
                            return false;
                                    });
                              } 
                            });
                   
                   return false;
    });
   
    });
});
  /* Code for Product Edit   start */  
</script>

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var blog_details_id = $(this).data('blog_details_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#blog_details_id").val(blog_details_id);        
                        $('#deleteBlogDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelBlogDetails.php",
                            data:"blog_details_id="+blog_details_id,
                            success: function(data){ 
                            $("#search_category_id").find('#searchcategory_id').val(blog[blog_details_id]["category_id"]).trigger("change");
                        $('#deleteBlogDetails').each(function(){
                            this.reset();
                            $('#delete1').modal('hide');
                            return false;
                        });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................
jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {	
                       
                             var inputId = $("#"+this.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <2){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
//                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
//                                that._deleteFiles($(this).parent().parent().attr('rel'));
//                                $(this).parent().parent().remove();
//                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                
                                if(that.up_obj.upd_button==null){
                                   
                                }
                                
                            }
                            $("#"+this.up_obj.dragArea).empty();
                            
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 1 files.")
                            }
                        }
                     
                        
                this.setStream = function(stream,blog_details_id){
                    that.all[blog_details_id]["byte_stream"]=stream;
                    $(".dfiles[rel='"+blog_details_id+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                   if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>')
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        
                      var blog = {};  
                      if(that.up_obj.upd_button==null){
                              $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="title"){
                                        blog["title"]= $(this).val();
                                    }
                                    
                                    if($(this).attr("name")=="date"){
                                        blog["date"]= $(this).val();
                                    }
                                    if($(this).attr("name")=="short_description"){
                                        blog["short_description"]= $(this).val();
                                    }
                                    if($(this).attr("name")=="fb"){
                                        blog["fb"]= $(this).val();
                                    }
                                    if($(this).attr("name")=="tw"){
                                        blog["tw"]= $(this).val();
                                    }
                                    
                                    if($(this).attr("name")=="category_id"){
                                        blog["category_id"]= $("#category_id").val();
                                    }
                                    if($(this).attr("name")=="status"){
                                        blog["status"]= $(this).val();
                                    }
                               });
                               blog["user_id"]= '1';
                               blog["long_description"]= encodeURIComponent(text_editor.getData());
                      }else{
                          blog['blog_details_id']=that.up_obj.blog_details_id;
                      }
                        
                      
                      blog["nimage"] =file["byte_stream"] ;
                            $.ajax({
                            type:"post",
                            url:that.up_obj.url,
                            data:blog,
                            success: function(data){
                                alert(data)
                             if(that.up_obj.upd_button!=null){
                                  that.up_obj.upd_button.empty();
                                         that.up_obj.upd_button.append($("<i>").addClass("fa fa-check-square-o"))
                                         that.up_obj.onlad();
                                     }else{
                                       
                                          $("#dragAndDropFiles").find("canvas").remove();
                                          $("#dragAndDropFiles").append('<h1 style="font-size: 26px;">Drop Images Here</h1><input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">');
                                            that._deleteFiles(key);
                                            $(".dfiles[rel='"+ids+"']").remove();
                                            //
                                        $('#insertform').each(function(){
                                            this.reset();
                                            $("#search_course_id").val(blog["category_id"]);
                                            $("#search_category_id").find('#searchcategory_id').val(blog["category_id"]).trigger("change");;
                                            text_editor.setData("");
                                            return false;
                                        });
                                     }   
                                    } 
                                });
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
   
                this._startUpload = function(){
                    if(that.up_obj.upd_button==null){
                        
                            var reg = /^\\d+$/;
                            var b=true;
                            $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="nheading" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill  Heading").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                     if($(this).attr("name")=="ndate" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                                    if($(this).attr("name")=="ncontent" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill Blog Details Detail").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                            });

                            if(b==false){
                            return b;
                            }
                            if(Object.keys(that.all).length==0){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please upload  Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        return false;
                            }else{
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }else{
                            if(Object.keys(that.all).length==1){
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }
                    
                    
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,blog_details_id) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,blog_details_id),blog_details_id)
              }
            };
        }

                this.readfiles=function(files,blog_details_id) {
                    that.processfile(files,blog_details_id); // process each file at once
                }

                this.resizeMe=function (img,blog_details_id) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[blog_details_id]["image"]=canvas1;
             
                  var canvas2 = document.createElement('canvas');
              canvas2.width =100;
              canvas2.height = 100;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 100, 100);
                                $("#"+this.up_obj.dragArea).append(canvas2);
                                 if(that.up_obj.upd_button!=null){
                                that.up_obj.upd_button.click(function (){
                                    that.up_obj.upd_button.empty();
                                    that.up_obj.upd_button.append('<img src="../style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>');
                                   that._startUpload()
                                });
                                }
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
function onlad(category_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelBlogDetails.php",
                    data:{'category_id':category_id},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                           var img= "../server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.title))
                                .append($('<td/>').html(article.short_description))
                                .append($('<td/>').html(decodeURIComponent(article.long_description)))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-primary btn-xs\" \")><i class=\"fa fa-edit\"></i    ></button>")).click(function (){
                                    showComment(article.blog_details_id)
                                } )
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-blog_details_id="+article.blog_details_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.blog_details_id);
                                     
                                })))
                             );
                        });
                    }
                });
                }
                function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdBlogStatus.php",
                    data:{'blog_details_id':id,'status':value},
                    success: function(data) {
                        
                    }
                  });
              }
                function updateComment(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdBlogRplyDetialsStatus.php",
                    data:{'blog_rply_details_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
                function showComment(blog_details_id){
                                     $("#showcooment").modal('toggle');
                                     $("#showcooment").off('shown.bs.modal');
                                     $("#showcooment").on('shown.bs.modal', function(){
                                            $.ajax({
                                            type:"post",
                                            url:"../server/controller/SelBlogRplyDetails.php",
                                            data:{'blog_details_id':blog_details_id},
                                            success: function(data){
                                                $("#comment_model").empty();
                                                var duce = jQuery.parseJSON(data);
                                                $.each(duce, function (index, article) {
                                                    var btn_class="btn-danger";
                                                   if(article.status=="Enable"){
                                                           btn_class="btn-success";
                                                   }
                                                   $("#comment_model").append($('<tr/>')
                                                        .append($('<td/>').html((index+1)))
                                                        .append($('<td/>').html(article.user_name))
                                                        .append($('<td/>').html(article.comment))
                                                        .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status",'type':'button'}).append(article.status).click(function(){
                                                             updateComment($(this),article.blog_rply_details_id);
                                                        })))
                                                     );
                                                });
                                               } 
                                            });
                        });
              }
            
</script>


</html>

