<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>User Payment Details</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
      <script type="text/javascript">
          course_batch_details={}
          $(document).ready(function(){
              onlad();
            });  
      </script>
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">User Payment Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">User Payment Details</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-3">
                     
                        </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>User Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Payment</th>
                        <th>Paid</th>
                        <th>Balance</th>
                        <th colspan="3">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

         <div class="modal fade" id="editPayment" role="dialog">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Payment</h4>
        </div>
        <form id ="updatePayment" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                <div class="box">
               <div class="box-body">
                        <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="blog heading">Payment</label>
                            <input type="hidden" class="form-control" name="user_payment_details_id" id="user_payment_details_id" value="">
                            <input type="text" class="form-control" name="payment" id="payment" placeholder="Payment here">
                        </div>
                        <div class="form-group">
                            <label for="blog heading">Paid</label>
                            <input type="text" class="form-control" name="paid" id="paid" placeholder="Paid here">
                        </div>
                        <div class="form-group">
                            <label for="blog heading">Balance</label>
                            <input type="text" class="form-control" name="balance" id="balance" placeholder="Balance here" readonly>
                        </div>
                        
                </div><!-- /.col -->
                
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

      
   
  </body>
  
    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  

//...........................................................................................................

var user_payment_details={}
function onlad(){ 
    
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
//                    data:{'course_id':course_id},
                    url:"../server/controller/SelUserPaymentDetails.php",
                    success: function(data) {
                        user_payment_details={}
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            user_payment_details[article.user_payment_details_id]=article;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.gender))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.payment))
                                .append($('<td/>').html(article.paid))
                                .append($('<td/>').html(article.balance))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs btn-primary").append("Edit").click(function (){
                                    showEdit(article.user_payment_details_id);
                                })))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs btn-primary").append("Send Message").click(function (){
                                    sendMsg($(this),article.user_payment_details_id);
                                })))
                                .append($('<td/>').html($("<button>").addClass("btn btn-xs btn-primary").append("Send Reciept").click(function (){
                                    sendReciept($(this),article.user_payment_details_id);
                                })))
                                .append($('<td/>').html(article.date_fo_regs))
                             );
                        });
                    }
                });
                }
                
function showEdit(user_payment_details_id){
$("#editPayment").modal('toggle');
         $("#editPayment").on('shown.bs.modal', function(){
                    $("#editPayment").find("#user_payment_details_id").val(user_payment_details_id);        
                    $("#editPayment").find("#payment").val(user_payment_details[user_payment_details_id].payment);        
                    $("#editPayment").find("#paid").val(user_payment_details[user_payment_details_id].paid);        
                    $("#editPayment").find("#balance").val(user_payment_details[user_payment_details_id].balance);        
                        $('#updatePayment').off();
                        $('#updatePayment').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/UpdUserPaymentDetails.php",
                            data:$("#updatePayment").serialize(),
                            success: function(data){ 
                                $("#editPayment").modal('hide');
                                onlad();
                            } 
                        });
                        return false;
                    });
    
    });

}                

   $("#editPayment").find("#payment").keyup(function (e){
     cal();
   })
   $("#editPayment").find("#paid").keyup(function (e){
     cal();
   })
function cal(){
    $("#editPayment").find("#balance").val($("#editPayment").find("#payment").val().trim()-$("#editPayment").find("#paid").val().trim());
}

function sendMsg(obj,user_payment_details_id){
            obj.html($("<img>").attr({'src':'../assets/images/fb_ty.gif'}))
            var message ="Dear Student your remaining balance fees is "+user_payment_details[user_payment_details_id].balance+" Rs."
                    +" Payment Detail  Fess : "+user_payment_details[user_payment_details_id].payment+" Rs"
                    +" Paid : "+user_payment_details[user_payment_details_id].paid+" Rs";
            $.ajax({
                type:"post",
                url:"../server/controller/PaymentMessage.php",
                data:{'contact_no':user_payment_details[user_payment_details_id].contact_no,'message':message},
                success: function(data){ 
                    onlad();
                } 
                  });
 }
 
function sendReciept(obj,user_payment_details_id){
            obj.html($("<img>").attr({'src':'../assets/images/fb_ty.gif'}));
            $.ajax({
                type:"post",
                url:"../server/controller/SendReceipt.php",
                data:{'registered_temp_code':user_payment_details[user_payment_details_id].registered_temp_code,
                    'paid':user_payment_details[user_payment_details_id].paid,
                    'balance':user_payment_details[user_payment_details_id].balance,
                    'user_name':user_payment_details[user_payment_details_id].user_name,
                    'email':user_payment_details[user_payment_details_id].email,
                    'date':user_payment_details[user_payment_details_id].date},
                success: function(data){ 
                    obj.html("Send Reciept");
                } 
            });
 }


</script>


</html>


