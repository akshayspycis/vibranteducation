<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Exam Pattern</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     
    <style>
#upload-wrapper {
	    background: #828b82;
    padding: 50px;
    border-radius: 10px;
}
#upload-wrapper h3 {
	padding: 0px 0px 10px 0px;
	margin: 0px 0px 20px 0px;
	margin-top: -30px;
	border-bottom: 1px dotted #DDD;
}
#upload-wrapper input[type=file] {
	padding: 6px;
	background: #FFF;
	border-radius: 5px;
}
#upload-wrapper #submit-btn {
	border: none;
	padding: 10px;
	background: #61BAE4;
	border-radius: 5px;
	color: #FFF;
}
#output{
	padding: 5px;
	font-size: 12px;
}

/* prograss bar */
#progressbox {
	border: 1px solid #CAF2FF;
	padding: 1px; 
	position:relative;
	width:400px;
	border-radius: 3px;
	margin: 10px;
	display:none;
	text-align:left;
}
#progressbar {
	height:20px;
	border-radius: 3px;
	background-color: #CAF2FF;
	width:1%;
}
#statustxt {
	top:3px;
	left:50%;
	position:absolute;
	display:inline-block;
	color: #FFFFFF;
}

    </style>
  </head>
  
  <body class="skin-blue  skin-blue  sidebar-mini sidebar-collapse ">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Exam Pattern Analysis</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exams</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addExams">Add Exams</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name of Exam</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exams Sub Categories</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addSubCat">Add Sub Category</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data2" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Exam Name </th>
                        <th>Sub Cat</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
             <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exam Pattern</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addExamPhase">Add</button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data3" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Exam Name </th>
                        <th>Sub Cat</th>
                        <th>Phase</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
     <!-- Modal TO Insert Exams -->
    <div class="modal fade" id="insExams" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams </h4>
                </div>
                <form id ="insertform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="Name">Exam Name</label>
                                                    <input type="text" class="form-control" name="exam_name" id="exam_name" placeholder="Name here">
                                                </div>
                                                <!-- /.form-group -->
            
            
            
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
      <!-- Modal TO Edit Exams -->
    <div class="modal fade" id="editExams" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Exams </h4>
                </div>
                <form id ="updateExams" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Name">Exams Name</label>
                                        <input type="hidden" class="form-control" name="exam_id" id="exam_id" placeholder="Name here" value="">
                                        <input type="text" class="form-control" name="exam_name" id="exam_name" placeholder="Name here" value="">
                                    </div>
                                    <!-- /.form-group -->
            
                                </div><!-- /.col -->
                                <!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" id="edit2" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
       <!-- Modal TO Delete Exams -->
    <div class="modal fade" id="delExams" role="dialog">
        <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Exams</h4>
                </div>
                <form id ="delExamsForm">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden"  name="id" id="id" value="" class="form-control"/>
                                <input type="hidden"  name="path" id="path" value="" class="form-control"/>
                                <p id ="msg">Sure to want to delete ?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
       <!-- Modal TO Insert Sub Cat Exams -->
   <div class="modal fade" id="insExamsSubCat" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams Sub Category </h4>
                </div>
                <form id ="insertExamSubCatform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <input type="text" class="form-control" name="exam_sub_cat_name" id="exam_sub_cat_name" placeholder="Name here">
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
       
       
         <!-- Modal TO Insert Exams Phases -->
   <div class="modal fade" id="insExamsPhase" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams Phase </h4>
                </div>
                <form id="insertExamSPhaseform" action="../server/controller/InsExamsPhases.php" method="post" enctype="multipart/form-data" >
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                        <div  class="col-md-12">
                            <div align="center" id="upload-wrapper">
                                    <h3>File Uploader</h3>
                                    <input name="FileInput" id="FileInput" type="file" />
                                    <img src="../test/ajax/images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
                                    <div id="progressbox" ><div id="progressbar"></div ><div id="statustxt">0%</div></div>
                                    
                            </div>
                            <div id="output"></div>
                        </div>
                    </div><!-- /.row -->
                    <div class="modal-footer">
                        <div class="validate pull-left" style="font-weight:bold;"></span></div>
                        <button type="submit" id="submit-btn" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
 <!-- Modal TO Success -->        
    <div id="enquirysuccess2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;color:white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:gray;font-family:verdana;font-size:14px;line-height:22px"> Inserted Successfully</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
        <!-- Modal TO Warning -->
    <div id="warningmsg2" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:orangered;color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warning</h4>
                </div>
                <div class="modal-body" id="errormsg">
                    <p id="msg" style="color:#000;font-family:century gothic;font-size:18px;font-weight:bold"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>
         <!-- Modal TO Insert Exams Sections -->
  </body>
  <script>
      $(document).ready(function() {
          $("#addExams").click(function(){
            $("#insExams").modal('show');}); 
      });
         $("#insExams").on('shown.bs.modal', function(){
         });
          $('#insertform').submit(function() { 
                 if(this.exam_name.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                      
                  });
                  this.exam_name.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExams.php",
                      data:$('#insertform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExams();
                      }
                      
                  });
              }
              
              return false;
          });
  </script>
  <script>
      /* Select Exams */  
      $(document).ready(function() {
             exams ={};
             examssubcategory={};
             examsphases ={};
             examssections ={};
             prelimsexam ={};
             mainsexam ={};
             discriptivetest ={};
             groupdiscussion ={};
             onLoadExams();
             onLoadExamsSubCat();
             onLoadExamsphases();
       });
      function onLoadExams(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExams.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            exams[article.id]={};
                            exams[article.id]["exam_id"]=article.exam_id;
                            exams[article.id]["exam_name"]=article.exam_name;
                            $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exams_row'}).append("Edit").click(function (){edit_exams_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_exams_row'}).append("Delete").click(function (){delete_exams_row(article.exam_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
  <script>
       function edit_exams_row(exam_id,exam_name){
                 $("#editExams").modal('show'); 
                 $('#edit2').off();
                  $("#updateExams").find("#exam_id").val(exam_id);  
                  $("#updateExams").find("#exam_name").val(exam_name);  
                  $('#updateExams').submit(function(){
                    
                      if(this.exam_name.value === ""){
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                       $("#errormsg").find("#msg").empty(); 
                          $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                             });
                      this.exam_name.focus();
                      return false;
                  }else{
                       $.ajax({
                            type:"post",
                            url:"../server/controller/UpdExams.php",
                            data:$('#updateExams').serialize(),
                            success: function(data){
//                            alert(data.trim());
                            onLoadExams();
                            $("#editExams").modal('hide');; 
                      } 
                      
                      });
                  }
                        
                    
                    return false;
    });
      }
       function delete_exams_row(exam_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExams.php",
                            data:{"exam_id":exam_id},
                            success: function(data){
//                            alert(data.trim());
                            onLoadExams();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
  </script>
  <!--For Sub Cat Start-->
   <script>
      $(document).ready(function() {
          $("#addSubCat").click(function(){
            $("#insExamsSubCat").modal('show');}); 
          });
         $("#insExamsSubCat").on('shown.bs.modal', function(){
         });
          $('#insertExamSubCatform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id});
                        $('#insertExamSubCatform').find('#selExamName').append(a);                    
                        });
                        }
                        }); 
              $('#insertExamSubCatform').submit(function() { 
                 if(this.exam_sub_cat_name.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                      
                  });
                  this.exam_name.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExamsSubCategory.php",
                      data:$('#insertExamSubCatform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertExamSubCatform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExamsSubCat();
                      }
                      
                  });
              }
              
              return false;
          });
  </script>
  <script>
      /* Select Exams for Sub Cat */  
      function onLoadExamsSubCat(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsSubCategory.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data2 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examssubcategory[article.id]={};
                            examssubcategory[article.id]["exam_sub_cat_id"]=article.exam_sub_cat_id;
                            examssubcategory[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            examssubcategory[article.id]["exam_name"]=article.exam_name;
                            $("#data2").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
//                                .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_exam_sub_cat_row'}).append("Delete").click(function (){delete_exam_sub_cat_row(article.exam_sub_cat_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
   <script>
      function delete_exam_sub_cat_row(exam_sub_cat_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsSubCategory.php",
                            data:{"exam_sub_cat_id":exam_sub_cat_id},
                            success: function(data){
//                            alert(data.trim());
                            onLoadExamsSubCat();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
   <!--For Sub Cat End-->
    <!--For Phases Start-->
  <script>
      $(document).ready(function() {
          $("#addExamPhase").click(function(){
            $("#insExamsPhase").modal('show');}); 
         });
         $("#insExamsPhase").on('shown.bs.modal', function(){
                
          });
                $('#insertExamSPhaseform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertExamSPhaseform').find('#selExamName').append(a);                    
                        });
                        $('#insertExamSPhaseform').find('#selExamName').trigger("change");
                        }
                        });
                
              
                 $('#insertExamSPhaseform').find('#selExamName').change(function()
                 {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertExamSPhaseform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertExamSPhaseform').find('#selExamSubCat').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
            
  </script>
   <script>
      /* Select Exams Phases */  
      function onLoadExamsphases(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsPhases.php",
                    success: function(data) {
//                       
                       var duce = jQuery.parseJSON(data);
                        $("#data3 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examsphases[article.id]=article;
                            var path=$('<iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/'+article.exam_phase_name+'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>')
                            $("#data3").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(path))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_examphases_row'}).append("Delete").click(function (){delete_examphases_row(article.exam_phase_id); })))
                             );
                        });
                    }
                });   
             }
  </script>  
     <script>
      function delete_examphases_row(exam_phase_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsPhases.php",
                            data:{"exam_phase_id":exam_phase_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadExamsphases();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  
  <!--Exam Section Begin-->
  <script>
      $(document).ready(function() {
          $("#addExamSections").click(function(){
            $("#insExamsSections").modal('show');}); 
         });
         $("#insExamsSections").on('shown.bs.modal', function(){
                
          });
                $('#insertExamsSectionform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertExamsSectionform').find('#selExamName').append(a);                    
                        });
                        $('#insertExamsSectionform').find('#selExamName').trigger("change");
                        }
                        });
                  $('#insertExamsSectionform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertExamsSectionform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertExamsSectionform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertExamsSectionform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertExamsSectionform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertExamsSectionform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertExamsSectionform').find('#selExamPhase').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
                $('#insertExamsSectionform').submit(function() { 
                   if(this.selExamName.value === "") {
                       
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Name'.") ;
                      
                  });
                  this.selExamName.focus();
                  return false;
              }
                 else if(this.selExamSubCat.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Sub Category'.") ;
                      
                  });
                  this.selExamSubCat.focus();
                  return false;
              }
                 else if(this.selExamPhase.value === ""){
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Phase'.") ;
                   });
                  this.selExamPhase.focus();
                  return false;
              }else if(this.exam_section_name.value === "") {
                      alert("Please Enter Section Name");
                  this.exam_section_name.focus();
                  return false;
                  
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExamsSections.php",
                      data:$('#insertExamsSectionform').serialize(),
                      success: function(data){
                             alert(data.trim());
                          $('#insertExamsSectionform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExamsSections();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
     <script>
      /* Select Exams Sections */  
      function onLoadExamsSections(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsSections.php",
                    data:{exam_phase_id:''},
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data4 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examssections[article.id]={};
                            examssections[article.id]["exam_section_id"]=article.exam_section_id;
                            examssections[article.id]["exam_name"]=article.exam_name;
                            examssections[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            examssections[article.id]["exam_phase_name"]=article.exam_phase_name;
                            examssections[article.id]["exam_section_name"]=article.exam_section_name;
                            $("#data4").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').html(article.exam_section_name))
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_examphases_row'}).append("Delete").click(function (){delete_examsections_row(article.exam_section_id); })))
                             );
                        });
                    }
                });   
             }
  </script>  
     <script>
      function delete_examsections_row(exam_section_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsSections.php",
                            data:{"exam_section_id":exam_section_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadExamsSections();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  
<script type="text/javascript">
$(document).ready(function() { 
	var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			success:       afterSuccess,  // post-submit callback 
			uploadProgress: OnProgress, //upload progress callback 
			resetForm: true        // reset the form after successful submit 
		}; 
	 $('#insertExamSPhaseform').submit(function() { 
              $(this).ajaxSubmit(options);  			
              return false; 
         }); 

//function after succesful file upload (when server response)
function afterSuccess()
{
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button
	$('#progressbox').delay( 1000 ).fadeOut(); //hide progress bar
        onLoadExamsSections();
}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
   
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		if( !$('#FileInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}
		var fsize = $('#FileInput')[0].files[0].size; //get file size
		var ftype = $('#FileInput')[0].files[0].type; // get file type
		//allow file types 
		switch(ftype)
        {
			case 'application/pdf':
                        break;
                        default:
                        $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		if(fsize>52428800) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//progress bar function
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar
    $('#progressbox').show();
    $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $('#statustxt').html(percentComplete + '%'); //update status text
    if(percentComplete>50)
        {
            $('#statustxt').css('color','#000'); //change status text to white after 50%
        }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
}); 

</script>
</html>


