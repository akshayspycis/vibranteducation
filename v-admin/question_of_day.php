<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Question of the day</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
        <script type="text/javascript">
              cate={}
         $(document).ready(function() {
             onlad();
            });  
            

        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Question of the day</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Question of the day</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Question</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Question</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Question of the day </h4>
        </div>
        <form id ="insQuestionOfDay">    
        <div class="modal-body">
        <div class="row">
        <div class="form-group col-md-12" >
            <label>Question </label>
            <textarea class="form-control" rows="5" id="question_of_day"  name="question_of_day" style="resize:none" placeholder="Content here"></textarea>
         </div>
        
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Question</h4>
</div>
<form id ="editCategory">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
<input type="hidden"  name="question_of_day_id" id="question_of_day_id" value="" class="form-control"/>
 <textarea class="form-control" rows="5" id="editcate"  name="editcate" style="resize:none" placeholder="Content here"></textarea>
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="deleteCategory">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <p>Sure to want to delete Question ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      var text_editor;
      $(document).ready(function(){
          
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                text_editor=CKEDITOR.replace("question_of_day");
                            $('#insQuestionOfDay').off("submit");
                            $('#insQuestionOfDay').submit(function() {
                            
                            if(text_editor.getData() == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Question of day").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#question_of_day').focus();
                                $('#insQuestionOfDay').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        var question={}
                        question["question_of_day"]= encodeURIComponent(text_editor.getData())
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsQuestionOfDay.php",
                        data:question,
                        success: function(data){ 
                        onlad();
                        $('#insQuestionOfDay').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    var question_of_day={}
         function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelQuestionOfDay.php",
                    success: function(data) {
                        question_of_day={}
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            question_of_day[article.question_of_day_id]={};
                            question_of_day[article.question_of_day_id]["question_of_day"]= article.question_of_day;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(decodeURIComponent(article.question_of_day)))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.question_of_day_id+"  id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html($("<button>").addClass("btn btn-danger btn-xs").attr({'type':'button'}).append("Delete").click(function (){
                                    delete_question(article.question_of_day_id);
                                })))
                            );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var question_of_day_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        text_editor=CKEDITOR.replace("editcate");
                        text_editor.setData(decodeURIComponent(question_of_day[question_of_day_id]["question_of_day"]));
                            $('#editCategory').off("submit");
                            $('#editCategory').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(text_editor.getData() == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid demo class ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }

                    else {
                        var question={}
                        question["question_of_day_id"]= question_of_day_id;
                        question["question_of_day"]= encodeURIComponent(text_editor.getData());
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/UpdQuestionOfDay.php",
                        data:question,
                        success: function(data){ 
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        function delete_question(question_of_day_id){
            $("#delete1").modal("show");
            $("#delete1").on('shown.bs.modal', function(){
                    $('#deleteCategory').off("submit");
                    $('#deleteCategory').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelQuestionOfDay.php",
                            data:{'question_of_day_id':question_of_day_id},
                            success: function(data){ 
                            $('#deleteCategory').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

