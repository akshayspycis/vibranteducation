  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="style/images/user.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo "Welcome"." ".$_SESSION['user_name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Main  Navigation</li>
            <li>
                <a href="home.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li>
                <a href="banners.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Banners</span>
              </a>
            </li>
            <li>
                <a href="course_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Course Details</span>
              </a>
            </li>
            <li>
                <a href="queries.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Latest Queries</span>
              </a>
            </li>
            <li>
                <a href="exam_pattern_analysis.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Exam Pattern Analysis</span>
              </a>
            </li>
             
             <li>
                 <a href="newsupdate.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Latest News</span>
              </a>
             
            </li>
             <li>
                 <a href="noticeboard.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Notice Board</span>
              </a>
             
            </li>
            <li>
                <a href="queries.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Course Queries</span>
              </a>
             
            </li> 
            <li>
                <a href="demo_class.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Demo Class</span>
              </a>
            </li> 
            <li>
                <a href="demo_date.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Demo Date</span>
              </a>
            </li> 
            <li>
                <a href="demo_registration.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Demo Registration</span>
              </a>
            </li> 
            <li>
                <a href="question_of_day.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Question of day</span>
              </a>
            </li> 
            <li>
                <a href="thought_of_day.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Thought of the day</span>
              </a>
            </li> 
            <li>
                <a href="course_ragistration.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>Course Ragistration</span>
              </a>
            </li> 
            <li>
                <a href="user_payment_details.php">
                <i class="fa  fa-angle-double-right"></i>
                <span>User Payment</span>
              </a>
            </li> 
             <li class="treeview">
                 <a href="gallerycate.php">
                    <i class="fa fa-angle-double-right"></i>
                    <span>Gallery Categories</span>
                </a>
            </li>
            <li class="treeview">
              <a href="gallery.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Gallery</span>
              </a>
            </li>
           
           
            <li>
                <a href="feedback.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Student Feedback </span>
              </a>
            </li> 
            <li>
                <a href="careers.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Career</span>
              </a>
            </li> 
            <li>
                <a href="downloads.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Downnload Section</span>
              </a>
            </li> 
           
            <li>
                <a href="blog_category.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Blog Category </span>
              </a>
            </li> 
            <li>
                <a href="blog_details.php">
                <i class="fa fa-angle-double-right "></i>
                <span>Blog Details</span>
              </a>
            </li> 
            <li>
                <a href="careers.php">
                <i class="fa fa-angle-double-right "></i>
                <span>Careers</span>
              </a>
            </li> 
            <li>
                <a href="refer_details.php">
                <i class="fa fa-angle-double-right "></i>
                <span>Refer Details</span>
              </a>
            </li> 
            <li>
                <a href="notifications.php">
                <i class="fa fa-angle-double-right "></i>
                <span>Latest Notifications</span>
              </a>
            </li> 
            <li>
                <a href="selections.php">
                <i class="fa fa-angle-double-right "></i>
                <span>Our Selection</span>
              </a>
            </li> 
            <li>
                <a href="category_details.php">
                <i class="fa fa-angle-double-right "></i>
                <span>File Category</span>
              </a>
            </li> 
            <li>
                <a href="file_details.php">
                <i class="fa fa-angle-double-right "></i>
                <span>File Details</span>
              </a>
            </li> 
                        

<!--            <li>
                <a href="cfaq.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Faq's Category</span>
              </a>
           </li> -->
<!--            <li><a href="faq.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Faq's</span>
              </a></li>-->
           
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>