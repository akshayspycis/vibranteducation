<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Thought of the day</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
        <script type="text/javascript">
              cate={}
         $(document).ready(function() {
             onlad();
            });  
            

        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Thought of the day</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Thought of the day</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addcat">Add Thought</button>-->
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Content</th>
                        <th>Author</th>
                        <th>User Name</th>
                        <th>User Pic</th>
                        <th>Post date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Thought of the day </h4>
        </div>
        <form id ="insThoughtOfDay">    
        <div class="modal-body">
        <div class="row">
        <div class="form-group col-md-12" >
            <label>Thought </label>
            <textarea class="form-control" rows="5" id="thought_of_day"  name="thought_of_day" style="resize:none" placeholder="Content here"></textarea>
         </div>
        
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Thought</h4>
</div>
<form id ="editCategory">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
<input type="hidden"  name="thoughtofday_id" id="thoughtofday_id" value="" class="form-control"/>
 <textarea class="form-control" rows="5" id="editcate"  name="editcate" style="resize:none" placeholder="Content here"></textarea>
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="deleteCategory">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <p>Sure to want to delete Thought ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      var text_editor;
      $(document).ready(function(){
          
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                text_editor=CKEDITOR.replace("thought_of_day");
                            $('#insThoughtOfDay').off("submit");
                            $('#insThoughtOfDay').submit(function() {
                            
                            if(text_editor.getData() == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Thought of day").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#thought_of_day').focus();
                                $('#insThoughtOfDay').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        var thought={}
                        thought["thought_of_day"]= encodeURIComponent(text_editor.getData())
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsThoughtOfDay.php",
                        data:thought,
                        success: function(data){ 
                        onlad();
                        $('#insThoughtOfDay').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    var thought_of_day={}
         function onlad(){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelThoughtOfDay.php",
                    success: function(data) {
                        thought_of_day={}
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            thought_of_day[article.thoughtofday_id]={};
                            thought_of_day[article.thoughtofday_id]["thought_of_day"]= article.thought_of_day;
                             var btn_class="btn-danger";
                            if(article.status=="Enable"){
                               btn_class="btn-success";
                            }
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.content))
                                .append($('<td/>').html(article.author))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html($("<img>").addClass("img-circle").css({'width':'83px'}).attr({'src':'../server/controller/'+article.pic_path})))
                                .append($('<td/>').html(article.posteddate))
                                .append($('<td/>').html($("<button>").addClass("btn btn-danger btn-xs").attr({'type':'button'}).append("Delete").click(function (){
                                    delete_thought(article.thoughtofday_id);
                                })))
                                .append($('<td/>').html($("<button>").addClass("btn "+btn_class+" btn-xs").attr({'type':'button'}).append(article.status).click(function (){
                                    updatestatus($(this),article.thoughtofday_id);
                                })))
                            );
                        });
                    }
                });
         }
              function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdThoughtOfDayStatus.php",
                    data:{'thoughtofday_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
</script>

<script type="text/javascript" language="javascript">
        function delete_thought(thoughtofday_id){
            $("#delete1").modal("show");
            $("#delete1").on('shown.bs.modal', function(){
                    $('#deleteCategory').off("submit");
                    $('#deleteCategory').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelThoughtOfDay.php",
                            data:{'thoughtofday_id':thoughtofday_id},
                            success: function(data){ 
                            $('#deleteCategory').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

