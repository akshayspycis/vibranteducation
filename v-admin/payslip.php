<?php // include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>User Payment Details</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
   
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
  

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">User Payment Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                 
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-3">
                     
                        </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    
                  <div class="table-responsive"> 
                      <table border="0" cellspacing="0"width="100%" style="box-shadow: 0 2px 1px rgba(0,0,0, 0.50);"> 
                          <tr>
                              <td colspan="2" ><p style="font-size:18px;margin-top:20px;font-family:arial;line-height:25px;text-align:center">Please note details of Funds Received from you.</p></td>
                          </tr>
                          <tr>
                              <td width="50%">
                                  <h4 style="font-family:arial;font-weight:bold">VIBRANT EDUCATION SERVICES</h4>
                                  <p style="font-family:arial">164,I & II Floor, Samanvay Nagar, <br>Awadhpuri, Bhopal<br> contact <b>0755-4047934</b>, <b>98-262-262-99</b><br>
                                      website <a href="http://www.vibrantcareer.com/"><b>www.vibrantcareer.com</b></a> </p>
                              </td>
                              <td style="float:right"><img class="img-responsive" src="../assets/images/vibr.png" width="300" /></td>
                          </tr>
                          <tr>
                              <td colspan="2" ><h1 style="font-family:arial;text-align:center;font-weight:bold;margin-top:40px;">FEE RECIEPT</h1></td>
                          </tr>
                          <tr>
                              <td width="50%"><p style="font-family:arial;font-weight:bold;margin-top:10px;">Reciept No. VES-23-03-2017-001</p><p style="font-family:arial;font-weight:bold;margin-top:10px;">Registration No. VES001</p></td>
                              <td width="50%"><p style="font-family:arial;font-weight:bold;margin-top:10px;float:right">Date 23-03-2017</p></td>
                          </tr>
                           <tr>
                               <td colspan="2"><p style="font-family:arial;font-weight:bold;line-height:24px;margin-top:10px;">Recieved with thanks a sum of Rupess <span style="text-decoration:underline">4000</span>, In words <span style="text-decoration:underline">Four Thousand</span>  only. from Sonam Jain c/o  Ashok Jain  via cash for Bank PO course as 1st Installment.<br>Course Duration 6 month from...... to .......</p></td>
                              
                          </tr>
                           <tr>
                               <td colspan="2"><p style="font-family:arial;font-weight:bold;line-height:24px;margin-top:10px;text-decoration:underline">Terms & Condition</p>
                                   <ul>
                                       <li>Cheques are subjected for realization.</li>
                                       <li>Fee once paid is neither refundable nor adjustable.</li>
                                       <li>As per admission Form</li>
                                   </ul></td>
                              
                          </tr>
                      </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

      
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

      
   
  </body>
  
    




</html>



