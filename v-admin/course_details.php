<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Course Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <script src="https://cdn.ckeditor.com/4.5.10/standard-all/ckeditor.js"></script>
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          course={}
          $(document).ready(function(){
              onlad();
              $(".modal").on('hide.bs.modal', function() {
                location.reload(true);
              });
            });  
          
      </script>
      
      
    <style>
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-blue  sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Course Details & Updates</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Course Details & Updates</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addCourseDetails">Add Course Details</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Title</th>
                        <!--<th width="20%">Short Description</th>-->
                        <th width="30%">Long Description</th>
                        <!--<th>Date</th>-->
                        <th>Image</th>
                        <!--<th  ><i class="fa fa-comment"></i</th>-->
                        <th  style="text-align:center">Action</th>
                        <th style="text-align:center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
      <div class="modal fade" id="insCourseDetails" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Course Details </h4>
        </div>
        <form id ="insertform" enctype="multipart/form-data">    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12">
                
                        <div class="row">
                        
                        <div class="form-group col-md-6">
                            <label for="student name">Course Heading</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="Heading here">
                        </div>
                        <div class="form-group col-lg-6 col-sm-6">
                            <label for="question">Status</label>
                            <select class="form-control select2" id="status" name="status">
                                <option value="Enable">Enable</option>
                                <option value="Disable">Disable</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="image">Add Image</label>
                        <div class="box-body">
                          <div class="uploadArea" id="dragAndDropFiles" style="min-height: 60px;">
                              <h1 style="font-size: 26px;">Drop Images Here</h1>
                                  <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                          </div>
                        </div>
                        <p class="help-block"></p>
                       </div>
                 <div class="form-group col-md-12" >
                    <label>Course Long Content</label>
                    <textarea class="form-control" rows="5" id="long_description"  name="long_description" style="resize:none" placeholder="Content here"></textarea>
                 </div><!-- /.form-group -->
              </div><!-- /.row -->
                
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
   
      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Course Details</h4>
        </div>
        <form id ="deleteCourseDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="course_details_id" id="course_details_id" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
      
  </body>
  <script type="text/javascript" language="javascript">
      var obj_class=null;
      var text_editor;
      $(document).ready(function(){
        $("#addCourseDetails").click(function(){
            
            $("#insCourseDetails").modal('show');}); 
                $("#insCourseDetails").on('shown.bs.modal', function(){
                 text_editor=CKEDITOR.replace("long_description");
                var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "insertform", // Form ID
                        dragArea: "dragAndDropFiles", // Upload Area ID
                        multiUpload:"multiUpload",
                        url:"../server/controller/InsCourseDetails.php",
                        onlad:onlad,
                        strlenght:25,
                        module:1
                        }
                        if(obj_class==null){
                           obj_class=new $.UploadImg(obj);           
                        }
   
    });
   }); 
         
  /* Code for Product Edit   start */  
  var upd_img=false;
  // this when we want to update image above var is set value;
$(document).on("click", ".close", function () {
    var course_details_id=$(this).attr('id');
    $(this).next('span').empty();
    var form = $("<form>").attr('id','update_img')
    var a=$("<div>").addClass("uploadArea").attr('id','dragAndDropFiles1').css('min-height','97px')
        .append($("<h1>").css('font-size','16px').append("Drop Images Here"))
        .append($("<input/>").css({'width':'0px','height':'0px','overflow':'hidden'}).attr({'type':'file','data-maxwidth':'620','data-maxheight':'620','name':'file[]','id':'multiUpload1'}))
         var upd_button=$("<button>").attr({'type':'button','class':'btn btn-default btn-sm'}).append($("<i>").addClass("fa fa-upload "))
                 .css({'margin-bottom':'5px'})
         form.append(upd_button)
         form.append(a)
         $(this).next('span').append(form);
         var obj = {
                support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "update_img", // Form ID
                        dragArea: "dragAndDropFiles1", // Upload Area ID
                        multiUpload:"multiUpload1",// input field id
                        url:"../server/controller/UpdCourseDetails.php",//location call controller
                        onlad:onlad,
                        course_details_id:course_details_id,
                        upd_button:upd_button,
                        strlenght:25,
                        module:1
                        }
                      new $.UploadImg(obj);           
});

  $(document).on("click", "#edit", function () {
                 var course_details_id = $(this).data('course_details_id');
                 
                 $("#editCourse Details").on('shown.bs.modal', function(){
                                    $("#editCourse Details").find("#course_details_id").val(course_details_id);  
                                    $("#editCourse Details").find("#nheading").val(course[course_details_id]["nheading"]);  
                                    $("#editCourse Details").find("#ndate").val(course[course_details_id]["ndate"]);  
                                    $("#editCourse Details").find("#nimage").val(course[course_details_id]["nimage"]);  
                                    $("#editCourse Details").find("#ncontent").val(course[course_details_id]["ncontent"]);  
                                     var img= "../server/controller/"+course[course_details_id]["nimage"];
                                     $("#editCourse Details").find("#close_image").attr({'id':course_details_id});
                                     $("#editCourse Details").find("#imgg").empty();
                                    $("#editCourse Details").find("#imgg").append("<img src="+img+" height=\"100\" width=\"100\"/>");  
                                    $('#updateform').submit(function() {
                        $.ajax({
                        type:"post",
                        url:"../server/controller/updateCourseDetails.php",
                        data:$('#updateform').serialize(),
                        success: function(data){
                            onlad();
                          $('#updateform').each(function(){
                            this.reset();
                            $('#editCourse Details').modal('hide');
                            return false;
                                    });
                              } 
                            });
                   
                   return false;
    });
   
    });
});
  /* Code for Product Edit   start */  
</script>

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var course_details_id = $(this).data('course_details_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#course_details_id").val(course_details_id);        
                        $('#deleteCourseDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelCourseDetails.php",
                            data:{'course_details_id':course_details_id},
                            success: function(data){ 
                            $("#search_category_id").find('#searchcategory_id').val(course[course_details_id]["category_id"]).trigger("change");
                        $('#deleteCourseDetails').each(function(){
                            this.reset();
                            $('#delete1').modal('hide');
                            return false;
                        });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................
jQuery.extend({
	UploadImg: function(obja){
                this.up_obj = obja;
                var items = "";
                this.all = {}
                var that = this;
		var listeners = new Array();
                var fileinput = document.getElementById(this.up_obj.multiUpload);
                var max_width = fileinput.getAttribute('data-maxwidth');
                var max_height = fileinput.getAttribute('data-maxheight');
                var form = document.getElementById(this.up_obj.form);
		/**
		 * get contents of cache into an array
		 */
                this._init = function(){
                    if (window.File && window.FileReader && window.FileList && window.Blob) {	
                       
                             var inputId = $("#"+this.up_obj.form).find("input[type='file']").eq(0).attr("id");
                             document.getElementById(inputId).addEventListener("change", that._read, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
                             document.getElementById(that.up_obj.dragArea).addEventListener("drop", that._dropFiles, false);
                             document.getElementById(that.up_obj.form).addEventListener("submit", that._submit, false);
                    } else console.log("Browser supports failed");
                    
                }
                
                $("#"+this.up_obj.dragArea).click(function() {
                    fileinput.click();
                });
                
                this._submit = function(e){
                    e.stopPropagation(); 
                    e.preventDefault();
                    that._startUpload();
            	}
                
                this._preview = function(data){
                    this.items = data;
                    if(this.items.length > 0 && this.items.length <2){
                        var html;		
                        var uId = "";
                        for(var i = 0; i<this.items.length; i++){
                            uId = this.items[i].name._unique();
                            obj={};
                            obj["file"]=this.items[i];
                            that.all[uId]=obj;
//                            var kk=$('<a></a>').append($("<i></i>").addClass('fa  fa-close ')).css({'position':'absolute','right':'5px','top':'2px'}).click(function (e){e.stopPropagation();
//                                that._deleteFiles($(this).parent().parent().attr('rel'));
//                                $(this).parent().parent().remove();
//                            });
                            var sampleIcon = 'fa fa-image';
                            var errorClass = "";
                            if(typeof this.items[i] != undefined){
                                if(that._validate(this.items[i].type) <= 0) {
                                    sampleIcon = 'fa fa-exclamation';
                                    errorClass =" invalid";
                                } 
                                
                                if(that.up_obj.upd_button==null){
                                   
                                }
                                
                            }
                            $("#"+this.up_obj.dragArea).empty();
                            
                                that.readfiles(this.items[i],uId)
                            }
                            }else{
                                alert("Image file select limit maximum is 1 files.")
                            }
                        }
                     
                        
                this.setStream = function(stream,course_details_id){
                    that.all[course_details_id]["byte_stream"]=stream;
                    $(".dfiles[rel='"+course_details_id+"'] >h5>img").remove();
                }
        
                this._read = function(evt){
                        if(evt.target.files){
                            that._preview(evt.target.files);
                            //that._preview(evt.target.files);
                        } else 
                            console.log("Failed file reading");
                }
	
                this._validate = function(format){
                        var arr = this.up_obj.support.split(",");
                        return arr.indexOf(format);
                }
	
                this._dropFiles = function(e){
                        e.stopPropagation(); 
                        e.preventDefault();
                        that._preview(e.dataTransfer.files);
                }
                
                this._deleteFiles = function(key){
                    delete that.all[key];
                }
                
                this._uploader = function(file,key){
                   if(typeof file != undefined && that._validate(file["file"].type) > 0){
			var data = new FormData();
			var ids = file["file"].name._unique();
			data.append('images',file["byte_stream"]);
			data.append('index',ids);
			$(".dfiles[rel='"+ids+"'] >h5").append('<img src="style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>')
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        
                      var course = {};  
                      if(that.up_obj.upd_button==null){
                              $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="title"){
                                        course["title"]= $(this).val();
                                    }
                                    if($(this).attr("name")=="status"){
                                        course["status"]= $(this).val();
                                    }
                               });
                               course["user_id"]= '1';
                               course["long_description"]= encodeURIComponent(text_editor.getData());
                      }else{
                          course['course_details_id']=that.up_obj.course_details_id;
                      }
                        
                      
                      course["nimage"] =file["byte_stream"] ;
                            $.ajax({
                            type:"post",
                            url:that.up_obj.url,
                            data:course,
                            success: function(data){
                                alert(data)
                             if(that.up_obj.upd_button!=null){
                                  that.up_obj.upd_button.empty();
                                         that.up_obj.upd_button.append($("<i>").addClass("fa fa-check-square-o"))
                                         that.up_obj.onlad();
                                     }else{
                                       
                                          $("#dragAndDropFiles").find("canvas").remove();
                                          $("#dragAndDropFiles").append('<h1 style="font-size: 26px;">Drop Images Here</h1><input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">');
                                            that._deleteFiles(key);
                                            $(".dfiles[rel='"+ids+"']").remove();
                                            //
                                        $('#insertform').each(function(){
                                            this.reset();
                                            $("#search_course_id").val(course["category_id"]);
                                            $("#search_category_id").find('#searchcategory_id').val(course["category_id"]).trigger("change");;
                                            text_editor.setData("");
                                            return false;
                                        });
                                     }   
                                    } 
                                });
                    }else {
                        console.log("Invalid file format - "+file.name);
                    }
                }
   
                this._startUpload = function(){
                    if(that.up_obj.upd_button==null){
                        
                            var reg = /^\\d+$/;
                            var b=true;
                            $('#insertform').find(":input").each(function() {
                                    if($(this).attr("name")=="nheading" && $(this).val()==""){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please fill  Heading").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        (this).focus();
                                        b=false;
                                        return false;
                                    }
                            });

                            if(b==false){
                            return b;
                            }
                            if(Object.keys(that.all).length==0){
                                        $(".validate").addClass("text-danger").fadeIn(100).text("Please upload  Image").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                        return false;
                            }else{
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }else{
                            if(Object.keys(that.all).length==1){
                                $.each(that.all,function(key,value){
                                    that._uploader(value,key);
                                });
                            }
                    }
                    
                    
                } 
        
                String.prototype._unique = function(){
                        return this.replace(/[a-zA-Z]/g, function(c){
                            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
                        });
                }
                
                this.processfile =function(file,course_details_id) {
            if( !( /image/i ).test( file.type )){
                alert( "File "+ file.name +" is not an image." );
                return false;
            }
            // read the files
              var reader = new FileReader();
              reader.readAsArrayBuffer(file);
              reader.onload = function (event) {
              var blob = new Blob([event.target.result]); // create blob...
              window.URL = window.URL || window.webkitURL;
              var blobURL = window.URL.createObjectURL(blob); // and get it's URL
              var image = new Image();
              image.src = blobURL;
              image.onload = function() {
              that.setStream(that.resizeMe(image,course_details_id),course_details_id)
              }
            };
        }

                this.readfiles=function(files,course_details_id) {
                    that.processfile(files,course_details_id); // process each file at once
                }

                this.resizeMe=function (img,course_details_id) {
           var canvas = document.createElement('canvas');
           var width = img.width;
           var height = img.height;
          // calculate the width and height, constraining the proportions
            if (width > height) {
                if (width > max_width) {
                  //height *= max_width / width;
                  height = Math.round(height *= max_width / width);
                  width = max_width;
                }
            } else {
                if (height > max_height) {
                  //width *= max_height / height;
                  width = Math.round(width *= max_height / height);
                  height = max_height;
                }
            }
  
             // resize the canvas and draw the image data into it
              canvas.width = width;
              canvas.height = height;
              var ctx = canvas.getContext("2d");
              ctx.drawImage(img, 0, 0, width, height);
              var canvas1 = document.createElement('canvas');
              canvas1.width = 50;
              canvas1.height = 50;
              var ctx = canvas1.getContext("2d");
              ctx.drawImage(img, 0, 0, 50, 50);
              that.all[course_details_id]["image"]=canvas1;
             
                  var canvas2 = document.createElement('canvas');
              canvas2.width =100;
              canvas2.height = 100;
              var ctx = canvas2.getContext("2d");
              ctx.drawImage(img, 0, 0, 100, 100);
                                $("#"+this.up_obj.dragArea).append(canvas2);
                                 if(that.up_obj.upd_button!=null){
                                that.up_obj.upd_button.click(function (){
                                    that.up_obj.upd_button.empty();
                                    that.up_obj.upd_button.append('<img src="../style/dist/img/loading_1.gif" style="height:20px;width:20px;"/>');
                                   that._startUpload()
                                });
                                }
             // preview.appendChild(canvas1); // do the actual resized preview
              return canvas.toDataURL("image/jpeg",0.7); // get the data from canvas as 70% JPG (can be also PNG, etc.)

        }
        
                this._init();
        }
});
function onlad(){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelCourseDetails.php",
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            course[article.course_details_id]=article;
                           var img= "../server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               btn_class="btn-success";
                           }
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.title))
//                                .append($('<td/>').html(article.short_description))
                                .append($('<td/>').html(decodeURIComponent(article.long_description)))
//                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-primary btn-xs\" \")><i class=\"fa fa-edit\"></i    ></button>")).click(function (){
//                                    showComment(article.course_details_id)
//                                } )
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-course_details_id="+article.course_details_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(article.status).click(function(){
                                     updatestatus($(this),article.course_details_id);
                                     
                                })))
                             );
                        });
                    }
                });
                }
                function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Enable"){
                                value="Disable";
                                obj.empty();
                                obj.append("Disable");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="Enable";
                                obj.empty();
                                obj.append("Enable");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/UpdCourseStatus.php",
                    data:{'course_details_id':id,'status':value},
                    success: function(data) {
                        
                    }
                  });
              }
                
                
            
</script>


</html>

