<?php include 'includes/session.php'; ?>    
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Contact Us  |  Vibrant Education</title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
            <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header" onload="loadHTML('contact')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/bg/22.jpg'); background-position: 50% 30%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container">
					
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					<div class="row">
                                            <div class="col-md-12 text-center  pv-20">
                                                <h1 class="page-title text-center">Contact Us</h1>
                                                <div class="separator"></div>
                                                <p class="lead text-center" style="font-weight:bold">It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</p>
                                                <ul class="list-inline mb-20 text-center" style="font-weight:bold">
                                                    <li><i class="text-default fa fa-map-marker pr-5"></i>164, I & II floor, Samanvay Nagar, 
                                                        Awadhpuri, Bhopal (M.P.)</li>
                                                    <li><a href="tel:+00 1234567890" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>98-262-262-99</a></li>
                                                    <li><a href="" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-5"></i>contactus@vibrantcareer.com</a></li>
                                                </ul>
                                                <div class="separator"></div>
                                                <ul class="social-links circle animated-effect-1 margin-clear text-center space-bottom">
<!--                                                    <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                    <li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                    <li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                                                    <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                                    <li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>-->
                                                                                    
                                                </ul>
                                            </div>
					</div>
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 ;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="container">
                               <div class="main col-md-12 space-bottom">
							<h2 class="title">Feel free to contact us</h2>
							<div class="row">
								<div class="col-md-6">
									
                                                                    <form role="form" class="" method="post" id="contact-query">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    
                                                                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    
                                                                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                                                                </div>
            
                                                                                <div class="form-group">
                                                                                    
                                                                                    <textarea class="form-control" rows="3" id="message" name="message" placeholder="Message"></textarea>
                                                                                </div>
                                                                                <input type="submit" name="submit" id="submit" class="btn btn-default" value="Send"/>
                                                                            </form>
									
								</div>
								<div class="col-md-6">
									<iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d3666.368944833861!2d77.48345376411096!3d23.229657684848572!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d23.229508!2d77.4854431!4m5!1s0x397c419168f7f083%3A0xde029f61c2c4e17e!2svibrant+education+services+bhopal!3m2!1d23.229208!2d77.485789!5e0!3m2!1sen!2sin!4v1489261074399" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
							</div>
						</div>   
                                
                            </div>
                        </section>
			   
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		 <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/ContactQuery.js"></script>
	</body>

</html>

