<?php include 'includes/session.php'; ?>    
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Vibrant Education Services | Terms & Conditions </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?php include './includes/csslinks.php' ?>
           
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include './includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/bg/computer-training-institute.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
			     <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 url('images/bg/overlay.png') repeat;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 text-center">
							<h2 class="text-center heading-font" style="text-transform:none;"><strong>Term and Conditions</strong>  </h2>
							<div class="separator"></div>						
						</div>
					</div>
					 <div class="col-md-12 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                        <h5 class="heading-font">Terms and Conditions for use of www.vibrantcareer.com :</h5>
                                        <p id="cpara">
                                            The <b>www.vibrantcareer.com </b>Web Site is comprised of various Web pages operated by <b>www.vibrantcareer.com </b>.
                                        </p>
                                        <p id="cpara">
                                           The <b>www.vibrantcareer.com </b> Web Site is offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the <b>www.vibrantcareer.com </b> Web Site constitutes your agreement to all such terms, conditions, and notices.
                                        </p>
                                        
                                        <h5 class="heading-font">Modification of these terms of Use : </h5>
                                        <p id="cpara">
                                           www.vibrantcareer.com reserves the right to change the terms, conditions, and notices under which the www.vibrantcareer.com Web Site is offered, including but not limited to the charges associated with the use of the www.vibrantcareer.com Web Site.
                                        </p>
                                         <h5 class="heading-font">Links to third party sites : </h5>
                                        <p id="cpara">
                                            The www.vibrantcareer.com Web Site may contain links to other Web Sites ("Linked Sites"). The Linked Sites are not under the control of www.vibrantcareer.com and www.vibrantcareer.com is not at all responsible for the contents of any such Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. www.vibrantcareer.com is not responsible for webcasting or any other form of transmission received from any Linked Site. www.vibrantcareer.com is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by www.vibrantcareer.com of the site or any association with its operators.
                                        </p>
                                        <h5 class="heading-font">No Unlawful or prohibited use :</h5>
                                        <p id="cpara">
                                           As a condition of your use of the www.vibrantcareer.com Web Site, you warrant to www.vibrantcareer.com that you will not use the www.vibrantcareer.com Web Site for any purpose that is unlawful or prohibited by these terms, conditions, and notices. You may not use the www.vibrantcareer.com Web Site in any manner which could damage, disable, overburden, or impair the www.vibrantcareer.com Web Site or interfere with any other party's use and enjoyment of the www.vibrantcareer.com Web Site. You may not obtain or attempt to obtain any materials or information through any means not intentionally made available or provided for and through the www.vibrantcareer.com Web Sites.
                                        </p>
                                        <h5 class="heading-font">Use of Communication Services :</h5>
                                        <p id="cpara">
                                          The www.vibrantcareer.com Web Site may contain bulletin board services, chat areas, news groups, forums, communities, personal web pages, calendars, and/or other message or communication facilities designed to enable you to communicate with the public at large or with a group (collectively, "Communication Services"), you agree to use the Communication Services only to post, send and receive messages and material that are proper and related to the particular Communication Service. By way of example, and not as a limitation, you agree that when using a Communication Service, you will not:
                                        </p>
                                        <ul class="list-icons" id="clist">
                                            <li>Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as rights of privacy and publicity) of others.</li>
                                            <li>Publish, post, upload, distribute or disseminate any inappropriate, profane, defamatory, infringing, obscene, indecent or unlawful topic, name, material or information.</li>
                                            <li>Upload files that contain software or other material protected by intellectual property laws (or by rights of privacy of publicity) unless you own or control the rights thereto or have received all necessary consents.</li>
                                            <li>Upload files that contain viruses, corrupted files, or any other similar software or programs that may damage the operation of another's computer.</li>
                                            <li>Advertise or offer to sell or buy any goods or services for any business purpose, unless such Communication Service specifically allows such messages.</li>
                                            <li>Conduct or forward surveys, contests, pyramid schemes or chain letters and MLM marketing.</li>
                                            <li>Download any file posted by another user of a Communication Service that you know, or reasonably should know, cannot be legally distributed in such manner.</li>
                                            <li>Falsify or delete any author attributions, legal or other proper notices or proprietary designations or labels of the origin or source of software or other material contained in a file that is uploaded.</li>
                                            <li>Restrict or inhibit any other user from using and enjoying the Communication Services.</li>
                                            <li>Violate any code of conduct or other guidelines which may be applicable for any particular Communication Service.</li>
                                            <li>Harvest or otherwise collect information about others, including e-mail addresses, without their consent.</li>
                                            <li>Violate any applicable laws or regulations.</li>
                                        </ul>
                                        
                                        <p id="cpara">
                                            www.vibrantcareer.com has no obligation to monitor the Communication Services. However, www.vibrantcareer.com reserves the right to review materials posted to a Communication Service and to remove any materials in its sole discretion. www.vibrantcareer.com reserves the right to terminate your access to any or all of the Communication Services at any time without notice for any reason whatsoever.
                                        </p>
                                        <p id="cpara">
                                           www.vibrantcareer.com reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in www.vibrantcareer.com’s sole discretion.
                                        </p>
                                         
                                        <p id="cpara">
                                           Always use caution when giving out any personally identifying information about yourself or your children in any Communication Service. www.vibrantcareer.com does not control or endorse the content, messages or information found in any Communication Service and, therefore, www.vibrantcareer.com specifically disclaims any liability with regard to the Communication Services and any actions resulting from your participation in any Communication Service. Managers, center heads and hosts are not authorized to comment on any step taken by www.vibrantcareer.com. www.vibrantcareer.com spokespersons, and their views do not necessarily reflect those of www.vibrantcareer.com.
                                        </p>
                                        <p id="cpara">
                                          Materials uploaded to a Communication Service may be subject to posted limitations on usage, reproduction and/or dissemination. You are responsible for adhering to such limitations if you download the materials.
                                        </p>
                                          <h5 class="heading-font">Materials Provided to www.vibrantcareer.com Or posted at any www.vibrantcareer.com website</h5>
                                          <p id='cpara'>
                                              www.vibrantcareer.comvdoes not claim ownership of the materials you provide to www.vibrantcareer.com(including feedback and suggestions) or post, upload, input or submit to any www.vibrantcareer.com Web Site or its associated services (collectively "Submissions"). However, by posting, uploading, inputting, providing or submitting your Submission you are granting www.vibrantcareer.com, and necessary sublicensees permission to use your Submission in connection with the operation of their Internet businesses including, without limitation, the rights to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat your Submission; and to publish your name in connection with your Submission.
                                          </p>
                                          <p id='cpara'>No compensation will be paid with respect to the use of your Submission, as provided herein. www.vibrantcareer.com is under no obligation to post or use any Submission you may provide and may remove any Submission at any time in www.vibrantcareer.com's sole discretion.</p>
                                          <p id='cpara'>By posting, uploading, inputting, providing or submitting your Submission you warrant and represent that you own or otherwise control all of the rights to your Submission as described in this section including, without limitation, all the rights necessary for you to provide, post, upload, input or submit the Submissions.</p>
                                    <h5 class="heading-font">LIABILITY DISCLAIMER</h5>
                                    <p id='cpara'>THE INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES INCLUDED IN OR AVAILABLE THROUGH THE www.vibrantcareer.com WEB SITE MAY INCLUDE INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN. www.vibrantcareer.com AND/OR ITS SUPPLIERS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE www.vibrantcareer.com WEB SITE AT ANY TIME. ADVICE RECEIVED VIA THE www.vibrantcareer.com WEB SITE SHOULD NOT BE RELIED UPON FOR PERSONAL, MEDICAL, LEGAL OR FINANCIAL DECISIONS AND YOU SHOULD CONSULT AN APPROPRIATE PROFESSIONAL FOR SPECIFIC ADVICE TAILORED TO YOUR SITUATION.</p>
                                    <p id='cpara'>www.vibrantcareer.com AND/OR ITS SUPPLIERS MAKE NO REPRESENTATIONS ABOUT THE SUITABILITY, RELIABILITY, AVAILABILITY, TIMELINESS, AND ACCURACY OF THE INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS CONTAINED ON THE www.vibrantcareer.com WEB SITE FOR ANY PURPOSE. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ALL SUCH INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS ARE PROVIDED "AS IS" WITHOUT WARRANTY OR CONDITION OF ANY KIND. www.vibrantcareer.comAND/OR ITS SUPPLIERS HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THIS INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS, INCLUDING ALL IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT.</p>
                                    <p id='cpara'>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL www.vibrantcareer.com AND/OR ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL, CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF USE, DATA OR PROFITS, ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OR PERFORMANCE OF THE www.vibrantcareer.com WEB SITE, WITH THE DELAY OR INABILITY TO USE THE www.vibrantcareer.com WEB SITE OR RELATED SERVICES, THE PROVISION OF OR FAILURE TO PROVIDE SERVICES, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS, SERVICES AND RELATED GRAPHICS OBTAINED THROUGH THE www.vibrantcareer.comWEB SITE, OR OTHERWISE ARISING OUT OF THE USE OF THE www.vibrantcareer.com WEB SITE, WHETHER BASED ON CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR OTHERWISE, EVEN IF www.vibrantcareer.com OR ANY OF ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IF YOU ARE DISSATISFIED WITH ANY PORTION OF THE www.vibrantcareer.comWEB SITE, OR WITH ANY OF THESE TERMS OF USE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE USING THE www.vibrantcareer.comWEB SITE.</p>
                                          <h5 class="heading-font">SERVICE CONTACT : contactus@vibrantcareer.com</h5>
                                          <h5 class="heading-font">TERMINATION/ACCESS RESTRICTION</h5>
                                          <p id='cpara'>
                                              www.vibrantcareer.com reserves the right, in its sole discretion, to terminate your access to the www.vibrantcareer.com Web Site and the related services or any portion thereof at any time, without notice. GENERAL To the maximum extent permitted by law, this agreement is governed by the laws of the Government of India, its States and its Union territories, and you hereby consent to the exclusive jurisdiction and venue of courts in Bhopal, India in the all disputes arising out of or relating to the use of the www.vibrantcareer.com Web Site. Use of the www.vibrantcareer.com Web Site is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph. You agree that no joint venture, partnership, employment, or agency relationship exists between you and www.vibrantcareer.com as a result of this agreement or use of the www.vibrantcareer.com Web Site. vibrantcareer.com's performance of this agreement is subject to existing laws and legal process, and nothing contained in this agreement is in derogation of vibrantcareer.com's right to comply with governmental, court and law enforcement requests or requirements relating to your use of the www.vibrantcareer.com Web Site or information provided to or gathered by www.vibrantcareer.com with respect to such use. If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision and the remainder of the agreement shall continue in effect. Unless otherwise specified herein, this agreement constitutes the entire agreement between the user and www.vibrantcareer.comwith respect to the www.vibrantcareer.com Web Site and it supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written, between the user and www.vibrantcareer.com with respect to the www.vibrantcareer.com Web Site. A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent an d subject to the same conditions as other business documents and records originally generated and maintained in printed form. It is the express wish to the parties that this agreement and all related documents be drawn up in English.
                                          </p> 
                                         <h5 class="heading-font">COPYRIGHT AND TRADEMARK NOTICES:</h5>
                                         <p id='cpara'>All contents of the www.vibrantcareer.com Web Site are: Copyright 2016 by Vibrant Education Services. All rights reserved.</p>
                                        
                                         <h5 class="heading-font">TRADEMARKS :</h5>
                                         <p id='cpara'>The names of actual companies and products mentioned herein may be the trademarks of their respective owners.</p>
                                          <p id='cpara'>The example companies, organizations, products, people and events depicted herein are fictitious. No association with any real company, organization, product, person, or event is intended or should be inferred.</p>
                                          <p id="cpara">Any rights not expressly granted herein are reserved.</p>
                                         
                                         
                                         
                                         </div>
					
				</div>
				
				
			</section>
                       
                        
			<!-- section end -->
                    
			<div class="clearfix"></div>
			
			<!-- ================ -->
			
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                
                
	</body>


</html>
