<?php include 'includes/session.php'; ?>    
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
            <meta charset="utf-8">
            <title>Blog Details  |  Vibrant Education</title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
              <?php include 'includes/csslinks.php';?>
        
                <style>
                    body p{
                        text-align:justify;
                    }
                </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<div id="page-start"></div>
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-9" id="blog_details">
                                            
                                         </div>
                                     <div class="col-md-3" style="position:relative;top:80px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Categories</h3> 
                                         <div class="body">
                                            <table class="table table-striped ">
                                                <tbody id="category_tbody">
                                                 
                                                 </tbody>
                                             </table>
                                          
                                             
                                         </div>
                                     </div>
                                      
                                      
                                 </div> 
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include 'includes/jslinks.php';?>
                
		<!-- Color Switcher End -->
                <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	</body>

        <script>
             $(document).ready(function(){
                var cta_id="";
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogCategoryDetails.php",
                      success: function(data) {
                        $("#category_tbody").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                        if(cta_id==""){
                            cta_id=article.blog_category_details_id;
                        }
                            $("#category_tbody").append($('<tr/>')
                                    .append($("<th>").append($("<a>").append(article.category+" ("+article.counter+")").css({'cursor':'pointer'})).click(function (){
                                         window.location="index.php?id="+article.blog_category_details_id
                                    }))
                            );   
                        })
                        
                    }
//                      success: function(data) {
//                        $("#category_tbody").empty();
//                        var duce = jQuery.parseJSON(data);
//                        $.each(duce, function (index, article) {
//                        if(cta_id==""){
//                            cta_id=article.blog_category_details_id;
//                        }
//                            $("#category_tbody").append($('<tr/>')
//                                    .append($("<th>").append($("<a>").append(article.category).css({'cursor':'pointer'})).click(function (){
//                                        window.location="index.php?id="+article.blog_category_details_id
//                                    }))
//                            );   
//                        })
//                    }
                })
               
                   if(urlParam('id')==null){
                    window.location="blog.php"
                   }else{
                        onlad(urlParam('id'));
                   }
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
  function onlad(blog_details_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelClientBlogDetails.php",
                    data:{'blog_details_id':blog_details_id},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
                       var duce = jQuery.parseJSON(data);
                      $("#blog_details").empty();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                            blog[article.blog_details_id]["fb"]=article.fb;
                            blog[article.blog_details_id]["tw"]=article.tw;
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                $("#blog_details").append($("<div>").addClass("timeline-item")
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                        .append($("<header>")
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        .append($("<div>").addClass("post-info")
                                                            .append($("<span>").addClass("post-date")
                                                                 .append($("<i>").addClass("icon-calendar"))
                                                                 .append($("<span>").addClass("month").append(article.date))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-user-1"))
                                                                 .append($("<a>").append("Admin"))
                                                             )
                                                            .append($("<span>").addClass("comments")
                                                                 .append($("<i>").addClass("icon-chat"))
                                                                 .append($("<a>").append("comment"))
                                                             )
                                                        ))
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}).css({'width':'100%','height':'250px'}))
                                                        )
                                                    .append($("<p>&nbsp;</p>"))
                                                    .append($("<div>").addClass("blogpost-content").append(decodeURIComponent(article.long_description)))
                                                    .append($("<footer>").addClass("clearfix")
                                                        .append($("<div>").addClass("link pull-left")
                                                            .append($("<i>").addClass("icon-tags"))
                                                            .append($("<a>").append("&nbsp; tag 1").css({'cursor':'pointer'}))
                                                         )
                                                        .append($("<div>").addClass("link pull-right")
                                                            .append($("<ul>").addClass("social-links circle small colored clearfix margin-clear text-right animated-effect-1")
                                                                .append($("<li>").addClass("twitter")
                                                                    .append($("<a>").attr({'target':'_blank','href':article.tw}).css({'cursor':'pointer'})
                                                                       .append($("<i>").addClass("fa fa-twitter"))
                                                                    )
                                                                )
                                                                .append($("<li>").addClass("facebook")
                                                                    .append($("<a>").attr({'target':'_blank','href':article.fb}).css({'cursor':'pointer'})
                                                                       .append($("<i>").addClass("fa fa-facebook"))
                                                                    )
                                                                )
                                                           )
                                                            
                                                        ))
                                                ));
                                        var id=""
                                        var commen_di=$("<div>");
                                        var asb=$("<b>")
                                        $.ajax({
                                            type:"post",
                                            url:"server/controller/SelBlogRplyDetails.php",
                                            data:{'blog_details_id':blog_details_id},
                                            success: function(data){
                                                $("#comment_model").empty();
                                                var duce = jQuery.parseJSON(data);
                                                asb.empty()
                                                asb.append("There are "+Object.keys(duce).length+" comments")
                                                $.each(duce, function (index, article) {
                                                    var btn_class="btn-danger";
                                                   if(article.status=="Enable"){
                                                           commen_di.append($("<div>").addClass("comment-content")
                                                                                .append($("<div>").addClass("comment-avatar").css({'width': '23px'})
                                                                                    .append($("<img>").addClass("img-circle").attr({'src':'../images/avatar.jpg'}))
                                                                                )
                                                                                .append($("<div>").addClass("comment-body clearfix")
                                                                                    .append($("<p>").append(article.user_name))
                                                                                    .append($("<p>").append(article.comment))
                                                                                    .append($("<p>").addClass("small margin-clear").css({'color':'#827979'}).append($("<i>").addClass("fa fa-pencil")).append("&nbsp;"+article.date))
                                                                                ))
                                                   }
                                                });
                                               },
                                               error:function (e){
                                               alert(e.responseText)
                                               }
                                            });
                                        $("#blog_details").append($("<div>").addClass("comment clearfix").attr({'id':'comments'})
                                                .append($("<h4>").addClass("title").append(asb))
                                                .append(commen_di)
                                                .append($("<div>").addClass("comments-form")
                                                    .append($("<div>").append($("<a>").attr({'data-toggle':'collapse','data-parent':'#accordion-2' ,'href':'#collapseTwo-comment' ,'class':'collapsed' ,'aria-expanded':'false'})
                                                        .append($("<i>").addClass("fa fa-comment pr-10")).append("Add your comment").click(function (){
                                                            id="";
                                                            <?php
                                                            if(isset($_SESSION['user_id']))
                                                                { ?>
                                                             id=<?php echo $_SESSION['user_id']; ?>;
                                                            <?php    }?>
                                                            if(id==""){
                                                             $("#mySignIn").modal("toggle")   ;
                                                             setTimeout(function (){
                                                                 $("#collapseTwo-comment").collapse('hide');
                                                             },500);
                                                             alert("It is only for Registered user.")
                                                            }
                                                        })))
                                                    .append($("<div>").addClass("panel-collapse collapse").attr({'id':'collapseTwo-comment','aria-expanded':'false'}).css({'height':'0px'})
                                                        .append($("<div>").attr({'id':'comment-form','role':'form'})
                                                            .append($("<div>").addClass("form-group has-feedback")
                                                                .append($("<textarea>").addClass("form-control")
                                                                .attr({'rows':'8','id':'message4','data-gramm':'true','data-txt_gramm_id':'bd4cb204-2cac-1700-1767-d4a0c03c0947','data-gramm_id':'bd4cb204-2cac-1700-1767-d4a0c03c0947','background':'transparent !important'})
                                                                .css({'z-index':'auto','position':'relative','line-height':'20px','font-size':'14px','transition':'none','spellcheck':'false'}))
                                                                .append($("<i>").addClass("fa fa-envelope-o form-control-feedback"))
                                                                .append($('<div class="validate pull-right" style="font-weight:bold;"></span></div>'))
                                                             )
                                                             .append($("<input>").attr({'type':'submit' ,'value':'Submit'}).addClass("btn btn-default").click(function (){
                                                                 if(id==""){
                                                                     $("#mySignIn").modal("toggle")   ;
                                                                     alert("It is only for Registered user.")
                                                                     return false;
                                                                 }else{
                                                                         if($(this).parent().find("textarea").val()==""){
                                                                             $(".validate").removeClass("text-danger").addClass("text-danger").fadeIn(100).text(" Please fill valid Comment ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                                                         }else{
                                                                             commen_di.append($("<div>").addClass("comment-content")
                                                                                .append($("<div>").addClass("comment-avatar").css({'width': '23px'})
                                                                                    .append($("<img>").addClass("img-circle").attr({'src':'../images/avatar.jpg'}))
                                                                                )
                                                                                .append($("<div>").addClass("comment-body clearfix")
                                                                                    .append($("<p>").append($(this).parent().find("textarea").val()))
                                                                                    .append($("<p>").addClass("small margin-clear").css({'color':'#ce0e0e'}).append($("<i>").addClass("fa fa-pencil")).append("&nbsp;Your Comment visble publicly after admin varification."))
                                                                                ))
                                                                        $.ajax({
                                                                            type:"post",
                                                                            url:"server/controller/InsBlogRplyDetails.php",
                                                                            data:{'blog_id':article.blog_details_id,'user_id':id,'comment':$(this).parent().find("textarea").val()},
                                                                            success: function(data) {
                                                                                
                                                                            }
                                                                            });
                                                                                $(this).parent().find("textarea").val("");
                                                                                
                                                                         }
                                                                     }
                                                             }))
                                                         )
                                                     )
                                               )
                                      );
                           }
                          
                        });
                    }
                });
                }
        </script>
</html>



