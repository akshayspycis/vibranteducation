$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    var refer_details_obj;
    $('#refer_details').find("#user_id").val(user_id)
    $('#refer_details').off("submit");
    $('#refer_details').submit(function() {
        contact_no=this.contact_no.value;
        if(this.user_name.value === "") {
            $("#refer_details").find("#errorMsg").empty(); 
            $("#refer_details").find("#errorMsg").append("Please Fill Name");
            this.user_name.focus();
            return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
           $("#refer_details").find("#errorMsg").empty(); 
            $("#refer_details").find("#errorMsg").append("Please Fill Correct Email");
            this.email.focus();
            return false;
        }
        else if(this.contact_no.value == ""|| !contactnofilter.test(this.contact_no.value)) {
           $("#refer_details").find("#errorMsg").empty(); 
            $("#refer_details").find("#errorMsg").append("Please Fill  Contact");
            this.contact_no.focus();
            return false;
        }else {
                $.ajax({
                    type:"post",
                    url:"server/controller/InsReferDetails.php",
                    data:$("#refer_details").serialize(),
                    success: function(data){
                        alert(data)
                        $('#success_msg_demo').modal('show'); 
                        setTimeout(function (){
                        window.location.replace("index.php");
                        },3000)

                    }
                });
        }
        return false;
    });
    
        
});

