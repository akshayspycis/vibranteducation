$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#userLogin').submit(function() {
        var path = window.location.pathname;
        var page = path.split("/").pop();
        if(this.email.value === "" || !emailfilter.test(this.email.value)) {
            $("#mySignIn").find("#errorMsg").empty(); 
            $("#mySignIn").find("#errorMsg").append("Please choose valid 'Email'.") ;
            this.email.focus();
            return false;
        }
        else if (this.password.value == "") {
            $("#mySignIn").find("#errorMsg").empty(); 
            $("#mySignIn").find("#errorMsg").append("Please choose valid 'Password'.") ;
            this.password.focus();
            return false;
        }
        
//        else if (this.terms.value == "") {
//            $("#mySignIn").find("#errorMsg").empty(); 
//            $("#mySignIn").find("#errorMsg").append("Please accept terms and conditions") ;
//            this.terms.focus();
//            return false;
//        }
        else {
            
             $("#mySignIn").find("#errorMsg").empty(); 
             $.ajax({
                type:"post",
                url:"server/controller/UserLogin.php",
                data:$('#userLogin').serialize(),
                success: function(data){
                    var result = $.trim(data);
                            if(result==="Error"){
                                $("#mySignIn").find("#errorMsg").empty(); 
                                $("#mySignIn").find("#errorMsg").append("Details is incorrect. Please try again'.") ;
                                $('#userLogin').each(function(){
                                this.reset();
                                this.email.focus();
                        });
                            }else if(result==="normal"){
                                 switch (page){
                                    case 'blogpostdetail.php':
                                       window.location.href = page+"?id="+urlParam1('id');
                                    break;
                                    default :
                                    window.location.href = page;
                               } 
                            }else if(result==="admin"){
                                window.location.href = "v-admin/home.php";
                            }
                            
                 }
               
            });
        }
        
        return false;
    });
    
});
 function urlParam1(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
 }

