$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    var contact_no;
    var course_registration={};
    var demo_date_id;
    var qualification_details={}
    var language_proficiency={}
    var work_experience={}
    var course_opted_details={}
    var bear_witness={}
    $('#password_code').modal({
      backdrop: 'static',
      keyboard: false
    }); 
//            $(".nav-pills").children().each(function(){
//                if($(this).find('a').attr('href')=='#pill-pr-8'){
//                    $(this).removeClass("disabled");
//                    $(this).addClass("active");
//                    $($(this).find('a').attr('href')).css({'display':'block'});
//                }else{
//                    $(this).removeClass("active");
//                    $(this).addClass("disabled");
//                    $(this).find('a').attr({'data-toggle':''});
//                    $($(this).find('a').attr('href')).css({'display':'none'});
//                }
//                
//            });
            
                        
                        
                        
    $('#password_code_form_submit').click(function() {
       if($("#registered_temp_code").val() === "" ) {
                showError($("#password_code"),"Please Enter Provided code");
                return false;
            }else{
                $.ajax({
                    type:"post",
                    url:"server/controller/SelUserRegistrationForDemo.php",
                    data:{'registered_temp_code':$("#registered_temp_code").val()},
                    success: function(data){
                      var duce = jQuery.parseJSON(data);
                      if(Object.keys(duce).length>0){
                       $('#password_code').modal('hide'); 
                        $.each(duce, function (index, article) {
                            $("#pill-pr-form-1").find("#user_name").val(article.user_name);
                            $("#pill-pr-form-1").find("#email").val(article.email);
                            $("#pill-pr-form-1").find("#contact_no").val(article.contact_no);
                            $("#pill-pr-form-1").find("input[name=gender][value=" + article.gender+ "]").prop('checked', true);
                            course_registration['user_registration_for_demo_id']=article.user_registration_for_demo_id;
                        });
                      }else{
                          showError($("#otpConfirm_form"),"Please Enter Valid code");
                      }
                    }
                });
            }
        return false;
    });
    $('#pill-pr-form-1').submit(function() {
        if(this.dob.value === "" ) {
                showError($("#pill-pr-form-1"),"Please Select Date of birth");
        }else{
            course_registration['dob']=this.dob.value;
            $(".nav-pills").children().each(function(){
                if($(this).find('a').attr('href')=='#pill-pr-2'){
                    $(this).removeClass("disabled");
                    $(this).addClass("active");
                    $($(this).find('a').attr('href')).css({'display':'block'});
                }else{
                    $(this).removeClass("active");
                    $(this).addClass("disabled");
                    $(this).find('a').attr({'data-toggle':''});
                    $($(this).find('a').attr('href')).css({'display':'none'});
                }
            });
        }
        return false;
    });
    $('#pill-pr-form-2').submit(function() {
        
        if(this.relative_name.value === "" ) {
                showError($("#pill-pr-form-2"),"Please Fill Relative Name");
        }else if(this.relative_contact_no.value === "" ) {
                showError($("#pill-pr-form-2"),"Please Fill Relative Contact No");
        }else if(this.relative_occupation.value === "" ) {
                showError($("#pill-pr-form-2"),"Please Fill Relative Occupation");
        }else if(this.school.value === "" ) {
                showError($("#pill-pr-form-2"),"Please Fill School Information");
        }else if(this.college.value === "" ) {
                showError($("#pill-pr-form-2"),"Please Fill College information");
        }else{
            course_registration['relative_name']=this.relative_name.value;
            course_registration['relative_occupation']=this.relative_occupation.value;
            course_registration['relative_contact_no']=this.relative_contact_no.value;
            course_registration['applicant_occupation']=this.applicant_occupation.value;
            course_registration['school']=this.school.value;
            course_registration['college']=this.college.value;
            course_registration['address_of_institiute_or_company']=this.address_of_institiute_or_company.value;
            course_registration['referal_name']=this.referal_name.value;
            course_registration['referal_contact_no']=this.referal_contact_no.value;
//            course_registration['user_id']=user_id;
            
            $(".nav-pills").children().each(function(){
                if($(this).find('a').attr('href')=='#pill-pr-3'){
                    $(this).removeClass("disabled");
                    $(this).addClass("active");
                    $($(this).find('a').attr('href')).css({'display':'block'});
                }else{
                    $(this).removeClass("active");
                    $(this).addClass("disabled");
                    $(this).find('a').attr({'data-toggle':''});
                    $($(this).find('a').attr('href')).css({'display':'none'});
                }
            });
        }
        return false;
    });
    $("#same_local_per").change(function() {
        if($("#same_local_per").prop("checked")){
           $('#pill-pr-form-3').find("#address_p").val($('#pill-pr-form-3').find("#address_l").val()); 
           $('#pill-pr-form-3').find("#street_p").val($('#pill-pr-form-3').find("#street_l").val()); 
           $('#pill-pr-form-3').find("#city_p").val($('#pill-pr-form-3').find("#city_l").val()); 
           $('#pill-pr-form-3').find("#pincode_p").val($('#pill-pr-form-3').find("#pincode_l").val()); 
           $('#pill-pr-form-3').find("#state_p").val($('#pill-pr-form-3').find("#state_l").val()); 
        }else{
           $('#pill-pr-form-3').find("#address_p").val(""); 
           $('#pill-pr-form-3').find("#street_p").val(""); 
           $('#pill-pr-form-3').find("#city_p").val(""); 
           $('#pill-pr-form-3').find("#pincode_p").val(""); 
           $('#pill-pr-form-3').find("#state_p").val(""); 
        }
    });
    $('#pill-pr-form-3').submit(function() {
        if(this.address_l.value === "" ) {
            showError($("#pill-pr-form-3"),"Please Fill Local Address");
        }else if(this.street_l.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Local Street Name");
        }else if(this.city_l.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Local City Name");
        }else if(this.pincode_l.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Local pincode");
        }else if(this.state_l.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Select Local state");
        }else if(!$("#same_local_per").prop("checked") && this.address_p.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Permanent Address");
        }else if(!$("#same_local_per").prop("checked") && this.street_p.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Permanent Street Name");
        }else if(!$("#same_local_per").prop("checked") && this.city_p.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Permanent City Name");
        }else if(!$("#same_local_per").prop("checked") && this.pincode_p.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Fill Permanent pincode");
        }else if(!$("#same_local_per").prop("checked") && this.state_p.value === "" ) {
                showError($("#pill-pr-form-3"),"Please Permanent Select state");
        }else{
            var address_details={'Permanent':{
                    'address':this.address_p.value,
                    'street':this.street_p.value,
                    'city':this.city_p.value,
                    'pincode':this.pincode_p.value,
                    'state':this.state_p.value
                }
                ,'Local':{
                    'address':this.address_l.value,
                    'street':this.street_l.value,
                    'city':this.city_l.value,
                    'pincode':this.pincode_l.value,
                    'state':this.state_l.value
                 }
             }
             course_registration['address_details']=address_details;
            $(".nav-pills").children().each(function(){
                if($(this).find('a').attr('href')=='#pill-pr-4'){
                    $(this).removeClass("disabled");
                    $(this).addClass("active");
                    $($(this).find('a').attr('href')).css({'display':'block'});
                }else{
                    $(this).removeClass("active");
                    $(this).addClass("disabled");
                    $(this).find('a').attr({'data-toggle':''});
                    $($(this).find('a').attr('href')).css({'display':'none'});
                }
            });
        }
        return false;
    });
    
    $('#pill-pr-form-4').submit(function() {
     var b=true;
     $('#qualification_details').children().each(function () {
            if($(this).find('td:nth-child(1) input').val()=="10th"){
                if(!setQualificationDetails($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
            if($(this).find('td:nth-child(1) input').val()=="12th"){
                if(!setQualificationDetails($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
            if($(this).find('td:nth-child(1) input').attr('placeholder')=="U G"){
                if($(this).find('td:nth-child(1) input').val()==""){
                    showError($("#pill-pr-form-4"),"Please Fill Graduation Name");
                    b=false;
                    return false;
                }else{
                    if(!setQualificationDetails($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
                }
            }
            if($(this).find('td:nth-child(1) input').attr('placeholder')=="Post G" && $(this).find('td:nth-child(1) input').val()!=""){
                if($(this).find('td:nth-child(1) input').val()==""){
                    showError($("#pill-pr-form-4"),"Please Fill Post Graduation Name");
                    b=false;
                    return false;
                }else{
                    if(!setQualificationDetails($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
                }
            }
            
        });
        if(b){
                course_registration['qualification_details']=qualification_details;
                $(".nav-pills").children().each(function(){
                    if($(this).find('a').attr('href')=='#pill-pr-5'){
                        $(this).removeClass("disabled");
                        $(this).addClass("active");
                        $($(this).find('a').attr('href')).css({'display':'block'});
                    }else{
                        $(this).removeClass("active");
                        $(this).addClass("disabled");
                        $(this).find('a').attr({'data-toggle':''});
                        $($(this).find('a').attr('href')).css({'display':'none'});
                    }
                });
            }
        return false;
    });
    
    $('#pill-pr-form-5').submit(function() {
     var b=true;
     $('#language_proficiency').children().each(function () {
            if($(this).find('td:nth-child(1) input').val()=="English"){
                if(!setLanguageProficiency($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
            if($(this).find('td:nth-child(1) input').val()=="Hindi"){
                if(!setLanguageProficiency($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
            if($(this).find('td:nth-child(1) input').attr('placeholder')=="Other (specify)" && $(this).find('td:nth-child(1) input').val()!=""){
                if(!setLanguageProficiency($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
        });
        if(b){
                course_registration['additional_qualification']=this.additional_qualification.value;
                course_registration['computer_literacy']=this.computer_literacy.value;
                course_registration['language_proficiency']=language_proficiency;
                $(".nav-pills").children().each(function(){
                    if($(this).find('a').attr('href')=='#pill-pr-6'){
                        $(this).removeClass("disabled");
                        $(this).addClass("active");
                        $($(this).find('a').attr('href')).css({'display':'block'});
                    }else{
                        $(this).removeClass("active");
                        $(this).addClass("disabled");
                        $(this).find('a').attr({'data-toggle':''});
                        $($(this).find('a').attr('href')).css({'display':'none'});
                    }
                });
            }
        return false;
    });
    $('#add_work_experience').click(function() {
        $('#work_experience').append('<tr><td><input type="text" class="form-control" id="organization" name="organization" placeholder="Organisation Name"></td><td><input type="text" class="form-control" id="time" name="time" ></td><td><input type="text" class="form-control" id="designation" name="designation" ></td></tr>');
    });
    $('#pill-pr-form-6').submit(function() {
     var b=true;
     $('#work_experience').children().each(function () {
            if($(this).find('td:nth-child(1) input').attr('placeholder')=="Organisation Name" && $(this).find('td:nth-child(1) input').val()!=""){
                if(!setWorkExperience($(this).find('td:nth-child(1) input').val(),$(this))){b=false;return false};
            }
        });
        if(b){
                course_registration['work_experience']=work_experience;
                $(".nav-pills").children().each(function(){
                    if($(this).find('a').attr('href')=='#pill-pr-7'){
                        $(this).removeClass("disabled");
                        $(this).addClass("active");
                        $($(this).find('a').attr('href')).css({'display':'block'});
                    }else{
                        $(this).removeClass("active");
                        $(this).addClass("disabled");
                        $(this).find('a').attr({'data-toggle':''});
                        $($(this).find('a').attr('href')).css({'display':'none'});
                    }
                });
            }
        return false;
    });
    
    $('#pill-pr-form-7').submit(function() {
     var b=false;
     $('#course_opted_details').children().each(function () {
            if($(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked")){
                b=true;
            }
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="course_po_mt_and_clerk")
                    course_opted_details['course_po_mt_and_clerk']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="advance_comm_english")
                    course_opted_details['advance_comm_english']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="ssc_and_railways")
                    course_opted_details['ssc_and_railways']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="english_for_competitive_exams")
                    course_opted_details['english_for_competitive_exams']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="personal_interviews")
                    course_opted_details['personal_interviews']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
                if($(this).find('td:nth-child(3) input[type="checkbox"]').attr('id')=="mba_entrance_exam")
                    course_opted_details['mba_entrance_exam']=$(this).find('td:nth-child(3) input[type="checkbox"]').prop("checked");
     });
            if(b){
                course_registration['course_opted_details']=course_opted_details;
                $(".nav-pills").children().each(function(){
                    if($(this).find('a').attr('href')=='#pill-pr-8'){
                        $(this).removeClass("disabled");
                        $(this).addClass("active");
                        $($(this).find('a').attr('href')).css({'display':'block'});
                    }else{
                        $(this).removeClass("active");
                        $(this).addClass("disabled");
                        $(this).find('a').attr({'data-toggle':''});
                        $($(this).find('a').attr('href')).css({'display':'none'});
                    }
                });
                var obj_class={};
            setTimeout(function(){
            for (var i=1;i<5;i++){
                
                var obj = {
                        support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
                        form: "pill-pr-form-upload-img-"+i, // Form ID
                        dragArea: "dragAndDropFiles"+i, // Upload Area ID
                        multiUpload:"multiUpload"+i,
                        url:"server/controller/InsImg.php",
                        onlad:returnImg,
                        strlenght:25,
                        module:i
                        }
                        if(obj_class[i]==null){
                                obj_class[i]=new $.UploadImg(obj);
                        }
            }
                },2000);
            }else{
                showError($("#pill-pr-form-7"),"Please select at least one Course Opted");
            }
        return false;
    });
    
    function setQualificationDetails(cls,obj){
        if(obj.find('td:nth-child(2) input').val()==""){
            showError($("#pill-pr-form-4"),"Please Fill "+cls+" School/College Name");
            return false;
        }else if(obj.find('td:nth-child(3) input').val()==""){
            showError($("#pill-pr-form-4"),"Please Fill "+cls+" Board/Universit Name");
            return false;
        }else if(obj.find('td:nth-child(4) input').val()==""){
            showError($("#pill-pr-form-4"),"Please Fill "+cls+" Year of passing");
            return false;
        }else if(obj.find('td:nth-child(5) input').val()==""){
            showError($("#pill-pr-form-4"),"Please Fill "+cls+" Percentage");
            return false;
        }else{
            qualification_details[cls]={}
            qualification_details[cls]['class']=obj.find('td:nth-child(1) input').val();
            qualification_details[cls]['school_college']=obj.find('td:nth-child(2) input').val();
            qualification_details[cls]['board_university']=obj.find('td:nth-child(3) input').val();
            qualification_details[cls]['year_of_passing']=obj.find('td:nth-child(4) input').val();
            qualification_details[cls]['percentage']=obj.find('td:nth-child(5) input').val();
            return true;
        }   
    }
    
    function setLanguageProficiency(cls,obj){
        var b=false;
        if(obj.find('td:nth-child(2) input[type="checkbox"]').prop("checked")){
            b=true;
        }
        if(obj.find('td:nth-child(3) input[type="checkbox"]').prop("checked")){
            b=true;
        }
        if(obj.find('td:nth-child(4) input[type="checkbox"]').prop("checked")){
            b=true;
        }
        if(b){
            language_proficiency[cls]={}
            language_proficiency[cls]['language']=obj.find('td:nth-child(1) input').val();
            language_proficiency[cls]['read']=obj.find('td:nth-child(2) input[type="checkbox"]').prop("checked");
            language_proficiency[cls]['write']=obj.find('td:nth-child(3) input[type="checkbox"]').prop("checked");
            language_proficiency[cls]['speak']=obj.find('td:nth-child(4) input[type="checkbox"]').prop("checked");
            return true;
        }else{
            showError($("#pill-pr-form-5"),"Please tick √ any option "+cls+" Proficiency ");
            return false;
        }   
    }
    
    function setWorkExperience(cls,obj){
        if(obj.find('td:nth-child(2) input').val()==""){
            showError($("#pill-pr-form-6"),"Please Fill Experience (in months till date) in "+cls+"");
            return false;
        }else if(obj.find('td:nth-child(3) input').val()==""){
            showError($("#pill-pr-form-6"),"Please Fill Job Profile or Designation in "+cls+"");
            return false;
        }else{
            work_experience[cls]={}
            work_experience[cls]['organization']=obj.find('td:nth-child(1) input').val();
            work_experience[cls]['time']=obj.find('td:nth-child(2) input').val();
            work_experience[cls]['designation']=obj.find('td:nth-child(3) input').val();
            return true;
        }   
    }
    
    function showError(form,title){
        form.find("#errorMsg").html(title);
    }
    
    function returnImg(module,img_path){
        var attestation;
        switch (module){
            case 1:
                attestation="Photograph";
                break;
            case 2:
                attestation="Id Proof";
                break;
            case 3:
                attestation="Address proof";
                break;
            case 4:
                attestation=$('#specify').val();
                break;
        }
        bear_witness[module]={
            'attestation':attestation,
            'pic_path':img_path,
        }
        course_registration["bear_witness"]=bear_witness;
    }
    
    $("#final_save").click(function(){
        var course_registration_json=JSON.stringify(course_registration);
        $.ajax({
            type:"post",
            url:"server/controller/InsCourseRegistration.php",
            data:{'course_registration':course_registration_json},
            success: function(data){
                if(data.trim()!='Error'){
                    $('#success_msg_demo').modal('show'); 
                    setTimeout(function (){
                        location.reload();
                    },4000)
                }else{
                    alert("Server Does not responded");
                }
            }
        });
    });
        
});

            
                        
                        