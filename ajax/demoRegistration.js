var check_demo_reg_form=true;
$(document).ready(function() {
    $("#otp_resend").click(function (){
         var that=$(this);
                 that.html($("<img>").attr({'src':'assets/images/fb_ty.gif'}));
         $.ajax({
            type:"post",
            url:"server/controller/ResendOtp.php",
            data:{'demo_user_contact':contact_no},
            success: function(data){
              that.html("Re Send");
              if(data.trim()!='Error'){

              }
            }

        });
     });
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    var contact_no;
    var demo_class_id;
    var demo_date_id;
    var user_registration_for_demo;
     $(".modal").on('hide.bs.modal', function() {
    	check_demo_reg_form=true;
     });
     $(".modal").on('shown.bs.modal', function() {
    	check_demo_reg_form=false;
     });
     
    $("#demoRegistration").on('shown.bs.modal', function(){
        check_demo_reg_form=false;
        $("#demo_class_button").empty();
        $.ajax({
            type:"post",
            url:"server/controller/SelDemoClassClient.php",
                success: function(data) {
                    $("#demo_class_button").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                            if(article.status=="Enable"){
                                var ba=$('<div>').addClass("")
                                .append($('<button>').addClass("btn square").attr({'type':'button'}).append($("<b>").html(article.demo_class)))
                                .click(function (){
                                        selDemoDate(article.demo_class_id);
                                    });
                                $("#demo_class_button").append(ba)
                                if(index==0){selDemoDate(article.demo_class_id);}
                            }
                        });
                    }
        });
    });
    
    function selDemoDate(id){
        demo_class_id=id;
        $("#loading_date").find('img').css({'display':'block'});
        $.ajax({
            type:"post",
            url:"server/controller/SelDemoDateClient.php",
            data:{'demo_class_id':demo_class_id},
                success: function(data) {
                    $("#loading_date").find('img').css({'display':'none'});
                    $("#demo_user_select_date").empty();
                    $("#demo_user_select_date").append('<option value="0">Please Select Date</option>');
                    var duce = jQuery.parseJSON(data);
                    $.each(duce, function (index, article) {
                       $("#demo_user_select_date").append($('<option>').attr({'value':article.demo_date_id}).html(article.date));
                    });
                }
        });
    }
    
    $('#userDemoRegistration').off("submit");
    $('#userDemoRegistration').submit(function() { 
        contact_no=this.demo_user_contact.value;
        if(this.demo_user_name.value === "") {
             $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Fill Name");
            this.demo_user_name.focus();
            return false;
        }
        else if(this.demo_user_gender.value === "") {
             $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Select Gender");
            this.demo_user_name.focus();
            return false;
        }
        else if(this.demo_user_email.value === "" || !emailfilter.test(this.demo_user_email.value)) {
           $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Fill Correct Email");
            this.demo_user_email.focus();
            return false;
        }
        else if(this.demo_user_contact.value == ""|| !contactnofilter.test(this.demo_user_contact.value)) {
           $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Fill  Contact");
            this.demo_user_contact.focus();
            return false;
        }
        else if (this.demo_user_select_date.value == "") {
           $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Select Date");
            this.demo_user_select_date.focus();
            return false;
        }
        
        else if($(this.terms).is(":not(:checked)")){
                $("#userDemoRegistration").find("#errorMsg").empty(); 
             $("#userDemoRegistration").find("#errorMsg").append("Please Accept Terms & Conditions");
            this.terms.focus();
            return false;
         }
        else {
              user_registration_for_demo = new Object();
                        user_registration_for_demo['user_name']=this.demo_user_name.value;
                        user_registration_for_demo['gender']=this.demo_user_gender.value;
                        user_registration_for_demo['email']=this.demo_user_email.value;
                        user_registration_for_demo['contact_no']=contact_no;
                        user_registration_for_demo['demo_class_id']=demo_class_id;
                        user_registration_for_demo['demo_date_id']=this.demo_user_select_date.value;
              $('#demoRegistration').modal('hide'); 
              $('#checkOtp').modal('show'); 
            
        }
        closeNav();
        return false;
    });
    $("#checkOtp").on('shown.bs.modal', function(){
                        var otp_no;
                         $.ajax({
                            type:"post",
                            url:"server/controller/genrateOtp.php",
                            data:{'demo_user_contact':contact_no},
                            success: function(data){
                              if(data.trim()!='Error'){
                                  
                              }
                            }

                        });
      
                       
    }); 
        $('#otpConfirm_form').submit(function() {
//        alert("hello");
       if($("#confirmCode").val() === "" ) {
                $("#otpConfirm_form").find("#errorMsg").empty(); 
                $("#otpConfirm_form").find("#errorMsg").append("Please Enter Six digit code");
                return false;
            }else{
                otp_no=$("#confirmCode").val();
                var as=JSON.stringify(user_registration_for_demo);
                $.ajax({
                    type:"post",
                    url:"server/controller/checkOtp.php",
                    data:{'contact_no':contact_no,'otp_no':otp_no},
                    success: function(data){
                      if(data.trim()!='Error'){
                        $('#checkOtp').modal('hide'); 
                        $('#success_msg_demo').modal('show'); 
                             $.ajax({
                                type:"post",
                                url:"server/controller/InsDemoRegistration.php",
                                data:{'user_registration_for_demo':as},
                                success: function(data){

                                    setTimeout(function (){
//                                        location.reload();
                                    window.location.replace("index.php");
                                    },3000)
                                    
                                }
                            });
                      }else{
                          $("#otpConfirm_form").find("#errorMsg").html("Please Enter Valid Six digit code");
                          
                      }
                    }
                });
            }
        return false;});
});

