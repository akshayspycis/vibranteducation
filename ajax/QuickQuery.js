$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#quick-query').submit(function() { 
        if(this.name.value === "") {
//             alert("Hello");
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                  $("#errormsg").find("#msg").empty(); 
                  $("#errormsg").find("#msg").append("Please choose valid 'Name'.") ;
                 
            });
            this.name.focus();
            return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
//                         alert("Hello");
             $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                  $("#errormsg").find("#msg").empty(); 
                  $("#errormsg").find("#msg").append("Please choose valid 'Email'.") ;
                 
            });
            this.email.focus();
            return false;
        }
        else if(this.contact.value == ""|| !contactnofilter.test(this.contact.value)) {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Contact No'.") ;
                        
            });
           
            this.contact.focus();
            return false;
        }
        else if (this.subject.value == "") {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Subject'.") ;
                
            });
            this.subject.focus();
            return false;
        }
        
        else if (this.message.value == "") {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Message'.") ;
                
            });
            this.message.focus();
            return false;
        }
        else {   
            
                        quick_query  = new Object();
                        quick_query ['name']=this.name.value;
                        quick_query ['email']=this.email.value;
                        quick_query ['contact']=this.contact.value;
                        quick_query ['subject']=this.subject.value;
                        quick_query ['message']=this.message.value;
                        $('#checkOtp1').modal('show'); 
        }
        closeNav();
        return false;
    });
     $("#checkOtp1").on('shown.bs.modal', function(){
                        var otp_no;
                         $.ajax({
                            type:"post",
                            url:"server/controller/genrateOtp.php",
                            data:{'demo_user_contact':quick_query ['contact']},
                            success: function(data){
                              if(data.trim()!='Error'){
                                  
                              }
                            }

                        });
                       
    }); 
     $('#otpConfirm_form1').submit(function() {
//        alert("hello");
       if($("#confirmCode1").val() === "" ) {
                $("#otpConfirm_form1").find("#errorMsg").empty(); 
                $("#otpConfirm_form1").find("#errorMsg").append("Please Enter Six digit code");
                return false;
            }else{
                var otp_no=$("#confirmCode1").val();
                $.ajax({
                    type:"post",
                    url:"server/controller/checkOtp.php",
                    data:{'contact_no':quick_query ['contact'],'otp_no':otp_no},
                    success: function(data){
                      if(data.trim()!='Error'){
                           $.ajax({
                                type:"post",
                                url:"server/controller/InsQuickQuery.php",
                                data:quick_query,
                                success: function(data){
                                    $('#checkOtp1').modal('hide'); 
                //                   alert(data.trim());
                                    $('#enquirysuccess').modal('show'); 
                                    $("#enquirysuccess").on('shown.bs.modal', function(){
                                        //                        $('#msg').append(data) ;
                                    }); 
                                    setTimeout(function(){
                                        $('#enquirysuccess').modal('hide')
                                    }, 10000);
                                    $('#quick-query').each(function(){
                                        this.reset();
                                    });
                                  $('#quick-query').find('#submit').attr("disabled", false); 
                                }
                            });
                      }else{
                          $("#otpConfirm_form1").find("#errorMsg").html("Please Enter Valid Six digit code");
                          
                      }
                    }
                });
            }
        return false;});
});

