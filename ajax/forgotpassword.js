$(document).ready(function() {
    $("#forgotpassword_button").click(function (){
        $('#mySignIn').modal("hide");
        $('#forgotpassword_model').modal("show");
    });
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    $('#forgotpassword').submit(function() {
        var path = window.location.pathname;
        var page = path.split("/").pop();
        var forgotemail=this.forgotemail.value;
        if(this.forgotemail.value === "" || !emailfilter.test(this.forgotemail.value)) {
            $("#forgotpassword").find("#errorMsg").empty(); 
            $("#forgotpassword").find("#errorMsg").append("Please choose valid 'Email'.") ;
            this.forgotemail.focus();
            return false;
        } else {
             $("#forgotpassword").find("#errorMsg").empty(); 
             $("#forgotpassword").find("#errorMsg").html($("<img>").attr({'src':'assets/images/fb_ty.gif'}));
             $.ajax({
                type:"post",
                url:"server/controller/ForgotPassword.php",
                data:$('#forgotpassword').serialize(),
                success: function(data){
                    $("#forgotpassword").find("#errorMsg").empty(); 
                    var result = $.trim(data);
                            if(result==="Error"){
                                $("#forgotpassword").find("#errorMsg").empty(); 
                                $("#forgotpassword").find("#errorMsg").append("Details is incorrect. Please try again'.") ;
                                $('#forgotpassword').each(function(){
                                    this.reset();
                                });
                            }else {
                                $("#forgotpassword").find("#successMsg").append("We've your password sent an email to "+forgotemail+".'.") ;
                                setTimeout(function (){
                                     $("#forgotpassword").find("#successMsg").empty(); 
                                     $('#forgotpassword').each(function(){
                                            this.reset();
                                      });
                                    $('#forgotpassword_model').modal("hide");
                                    $('#mySignIn').modal("show");
                                },3000);
                            }
                 }
            });
        }
        return false;
    });
    
});
 function urlParam1(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
 }

