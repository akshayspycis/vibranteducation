<?php
    class PrelimsExam{
        private $prelimsexam_id;
        private $exam_id;
        private $exam_sub_cat_id;
        private $exam_phase_id;
        private $exam_section_id;
        private $no_of_questions;
        private $max_marks;
        private $duration;
        private $gen;
        private $obc;
        private $sc;
        private $st;
        private $ph;
        private $vh;
        private $year;
        
        function getPrelimsexam_id() {
            return $this->prelimsexam_id;
        }

        function getExam_id() {
            return $this->exam_id;
        }

        function getExam_sub_cat_id() {
            return $this->exam_sub_cat_id;
        }

        function getExam_phase_id() {
            return $this->exam_phase_id;
        }

        function getExam_section_id() {
            return $this->exam_section_id;
        }

        function getNo_of_questions() {
            return $this->no_of_questions;
        }

        function getMax_marks() {
            return $this->max_marks;
        }

        function getDuration() {
            return $this->duration;
        }

        function getGen() {
            return $this->gen;
        }

        function getObc() {
            return $this->obc;
        }

        function getSc() {
            return $this->sc;
        }

        function getSt() {
            return $this->st;
        }

        function getPh() {
            return $this->ph;
        }

        function getVh() {
            return $this->vh;
        }

        function getYear() {
            return $this->year;
        }

        function setPrelimsexam_id($prelimsexam_id) {
            $this->prelimsexam_id = $prelimsexam_id;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

        function setExam_sub_cat_id($exam_sub_cat_id) {
            $this->exam_sub_cat_id = $exam_sub_cat_id;
        }

        function setExam_phase_id($exam_phase_id) {
            $this->exam_phase_id = $exam_phase_id;
        }

        function setExam_section_id($exam_section_id) {
            $this->exam_section_id = $exam_section_id;
        }

        function setNo_of_questions($no_of_questions) {
            $this->no_of_questions = $no_of_questions;
        }

        function setMax_marks($max_marks) {
            $this->max_marks = $max_marks;
        }

        function setDuration($duration) {
            $this->duration = $duration;
        }

        function setGen($gen) {
            $this->gen = $gen;
        }

        function setObc($obc) {
            $this->obc = $obc;
        }

        function setSc($sc) {
            $this->sc = $sc;
        }

        function setSt($st) {
            $this->st = $st;
        }

        function setPh($ph) {
            $this->ph = $ph;
        }

        function setVh($vh) {
            $this->vh = $vh;
        }

        function setYear($year) {
            $this->year = $year;
        }




}
?>