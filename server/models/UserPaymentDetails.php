<?php
    class UserPaymentDetails{
        
        private $user_payment_details_id;
        private $course_registration_id;
        private $payment;
        private $paid;
        private $balance;
  
        function getUser_payment_details_id() {
            return $this->user_payment_details_id;
        }

        function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        function getPayment() {
            return $this->payment;
        }

        function getPaid() {
            return $this->paid;
        }

        function getBalance() {
            return $this->balance;
        }

        function setUser_payment_details_id($user_payment_details_id) {
            $this->user_payment_details_id = $user_payment_details_id;
        }

        function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }

        function setPayment($payment) {
            $this->payment = $payment;
        }

        function setPaid($paid) {
            $this->paid = $paid;
        }

        function setBalance($balance) {
            $this->balance = $balance;
        }


        
    }


