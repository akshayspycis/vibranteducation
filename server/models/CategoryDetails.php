<?php
    class CategoryDetails {
        private $category_details_id;
        private $category;
        private $type;
        function getCategory_details_id() {
            return $this->category_details_id;
        }

        function getCategory() {
            return $this->category;
        }

        function getType() {
            return $this->type;
        }

        function setCategory_details_id($category_details_id) {
            $this->category_details_id = $category_details_id;
        }

        function setCategory($category) {
            $this->category = $category;
        }

        function setType($type) {
            $this->type = $type;
        }


    }


