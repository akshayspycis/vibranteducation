<?php
    class Selections{
        private $id;
        private $name;
        private $designation;
        private $orgname;
        private $image;
        private $date;
        
        function getId() {
            return $this->id;
        }

        function getName() {
            return $this->name;
        }

        function getDesignation() {
            return $this->designation;
        }

        function getOrgname() {
            return $this->orgname;
        }

        function getImage() {
            return $this->image;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setDesignation($designation) {
            $this->designation = $designation;
        }

        function setOrgname($orgname) {
            $this->orgname = $orgname;
        }

        function setImage($image) {
            $this->image = $image;
        }

        function setDate($date) {
            $this->date = $date;
        }


       
    }




