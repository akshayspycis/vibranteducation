<?php
    class FileDetails{

        private $file_details_id;
        private $category_details_id;
        private $title;
        private $discription;
        private $path;
        private $status;
        function getFile_details_id() {
            return $this->file_details_id;
        }

        function getCategory_details_id() {
            return $this->category_details_id;
        }

        function getTitle() {
            return $this->title;
        }

        function getDiscription() {
            return $this->discription;
        }

        function getPath() {
            return $this->path;
        }

        function getStatus() {
            return $this->status;
        }

        function setFile_details_id($file_details_id) {
            $this->file_details_id = $file_details_id;
        }

        function setCategory_details_id($category_details_id) {
            $this->category_details_id = $category_details_id;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setDiscription($discription) {
            $this->discription = $discription;
        }

        function setPath($path) {
            $this->path = $path;
        }

        function setStatus($status) {
            $this->status = $status;
        }


    }


