<?php
    class QuickQuery{
        private $quick_query_id;
        private $name;
        private $email;
        private $contact;
        private $subject;
        private $message;
        private $date;
        private $course_id;
        
        function getQuick_query_id() {
            return $this->quick_query_id;
        }

        function getName() {
            return $this->name;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact() {
            return $this->contact;
        }

        function getSubject() {
            return $this->subject;
        }

        function getMessage() {
            return $this->message;
        }

        function getDate() {
            return $this->date;
        }

        function getCourse_id() {
            return $this->course_id;
        }

        function setQuick_query_id($quick_query_id) {
            $this->quick_query_id = $quick_query_id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact($contact) {
            $this->contact = $contact;
        }

        function setSubject($subject) {
            $this->subject = $subject;
        }

        function setMessage($message) {
            $this->message = $message;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setCourse_id($course_id) {
            $this->course_id = $course_id;
        }



    }



