<?php
    class ThoughtOfDay{
        private $thoughtofday_id;
        private $content;
        private $author;
        private $user_registration_for_demo_id;
        private $status;
        private $posteddate;
        
        
        function getThoughtofday_id() {
            return $this->thoughtofday_id;
        }

        function getContent() {
            return $this->content;
        }

        function getAuthor() {
            return $this->author;
        }

        function getUser_registration_for_demo_id() {
            return $this->user_registration_for_demo_id;
        }

        function getStatus() {
            return $this->status;
        }

        function getPosteddate() {
            return $this->posteddate;
        }

        function setThoughtofday_id($thoughtofday_id) {
            $this->thoughtofday_id = $thoughtofday_id;
        }

        function setContent($content) {
            $this->content = $content;
        }

        function setAuthor($author) {
            $this->author = $author;
        }

        function setUser_registration_for_demo_id($user_registration_for_demo_id) {
            $this->user_registration_for_demo_id = $user_registration_for_demo_id;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setPosteddate($posteddate) {
            $this->posteddate = $posteddate;
        }


        
        
    }



