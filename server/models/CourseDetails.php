<?php
    class CourseDetails{
        
        private $course_details_id;
        private $title;
        private $long_description;
        private $status;
        private $pic;
        function getCourse_details_id() {
            return $this->course_details_id;
        }

        function getTitle() {
            return $this->title;
        }

        function getLong_description() {
            return $this->long_description;
        }

        function getStatus() {
            return $this->status;
        }

        function getPic() {
            return $this->pic;
        }

        function setCourse_details_id($course_details_id) {
            $this->course_details_id = $course_details_id;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setLong_description($long_description) {
            $this->long_description = $long_description;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setPic($pic) {
            $this->pic = $pic;
        }


        }



