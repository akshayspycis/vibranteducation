<?php
    class LanguageProficiency{
        private $language;
        private $read;
        private $write;
        private $speak;
        private $course_registration_id;
        
        function getLanguage() {
            return $this->language;
        }

        function getRead() {
            return $this->read;
        }

        function getWrite() {
            return $this->write;
        }

        function getSpeak() {
            return $this->speak;
        }

        function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        function setLanguage($language) {
            $this->language = $language;
        }

        function setRead($read) {
            $this->read = $read;
        }

        function setWrite($write) {
            $this->write = $write;
        }

        function setSpeak($speak) {
            $this->speak = $speak;
        }

        function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


       
        

    }


