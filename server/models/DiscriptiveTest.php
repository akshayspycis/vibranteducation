<?php
    class DiscriptiveTest{
        
        private $discriptivetest_id;
        private $exam_id;
        private $exam_sub_cat_id;
        private $exam_phase_id;
        private $papername;
        private $papertype;
        private $duration;
        private $maxmarks;
        private $year;
        
        function getDiscriptivetest_id() {
            return $this->discriptivetest_id;
        }

        function getExam_id() {
            return $this->exam_id;
        }

        function getExam_sub_cat_id() {
            return $this->exam_sub_cat_id;
        }

        function getExam_phase_id() {
            return $this->exam_phase_id;
        }

        function getPapername() {
            return $this->papername;
        }

        function getPapertype() {
            return $this->papertype;
        }

        function getDuration() {
            return $this->duration;
        }

        function getMaxmarks() {
            return $this->maxmarks;
        }

        function getYear() {
            return $this->year;
        }

        function setDiscriptivetest_id($discriptivetest_id) {
            $this->discriptivetest_id = $discriptivetest_id;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

        function setExam_sub_cat_id($exam_sub_cat_id) {
            $this->exam_sub_cat_id = $exam_sub_cat_id;
        }

        function setExam_phase_id($exam_phase_id) {
            $this->exam_phase_id = $exam_phase_id;
        }

        function setPapername($papername) {
            $this->papername = $papername;
        }

        function setPapertype($papertype) {
            $this->papertype = $papertype;
        }

        function setDuration($duration) {
            $this->duration = $duration;
        }

        function setMaxmarks($maxmarks) {
            $this->maxmarks = $maxmarks;
        }

        function setYear($year) {
            $this->year = $year;
        }


}



