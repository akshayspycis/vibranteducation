<?php
    class AddressDetails{
        private $address_details_id;
        private $address_type;
        private $address;
        private $street;
        private $city;
        private $pincode;
        private $state;
        private $course_registration_id;
        
        
        function getAddress_details_id() {
            return $this->address_details_id;
        }

        function getAddress_type() {
            return $this->address_type;
        }

        function getAddress() {
            return $this->address;
        }

        function getStreet() {
            return $this->street;
        }

        function getCity() {
            return $this->city;
        }

        function getPincode() {
            return $this->pincode;
        }

        function getState() {
            return $this->state;
        }

        function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        function setAddress_details_id($address_details_id) {
            $this->address_details_id = $address_details_id;
        }

        function setAddress_type($address_type) {
            $this->address_type = $address_type;
        }

        function setAddress($address) {
            $this->address = $address;
        }

        function setStreet($street) {
            $this->street = $street;
        }

        function setCity($city) {
            $this->city = $city;
        }

        function setPincode($pincode) {
            $this->pincode = $pincode;
        }

        function setState($state) {
            $this->state = $state;
        }

        function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


        

    }


