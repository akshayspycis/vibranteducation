<?php
    class Noticeboard {
        private $id;
        private $heading;
        private $content;
        private $date;
        function getId() {
            return $this->id;
        }

        function getHeading() {
            return $this->heading;
        }

        function getContent() {
            return $this->content;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setHeading($heading) {
            $this->heading = $heading;
        }

        function setContent($content) {
            $this->content = $content;
        }

        function setDate($date) {
            $this->date = $date;
        }


}