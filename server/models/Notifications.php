<?php
    class Notifications {
        private $id;
        private $heading;
        private $link;
        private $date;
        function getId() {
            return $this->id;
        }

        function getHeading() {
            return $this->heading;
        }

        function getLink() {
            return $this->link;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setHeading($heading) {
            $this->heading = $heading;
        }

        function setLink($link) {
            $this->link = $link;
        }

        function setDate($date) {
            $this->date = $date;
        }
}