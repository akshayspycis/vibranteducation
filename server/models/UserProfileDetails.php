<?php
    class UserProfileDetails{
        
        private $user_profile_details_id;
        private $user_id;
        private $pic;
        private $date;
        
        function getUser_profile_details_id() {
            return $this->user_profile_details_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getPic() {
            return $this->pic;
        }

        function getDate() {
            return $this->date;
        }

        function setUser_profile_details_id($user_profile_details_id) {
            $this->user_profile_details_id = $user_profile_details_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setPic($pic) {
            $this->pic = $pic;
        }

        function setDate($date) {
            $this->date = $date;
        }

   
         }


