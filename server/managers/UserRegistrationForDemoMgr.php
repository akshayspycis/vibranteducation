
<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class UserRegistrationForDemoMgr{    
        public function insUserRegistrationForDemo(UserRegistrationForDemo $user_registration_for_demo) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_registration_for_demo( "
                    . "user_name, "
                    . "gender, "
                    . "email, "
                    . "contact_no) "
                    . "VALUES ('".$user_registration_for_demo->getUser_name()."',"
                    . "'".$user_registration_for_demo->getGender()."',"
                    . "'".$user_registration_for_demo->getEmail()."',"
                    . "'".$user_registration_for_demo->getContact_no()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delUserRegistrationForDemo($user_registration_for_demo_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_registration_for_demo where user_registration_for_demo_id = '".$user_registration_for_demo_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selUserRegistrationForDemo( $registered_temp_code ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from user_registration_for_demo b where user_registration_for_demo_id=(select user_registration_for_demo_id from user_demo_date where registered_temp_code='".$registered_temp_code."')" ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selUserRegistrationForDemoAll() {
            $dbh = new DatabaseHelper();
            $sql = "select *,"
                    . "(select demo_class from demo_class where demo_class_id=udd.demo_class_id) as demo_class,"
                    . "(select date from demo_date where demo_date_id=udd.demo_date_id) as date"
                    . " from user_registration_for_demo urfd "
                    . "INNER JOIN user_demo_date udd ON udd.user_registration_for_demo_id= urfd.user_registration_for_demo_id " ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
  
    }
?>
