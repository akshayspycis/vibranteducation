

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
        class QuickQueryMgr{    

        //method to insert quick_query in database
        public function insQuickQuery(QuickQuery $quick_query) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO quick_query( "
                    . "quick_query_id, "
                    . "name, "
                    . "email, "
                    . "contact, "
                    . "subject, "
                    . "message, "
                    . "date) "
                    . "VALUES ('".$quick_query->getQuick_query_id()."',"
                    . "'".$quick_query->getName()."',"
                    . "'".$quick_query->getEmail()."',"
                    . "'".$quick_query->getContact()."',"
                    . "'".$quick_query->getSubject()."',"
                    . "'".$quick_query->getMessage()."',"
                    . "'".$quick_query->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

       
        
        //method to select QuickQuery from database
        public function selQuickQuery($course_id) {
            $dbh = new DatabaseHelper();
            $sql = "";
            if($course_id!=""){
                $sql = "select * from quick_query where course_id='".$course_id."'";
            }else{
                $sql = "select * from quick_query";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selUserQuery($contact_no,$email) {
            $dbh = new DatabaseHelper();
             $sql = "select * from quick_query qq where qq.email='".$email."' or qq.contact='".$contact_no."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
    
  //   method to update enquiry in database
  public function updateQuickQuery(QuickQuery $quick_query) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE quick_query SET " 
                    ."name='".$quick_query->getName()."',"
                    ."email='".$quick_query->getEmail()."',"
                    ."contact='".$quick_query->getContact()."',"
                    ."subject='".$quick_query->getSubject()."',"
                    ."message='".$quick_query->getMessage()."',"
                    ."date='".$quick_query->getDate()."',"
                    ."date='".$quick_query->getCourse_id()."'"
                    ."WHERE quick_query_id=".$quick_query->getQuick_query_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
           
             //method to delete Enquiry in database
        public function delQuickQuery($quick_query_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from quick_query where quick_query_id = '".$quick_query_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }  
    }
?>
