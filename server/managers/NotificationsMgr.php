<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class NotificationsMgr {    
        //method to insert notifications in database
        public function insNotifications(Notifications $notifications) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO notifications(heading, link) VALUES ('".$notifications->getHeading()."','".$notifications->getLink()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete notifications in database
        public function delNotifications($id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from notifications where id = '".$id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Notifications from database
        public function selNotifications() {
            $dbh = new DatabaseHelper();
            $sql = "select * from notifications ORDER BY date DESC";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update Notifications in database
        public function updateNotifications(Notifications $notifications) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE notifications SET " 
                    ."heading='".$notifications->getHeading()."',"
                    ."link='".$notifications->getLink()."'"
                    ."WHERE id=".$notifications->getId()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
         
    }
?>

