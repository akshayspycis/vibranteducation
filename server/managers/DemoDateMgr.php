
<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class DemoDateMgr{    
        public function insDemoDate(DemoDate $demo_date) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO demo_date( "
                    . "demo_class_id, "
                    . "date, "
                    . "status) "
                    . "VALUES ('".$demo_date->getDemo_class_id()."',"
                    . "'".$demo_date->getDate()."',"
                    . "'Enable')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delDemoDate($demo_date_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from demo_date where demo_date_id = '".$demo_date_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selDemoDate( $demo_class_id ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from demo_date b where demo_class_id='".$demo_class_id."'" ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selDemoDateClient( $demo_class_id ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from demo_date b where demo_class_id='".$demo_class_id."' and status='Enable'" ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function updDemoDatetatus(DemoDate $demo_date) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE demo_date SET " 
                    ."status='".$demo_date->getStatus()."'"
                   ." WHERE demo_date_id=".$demo_date->getDemo_date_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
         public function updateDemoDate(DemoDate $demo_date) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE demo_date SET " 
                    ."date='".$demo_date->getDate()."' "
                    ."WHERE demo_date_id=".$demo_date->getDemo_date_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
?>
