<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
        class BlogRplyDetailsMgr{    

        //method to insert blog_rply_details in database
        public function insBlogRplyDetails(BlogRplyDetails $blog_rply_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO blog_rply_details( "
                    . "blog_id, "
                    . "user_id, "
                    . "comment, "
                    . "receiver_id, "
                    . "status, "
                    . "date) "
                    . "VALUES ('".$blog_rply_details->getBlog_id()."',"
                    . "'".$blog_rply_details->getUser_id()."',"
                    . "'".$blog_rply_details->getComment()."',"
                    . "'".$blog_rply_details->getReceiver_id()."',"
                    . "'".$blog_rply_details->getStatus()."',"
                    . "'".$blog_rply_details->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delBlogRplyDetails($blog_rply_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from blog_rply_details where blog_rply_details_id = '".$blog_rply_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select BlogRplyDetails from database
        public function selBlogRplyDetails($blog_id) {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select user_name from user_details ud where ud.user_id=b.user_id) as user_name from blog_rply_details b where b.blog_id=".$blog_id." order by STR_TO_DATE(date, '%d-%m-%Y')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selBlogRplyDetailsClient($blog_rply_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from blog_rply_details where blog_rply_details_id=".$blog_rply_details_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateBlogRplyDetails(BlogRplyDetails $blog_rply_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE blog_rply_details SET " 
                    ."user_id='".$blog_rply_details->getUser_id()."',"
                    ."title='".$blog_rply_details->getTitle()."',"
                    ."short_description='".$blog_rply_details->getShort_description()."',"
                    ."long_description='".$blog_rply_details->getLong_description()."',"
                    ."date='".$blog_rply_details->getDate()."',"
                    ."category_id='".$blog_rply_details->getCategory_id()."',"
                    ."status='".$blog_rply_details->getStatus()."',"
                    ."pic='".$blog_rply_details->getPic()."',"
                    ."fb='".$blog_rply_details->getFb()."',"
                    ."tw='".$blog_rply_details->getTw()."'"
                    ."WHERE blog_rply_details_id=".$blog_rply_details->getBlog_detail_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updBlogRplyDetailstatus(BlogRplyDetails $blog_rply_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE blog_rply_details SET " 
                    ."status='".$blog_rply_details->getStatus()."'"
                   ." WHERE blog_rply_details_id=".$blog_rply_details->getBlog_rply_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
