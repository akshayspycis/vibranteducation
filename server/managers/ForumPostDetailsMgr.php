<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ForumPostDetailsMgr{    

        //method to insert forum_post_details in database
        public function insForumPostDetails(ForumPostDetails $forum_post_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO forum_post_details( "
                    . "forum_topic_details_id, "
                    . "forum_category_details_id, "
                    . "date, "
                    . "user_id, "
                    . "user_name, "
                    . "post,"
                    . "status) "
                    . "VALUES ('".$forum_post_details->getForum_topic_details_id()."',"
                    . "'".$forum_post_details->getForum_category_details_id()."',"
                    . "'".$forum_post_details->getDate()."',"
                    . "'".$forum_post_details->getUser_id()."',"
                    . "'".$forum_post_details->getUser_name()."',"
                    . "'".$forum_post_details->getPost()."',"
                    . "'".$forum_post_details->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delForumPostDetails($forum_post_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from forum_post_details where forum_post_details_id = '".$forum_post_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ForumPostDetails from database
        public function selForumPostDetails($forum_topic_details_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($forum_topic_details_id!=""){
                $sql = "select * from forum_post_details where forum_topic_details_id=".$forum_topic_details_id." order by STR_TO_DATE(date, '%d-%m-%Y')";
            }else{
                $sql = "select * from forum_post_details order by STR_TO_DATE(date, '%d-%m-%Y')";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selForumPostDetailsClient($forum_post_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from forum_post_details where forum_post_details_id=".$forum_post_details_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selForumPostDetailsInClient( $topic_url) {
            $dbh = new DatabaseHelper();
            $sql = "select * from forum_post_details b where b.status='Enable' and b.forum_topic_details_id=(select forum_topic_details_id from forum_topic_details where topic_url='".$topic_url."') order by STR_TO_DATE(date, '%d-%m-%Y')";

            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateForumPostDetails(ForumPostDetails $forum_post_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE forum_post_details SET " 
                    ."post='".$forum_post_details->getPost()."'"
                    ."WHERE forum_post_details_id=".$forum_post_details->getForum_post_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
          public function updForumPostDetailstatus(ForumPostDetails $forum_post_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE forum_post_details SET " 
                    ."status='".$forum_post_details->getStatus()."'"
                   ." WHERE forum_post_details_id=".$forum_post_details->getForum_post_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
