<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class ThoughtOfDayMgr{    

        //method to insert thoughtofday in database
        public function insThoughtOfDay(ThoughtOfDay $thoughtofday) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO thoughtofday(content, author, user_registration_for_demo_id, status, posteddate) VALUES ('".$thoughtofday->getContent()."','".$thoughtofday->getAuthor()."','".$thoughtofday->getUser_registration_for_demo_id()."','Disable','".$thoughtofday->getPosteddate()."')";
            
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delThoughtOfDay($thoughtofday_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from thoughtofday where thoughtofday_id = '".$thoughtofday_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ThoughtOfDay from database
        public function selThoughtOfDay() {
            $dbh = new DatabaseHelper();
            $sql = "select *
,(select user_name from user_registration_for_demo where user_registration_for_demo_id=t.user_registration_for_demo_id limit 1) as user_name
,(select pic_path from bear_witness where course_registration_id=(select course_registration_id from course_registration where user_registration_for_demo_id=t.user_registration_for_demo_id limit 1) and attestation='Photograph'limit 1) as pic_path from thoughtofday t";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function SelThoughtOfDayClient() {
            $dbh = new DatabaseHelper();
            $sql = "select *
                    ,(select user_name from user_registration_for_demo where user_registration_for_demo_id=t.user_registration_for_demo_id limit 1) as user_name
                    ,(select pic_path from bear_witness where course_registration_id=(select course_registration_id from course_registration where user_registration_for_demo_id=t.user_registration_for_demo_id limit 1) and attestation='Photograph'limit 1) as pic_path from thoughtofday t where t.status='Enable' order by thoughtofday_id desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
        public function updThoughtOfDay(ThoughtOfDay $thoughtofday) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE thoughtofday SET " 
                    ."status='".$thoughtofday->getStatus()."'"
                   ." WHERE thoughtofday_id=".$thoughtofday->getThoughtofday_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
    }
    
?>

