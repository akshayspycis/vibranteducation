<?php
    require_once '../dbhelper/DatabaseHelper.php';
    class ReferDetailsMgr{    
        public function insReferDetails(ReferDetails $refer_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO refer_details( "
                    . "user_id, "
                    . "user_name, "
                    . "email, "
                    . "contact_no, "
                    . "date) "
                    . "VALUES ('".$refer_details->getUser_id()."',"
                    . "'".$refer_details->getUser_name()."',"
                    . "'".$refer_details->getEmail()."',"
                    . "'".$refer_details->getContact_no()."',"
                    . "NOW())";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delReferDetails($refer_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from refer_details where refer_details_id = '".$refer_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selReferDetails( $user_id ) {
            $dbh = new DatabaseHelper();
            if($user_id==NULL){
                $sql = "select *,"
                        . "(select user_name from user_registration_for_demo where user_registration_for_demo_id=rd.user_id) as main_user_name"
                        . " from refer_details rd";
            }else{
                $sql = "select * from refer_details b where status='Enable' where demo_class_id=".$demo_class_id ;
            }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
  
    }
?>
