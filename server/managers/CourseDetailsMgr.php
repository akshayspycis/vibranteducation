<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class CourseDetailsMgr{    
        //method to insert course_details in database
        public function insCourseDetails(CourseDetails $course_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO course_details( "
                    . "title, "
                    . "long_description, "
                    . "status,"
                    . "pic) "
                    . "VALUES ('".$course_details->getTitle()."',"
                    . "'".$course_details->getLong_description()."',"
                    . "'".$course_details->getStatus()."',"
                    . "'".$course_details->getPic()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCourseDetails($course_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from course_details where course_details_id = '".$course_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select CourseDetails from database
        public function selCourseDetails() {
            $dbh = new DatabaseHelper();
            $sql = "select * from course_details order by course_details_id desc ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selCourseDetailsClient() {
            $dbh = new DatabaseHelper();
            $sql = "select course_details_id,title from course_details where status='Enable' order by course_details_id desc ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selCourseDetailsClientFull($course_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from course_details where course_details_id='$course_details_id' order by course_details_id desc ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  
          public function updCourseDetailstatus(CourseDetails $course_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_details SET " 
                    ."status='".$course_details->getStatus()."'"
                   ." WHERE course_details_id=".$course_details->getCourse_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
