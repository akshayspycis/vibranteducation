<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class QuestionOfDayMgr{    
        public function insQuestionOfDay(QuestionOfDay $question_of_day) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO question_of_day( "
                    . "question_of_day) "
                    . "VALUES ('".$question_of_day->getQuestion_of_day()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delQuestionOfDay($question_of_day_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from question_of_day where question_of_day_id = '".$question_of_day_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selQuestionOfDay( ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from question_of_day ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selQuestionOfDayClient( ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from question_of_day order by question_of_day_id desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function updateQuestionOfDay(QuestionOfDay $question_of_day) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE question_of_day SET " 
                    ."question_of_day='".$question_of_day->getQuestion_of_day()."' "
                    ."WHERE question_of_day_id=".$question_of_day->getQuestion_of_day_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
          
    }
?>
