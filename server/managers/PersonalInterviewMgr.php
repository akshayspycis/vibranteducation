<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class PersonalInterviewMgr {    
        //method to insert personalinterview in database
        public function insPersonalInterview(PersonalInterview $personalinterview){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO personalinterview("
                    . "exam_id, "
                    . "exam_sub_cat_id, "
                    . "exam_phase_id, "
                    . "content,"
                    . "year) "
                    . "VALUES ('".$personalinterview->getExam_id()."',"
                    . "'".$personalinterview->getExam_sub_cat_id()."',"
                    . "'".$personalinterview->getExam_phase_id()."',"
                    . "'".$personalinterview->getContent()."',"
                    . "'".$personalinterview->getYear()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            echo $sql;
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select PersonalInterview from database
        public function selPersonalInterview($personalinterview_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($personalinterview_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=pi.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=pi.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=pi.exam_phase_id) as exam_phase_name"
                    . " FROM personalinterview as pi";
           }
           else{
                $sql = "select * from personalinterview where personalinterview_id ='".$personalinterview_id."'";
           }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select PersonalInterview from database
        public function selPersonalInterview2($exam_id) {
            $dbh = new DatabaseHelper();
           
            $sql = "select * from personalinterview where exam_id ='".$exam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update PersonalInterview in database
        public function updatePersonalInterview(PersonalInterview $personalinterview){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE personalinterview SET " 
                    ."exam_sub_cat_name='".$personalinterview->getExam_sub_cat_name()."',"
                    ."exam_id='".$personalinterview->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$personalinterview->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete personalinterview in database
        public function delPersonalInterview($personalinterview_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  personalinterview where personalinterview_id = '".$personalinterview_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>




