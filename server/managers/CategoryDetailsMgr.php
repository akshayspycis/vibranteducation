<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class CategoryDetailsMgr{    
        //method to insert category_details in database
        public function insCategoryDetails(CategoryDetails $category_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO category_details( "
                    . "category, "
                    . "type) "
                    . "VALUES ('".$category_details->getCategory()."',"
                    . "'".$category_details->getType()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delCategoryDetails($category_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from category_details where category_details_id = '".$category_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to select CategoryDetails from database
        public function selCategoryDetails($type) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($type!=""){
                $sql = "select * from category_details where type='".$type."'";
            }else{
                $sql = "select * from category_details ";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateCategoryDetails(CategoryDetails $category_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE category_details SET " 
                    ."category='".$category_details->getCategory()."',"
                    ."type='".$category_details->getType()."'"
                    ."WHERE category_details_id=".$category_details->getCategory_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
?>
