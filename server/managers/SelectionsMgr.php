<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class SelectionsMgr{    

        //method to insert selections in database
        public function insSelections(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO selections(name, designation, orgname, image, date) VALUES ('".$selections->getName()."','".$selections->getDesignation()."','".$selections->getOrgname()."','".$selections->getImage()."','".$selections->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delSelections($id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from selections where id = '".$id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Selections from database
        public function selSelections() {
            $dbh = new DatabaseHelper();
            $sql = "select * from selections ORDER BY date DESC";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateSelections(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE selections SET " 
                    ."id='".$selections->getId()."',"
                    ."name='".$selections->getName()."',"
                    ."designation='".$selections->getDesignation()."',"
                    ."orgname='".$selections->getOrgname()."'"
                    ."WHERE id=".$selections->getId()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updSelectionsImg(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT selections.image FROM selections WHERE id=".$selections->getId()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['image'];
            }
            if(file_exists($file)&&unlink($file)){
                $sql ="UPDATE selections SET " 
                ."image='".$selections->getImage()."'"
                ."WHERE id=".$selections->getId()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>


