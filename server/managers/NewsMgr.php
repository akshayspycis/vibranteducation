<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class NewsMgr {    

        //method to insert news in database
        public function insNews(News $news) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO news(nheading, ncontent, ndate, nimage) VALUES ('".$news->getNheading()."','".$news->getNcontent()."','".$news->getNdate()."','".$news->getNimage()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delNews($nid) {
            $dbh = new DatabaseHelper();
            $sql = "delete from news where nid = '".$nid."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select News from database
        public function selNews() {
            $dbh = new DatabaseHelper();
            $sql = "select * from news ORDER BY nposted DESC";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update News in database
        public function updateNews(News $news) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE news SET " 
                    ."nid='".$news->getNid()."',"
                    ."nheading='".$news->getNheading()."',"
                    ."ndate='".$news->getNdate()."',"
                    ."ncontent='".$news->getNcontent()."'"
                    ."WHERE nid=".$news->getNid()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updNewsImg(News $news) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT news.nimage FROM news WHERE nid=".$news->getNid()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['nimage'];
            }
            if (unlink($file)){
                $sql ="UPDATE news SET " 
                ."nimage='".$news->getNimage()."'"
                ."WHERE nid=".$news->getNid()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>
