<?php
    require_once '../dbhelper/DatabaseHelper.php';
    class FileDetailsMgr{    
        public function insFileDetails(FileDetails $file_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO file_details( "
                    . "category_details_id, "
                    . "title, "
                    . "discription, "
                    . "path, "
                    . "status) "
                    . "VALUES ('".$file_details->getCategory_details_id()."',"
                    . "'".$file_details->getTitle()."',"
                    . "'".$file_details->getDiscription()."',"
                    . "'".$file_details->getPath()."',"
                    . "'Enable')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delFileDetails($file_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from file_details where file_details_id = '".$file_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selFileDetails($category_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select type from category_details where category_details_id=fd.category_details_id) as type from file_details fd where fd.category_details_id=$category_details_id";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selFileDetailsClient($type) {
            $dbh = new DatabaseHelper();
            $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='$type' and fd.status='Enable' order by file_details_id";
//            echo $sql;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
  
       public function updFileDetailstatus(FileDetails $file_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE file_details SET " 
                    ."status='".$file_details->getStatus()."'"
                   ." WHERE file_details_id=".$file_details->getFile_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
    }
?>
