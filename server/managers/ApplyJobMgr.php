<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ApplyJobMgr{    

        //method to insert apply_job in database
        public function insApplyJob(ApplyJob $apply_job) {
            
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO apply_job( "
                    . "career_id, "
                    . "name, "
                    . "email, "
                    . "contact_no, "
                    . "message, "
                    . "pdf,"
                    . "date) "
                    . "VALUES ('".$apply_job->getCareer_id()."',"
                    . "'".$apply_job->getName()."',"
                    . "'".$apply_job->getEmail()."',"
                    . "'".$apply_job->getContact_no()."',"
                    . "'".$apply_job->getMessage()."',"
                    . "'".$apply_job->getPdf()."',"
                    . "'".$apply_job->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function selApplyJob($career_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from apply_job where career_id='".$career_id."' order by STR_TO_DATE(date, '%d-%m-%Y')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
          
    }
?>
