<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class MainsExamMgr{    
        //method to insert mainsexam in database
       public function insMainsExam(MainsExam $mainsexam){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO mainsexam("
                    . "exam_id, "
                    . "exam_sub_cat_id, "
                    . "exam_phase_id, "
                    . "exam_section_id, "
                    . "no_of_questions, "
                    . "max_marks, "
                    . "duration, "
                    . "gen, "
                    . "obc, "
                    . "sc, "
                    . "st, "
                    . "ph, "
                    . "vh, "
                    . "year)"
                    . "VALUES ('".$mainsexam->getExam_id()."',"
                    . "'".$mainsexam->getExam_sub_cat_id()."',"
                    . "'".$mainsexam->getExam_phase_id()."',"
                    . "'".$mainsexam->getExam_section_id()."',"
                    . "'".$mainsexam->getNo_of_questions()."',"
                    . "'".$mainsexam->getMax_marks()."',"
                    . "'".$mainsexam->getDuration()."',"
                    . "'".$mainsexam->getGen()."',"
                    . "'".$mainsexam->getObc()."',"
                    . "'".$mainsexam->getSc()."',"
                    . "'".$mainsexam->getSt()."',"
                    . "'".$mainsexam->getPh()."',"
                    . "'".$mainsexam->getVh()."',"
                    . "'".$mainsexam->getYear()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
       //method to select MainsExam from database
        public function selMainsExam($mainsexam_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($mainsexam_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=me.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=me.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=me.exam_phase_id) as exam_phase_name"
                    . ",(select exam_section_name from examssections where exam_section_id=me.exam_section_id) as exam_section_name"
                    . " FROM mainsexam as me";
           }
           else{
                $sql = "select * from mainsexam where mainsexam_id ='".$mainsexam_id."'";
           }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
      
        //        method to update MainsExam in database
        public function updateMainsExam(MainsExam $mainsexam){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE mainsexam SET " 
                    ."exam_sub_cat_name='".$mainsexam->getExam_sub_cat_name()."',"
                    ."exam_id='".$mainsexam->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$mainsexam->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete mainsexam in database
        public function delMainsExam($mainsexam_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  mainsexam where mainsexam_id = '".$mainsexam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


