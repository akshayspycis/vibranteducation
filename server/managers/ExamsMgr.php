<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ExamsMgr {    
        //method to insert exams in database
        public function insExams(Exams $exams) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO exams(exam_name) VALUES ('".$exams->getExam_name()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select Exams from database
        public function selExams() {
            $dbh = new DatabaseHelper();
            $sql = "select * from exams";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selExamsCatSub() {
            $dbh = new DatabaseHelper();
            $sql = "SELECT e.exam_id,e.exam_name,es.exam_sub_cat_id,es.exam_sub_cat_name,(select exam_phase_name from examsphases where exam_id=e.exam_id and exam_sub_cat_id=es.exam_sub_cat_id order by exam_phase_id desc limit 1) as exam_phase_name FROM exams e LEFT JOIN examssubcategory es ON es.exam_id=e.exam_id ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        
        //        method to update Exams in database
        public function updateExams(Exams $exams) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE exams SET " 
                    ."exam_name='".$exams->getExam_name()."'"
                     ."WHERE exam_id=".$exams->getExam_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete exams in database
        public function delExams($exam_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from exams where exam_id = '".$exam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


