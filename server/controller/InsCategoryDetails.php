<?php
    include_once '../models/CategoryDetails.php'; 
    include_once '../managers/CategoryDetailsMgr.php'; 
    $category_details = new CategoryDetails();
    $category_details->setCategory($_POST["category"]);
    $category_details->setType($_POST["type"]);  
    $category_detailsMgr = new CategoryDetailsMgr();
    if ($category_detailsMgr->insCategoryDetails($category_details)) {
        echo 'Notice inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>
