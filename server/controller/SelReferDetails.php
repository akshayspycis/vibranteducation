<?php
     include_once '../models/ReferDetails.php';
     include_once '../managers/ReferDetailsMgr.php';
    $obj = new ReferDetailsMgr();
    $refer_details = $obj->selReferDetails($_POST['user_id']);
    $str = array();    
    while($row = $refer_details->fetch()){
            $arr = array(
            'refer_details_id' => $row['refer_details_id'], 
            'user_id' => $row['user_id'], 
            'main_user_name' => $row['main_user_name'],             
            'user_name' => $row['user_name'],             
            'email' => $row['email'],             
            'contact_no' => $row['contact_no'],             
            'date' => $row['date']             
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>