<?php
    include_once '../models/Banners.php';
    include_once '../managers/BannersMgr.php';
    $obj = new BannersMgr();
    $course_batch_details = $obj->selBanners();
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'banners_id' => $row['banners_id'], 
            'path' => $row['path'], 
            'content' => $row['content'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>