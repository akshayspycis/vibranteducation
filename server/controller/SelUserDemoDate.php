<?php
     include_once '../models/UserRegistrationForDemo.php';
     include_once '../managers/UserRegistrationForDemoMgr.php';
    $obj = new UserRegistrationForDemoMgr();
    $user_registration_for_demo = $obj->selUserRegistrationForDemoAll();
    $str = array();    
    while($row = $user_registration_for_demo->fetch()){
         $arr = array(
            'user_registration_for_demo_id' => $row['user_registration_for_demo_id'], 
            
            'user_name' => $row['user_name'], 
            'gender' => $row['gender'],             
            'email' => $row['email'],             
            'contact_no' => $row['contact_no'],             
            'demo_class' => $row['demo_class'],             
            'date' => $row['date'],             
            'registered_temp_code' => $row['registered_temp_code'],             
            'date_fo_regs' => $row['date_fo_regs'],             
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>