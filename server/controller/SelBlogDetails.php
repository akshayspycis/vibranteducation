<?php
     include_once '../models/BlogDetails.php';
    include_once '../managers/BlogDetailsMgr.php';
    $obj = new BlogDetailsMgr();
    $course_batch_details = $obj->selBlogDetails($_POST["category_id"]);
    $str = array();    
    while($row = $course_batch_details->fetch()){
            $arr = array(
            'blog_details_id' => $row['blog_details_id'], 
            'user_id' => $row['user_id'], 
            'title' => $row['title'], 
            'short_description' => $row['short_description'], 
            'long_description' => $row['long_description'], 
            'date' => $row['date'], 
            'category_id' => $row['category_id'], 
            'status' => $row['status'], 
            'pic' => $row['pic'],
            'fb' => $row['fb'],
            'tw' => $row['tw'],
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>