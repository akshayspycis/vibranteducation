 <?php
    include_once '../models/Notifications.php';
    include_once '../managers/NotificationsMgr.php';
    $notifications = new Notifications();    
    $notifications->setId($_POST["id"]);
    $notifications->setHeading($_POST["heading"]);
    $notifications->setLink($_POST["link"]);
    $notificationsMgr = new NotificationsMgr();    
    if($notificationsMgr->updateNotifications($notifications)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>