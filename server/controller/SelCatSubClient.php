<?php
    include_once '../models/Exams.php';
    include_once '../managers/ExamsMgr.php';
        $obj = new ExamsMgr();
        
        $exams = $obj->selExamsCatSub();
        $str = array();    
        while($row = $exams->fetch()){
            $arr = array(
                'exam_id' => $row['exam_id'], 
                'exam_sub_cat_id' => $row['exam_sub_cat_id'],
                'exam_sub_cat_name' => $row['exam_sub_cat_name'],
                'exam_name' => $row['exam_name'],
                'exam_phase_name' => $row['exam_phase_name'],
                );
            array_push($str, $arr); 
        }
        
    echo json_encode($str);
?>



