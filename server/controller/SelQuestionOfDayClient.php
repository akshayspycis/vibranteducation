<?php
     include_once '../models/QuestionOfDay.php';
    include_once '../managers/QuestionOfDayMgr.php';
    $obj = new QuestionOfDayMgr();
    $question_of_day = $obj->selQuestionOfDayClient();
    $str = array();    
    while($row = $question_of_day->fetch()){
            $arr = array(
            'question_of_day_id' => $row['question_of_day_id'], 
            'question_of_day' => $row['question_of_day']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>