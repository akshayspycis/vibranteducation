<?php
    include_once '../models/FileDetails.php';
    include_once '../managers/FileDetailsMgr.php';
    $obj = new FileDetailsMgr();
    $file_details = $obj->selFileDetailsClient($_POST['type']);
    $str = array();    
    while($row = $file_details->fetch()){
        $arr = array(
            'file_details_id' => $row['file_details_id'], 
            'category_details_id' => $row['category_details_id'], 
            'title' => $row['title'],             
            'discription' => $row['discription'],             
            'path' => $row['path'],             
            'type' => $row['type'],             
            'status' => $row['status']            
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>