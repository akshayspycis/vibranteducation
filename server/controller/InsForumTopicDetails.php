<?php
    include_once '../models/ForumTopicDetails.php'; 
    include_once '../managers/ForumTopicDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
        $forum_topic_details = new ForumTopicDetails();
    $forum_topic_details->setForum_category_details_id($_POST["forum_category_details_id"]);
    $forum_topic_details->setTopic_name($_POST["topic_name"]);
    $forum_topic_details->setDate($date->format('D, d M Y'));   
    $forum_topic_details->setTopic_url(str_replace(' ', '-', strtolower($_POST["topic_name"])));
    $forum_topic_detailsMgr = new ForumTopicDetailsMgr();
    if ($forum_topic_detailsMgr->insForumTopicDetails($forum_topic_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>