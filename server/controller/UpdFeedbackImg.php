<?php
    include_once '../models/Feedback.php';
    include_once '../managers/FeedbackMgr.php';
    $feedback = new Feedback(); 
    $feedback->setFeedback_id($_POST['feedback_id']);
    $upload_dir = "upload/";
    $img = $_POST['image'];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $feedback->setImage($file);
    $feedbackMgr = new FeedbackMgr(); 
    if ($feedbackMgr->updFeedbackImg($feedback)) {
     echo 'true';
    } else {
        echo 'Manager Error Found';        
    }
?>