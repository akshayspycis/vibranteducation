<?php
    session_start();                
    include_once '../models/UserDetails.php'; 
    include_once '../models/LoginDetails.php'; 
    include_once '../managers/UserLoginMgr.php'; 
    $user_details = new UserDetails();
    $user_details->setEmail($_POST["forgotemail"]);  
    $userloginMgr = new UserLoginMgr();
    $user_email = $user_details->getEmail();
    $userlogin = $userloginMgr->forgotPassword($user_email);
    if($row = $userlogin->fetch()){
           echo sendEmail($user_email, $row['password']); 
    } else {
          $userlogin = $userloginMgr->forgotPasswordOther($user_email);
          if($row = $userlogin->fetch()){
               echo sendEmail($user_email, $row['password']); 
          }else {
               echo 'Error';
          }
    }
function sendEmail($email,$password) {
    include("email/Email.php");
    $subject="VIBRANT EDUCATION SERVICES PASSWORD ASSISTANCE";
    $msg='<table border="0" cellspacing="0"width="100%" style="background:#e9ebee;padding:30px;"> 
                          <tr>
                              <td colspan="2" ><p style="font-size:18px;margin-top:20px;font-family:arial;line-height:25px;text-align:center">Vibrant Career</p></td>
                          </tr>
                          <tr>
                              <td width="50%">
                                  <h4 style="font-family:arial;font-weight:bold">VIBRANT EDUCATION SERVICES</h4>
                                  <p style="font-family:arial">164,I & II Floor, Samanvay Nagar, <br>Awadhpuri, Bhopal<br> contact <b>0755-4047934</b>, <b>98-262-262-99</b><br>
                                      website <a href="http://www.vibrantcareer.com/"><b>www.vibrantcareer.com</b></a> </p>
                              </td>
                              <td style="float:right"><img class="img-responsive" src="http://infoparkinnovations.in/vibr.png" width="300" /></td>
                          </tr>
                          <tr>
                              <td colspan="2" ><h1 style="font-family:arial;text-align:center;font-weight:bold;margin-top:40px;">We received a request to forgot password.</h1></td>
                          </tr>
                           <tr>
                            <td colspan="2" >
                            We received a request to forget password associated with this e-mail address. If you made this request, 
                                <br>
                                Your password is :<b>'.$password.'</b> . 
                             </td>
                           </tr>
                      </table>';
    return sendMail($email, $subject, $msg);
}
?>