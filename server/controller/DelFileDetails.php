<?php
    include_once '../managers/FileDetailsMgr.php';
    $file_details_id = $_POST["file_details_id"];
    $file_details_mgr = new FileDetailsMgr();
    if ($file_details_mgr->delFileDetails($file_details_id)) {
        if($_POST['type']!='Youtube'){
            unlink($_POST['path']);
        }
        echo 'File Deleted Successfully.';
    } else {
        echo 'Error';
    }    
?>