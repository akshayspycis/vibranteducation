<?php
    include_once '../models/BlogRplyDetails.php'; 
    include_once '../managers/BlogRplyDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $blog_rply_details = new BlogRplyDetails();
    $blog_rply_details->setBlog_id($_POST["blog_id"]);
    $blog_rply_details->setUser_id($_POST["user_id"]); 
    $blog_rply_details->setComment($_POST["comment"]);
    $blog_rply_details->setStatus("Disable");
    $blog_rply_details->setDate($date->format('d M Y'));
    $blog_rply_detailsMgr = new BlogRplyDetailsMgr();
    if ($blog_rply_detailsMgr->insBlogRplyDetails($blog_rply_details)) {
        echo 'BlogRplyDetails inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>

