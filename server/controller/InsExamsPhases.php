<?php
    include_once '../models/ExamsPhases.php'; 
    include_once '../managers/ExamsPhasesMgr.php'; 
    if(isset($_FILES["FileInput"])){
	$UploadDirectory	= 'media/'; //specify upload directory ends with / (slash)
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	if ($_FILES["FileInput"]["size"] > 52428800) {
		die("File size is too big!");
	}
        $UploadDirectory=$UploadDirectory.'PDF'.'/';
        $examsphases = new ExamsPhases();
        $examsphases->setExam_id($_POST["selExamName"]);
        $examsphases->setExam_sub_cat_id($_POST["selExamSubCat"]);
	switch(strtolower($_FILES['FileInput']['type'])){
    			case 'application/pdf':
				break;
			default:
				die('Unsupported File!'); //output error
	}
	$File_Name          = strtolower($_FILES['FileInput']['name']);
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); //get file extention
	$Random_Number      = rand(0, 9999999999); //Random number to be added to name.
	$NewFileName 		= $Random_Number.$File_Ext; //new file name
            if(move_uploaded_file($_FILES['FileInput']['tmp_name'], $UploadDirectory.$NewFileName )){
                $examsphases->setExam_phase_name($UploadDirectory.$NewFileName);
                $examsphasesMgr = new ExamsPhasesMgr();
                if ($examsphasesMgr->insExamsPhases($examsphases)) {
                    die('FileDetails inserted Successfully.');
                } else {
                    die('Server Error');
                }
            }else{
                die('error uploading File!');
            }
}else{
	die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
}
?>