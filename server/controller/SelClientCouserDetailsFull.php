<?php
     include_once '../models/CourseDetails.php';
    include_once '../managers/CourseDetailsMgr.php';
    $obj = new CourseDetailsMgr();
    $course_batch_details = $obj->selCourseDetailsClientFull($_POST['course_details_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
            $arr = array(
            'course_details_id' => $row['course_details_id'], 
            'title' => $row['title'], 
            'long_description' => $row['long_description'], 
            'status' => $row['status'], 
            'pic' => $row['pic'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>