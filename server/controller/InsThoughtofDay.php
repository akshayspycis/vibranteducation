<?php
    session_start();
    include_once '../models/ThoughtOfDay.php'; 
    include_once '../managers/ThoughtOfDayMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $thoughtofday = new ThoughtOfDay();
    $thoughtofday->setContent($_POST["content"]);
    $thoughtofday->setAuthor($_POST["author"]);  
    $thoughtofday->setUser_registration_for_demo_id($_SESSION['user_id']);   
    $thoughtofday->setPosteddate($date->format('D, d M Y'));   
    $thoughtofdayMgr = new ThoughtOfDayMgr();
    if ($thoughtofdayMgr->insThoughtOfDay($thoughtofday)) {
        echo 'ThoughtOfDay inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>