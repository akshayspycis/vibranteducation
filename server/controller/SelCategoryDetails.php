    <?php
    include_once '../models/CategoryDetails.php';
    include_once '../managers/CategoryDetailsMgr.php';
    $obj = new CategoryDetailsMgr();
    $category_details = $obj->selCategoryDetails($_POST['type']);
    $str = array();    
    while($row = $category_details->fetch()){
    $arr = array(
        'category_details_id' => $row['category_details_id'], 
        'category' => $row['category'],             
        'type' => $row['type'],             
    );
        array_push($str, $arr); 
    }
    echo json_encode($str);
    ?>