<?php
    include_once '../models/ApplyJob.php';
    include_once '../managers/ApplyJobMgr.php';
    $obj = new ApplyJobMgr();
    $apply_job = $obj->selApplyJob($_POST['career_id']);
    $str = array();    
    while($row = $apply_job->fetch()){
        $arr = array(
            'name' => $row['name'], 
            'email' => $row['email'], 
            'contact_no' => $row['contact_no'], 
            'message' => $row['message'], 
            'pdf' => $row['pdf'], 
            'date' => $row['date'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>