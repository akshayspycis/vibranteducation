<?php
    include_once '../models/CourseDetails.php'; 
    include_once '../managers/CourseDetailsMgr.php'; 
    $course_batch_details = new CourseDetails();
    $course_batch_details->setTitle($_POST["title"]);  
    $course_batch_details->setLong_description($_POST["long_description"]);   
    $course_batch_details->setStatus($_POST["status"]);   
    $upload_dir = "upload/";
    $img = $_POST["nimage"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $course_batch_details->setPic($file);   
    $course_batch_detailsMgr = new CourseDetailsMgr();
    if ($course_batch_detailsMgr->insCourseDetails($course_batch_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>