<?php
    include_once '../models/DemoDate.php';
    include_once '../managers/DemoDateMgr.php';
    $obj = new DemoDateMgr();
    $demo_class = $obj->selDemoDateClient($_POST['demo_class_id']);
    $str = array();    
    while($row = $demo_class->fetch()){
            $arr = array(
            'demo_date_id' => $row['demo_date_id'], 
            'date' => $row['date'], 
            'status' => $row['status']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>