<?php

    include_once '../models/FileDetails.php'; 
    include_once '../managers/FileDetailsMgr.php'; 
    $type=$_POST["type"];
    if(isset($_FILES["FileInput"])){
	############ Edit settings ##############
	$UploadDirectory	= 'media/'; //specify upload directory ends with / (slash)
	##########################################
	/*
	Note : You will run into errors or blank page if "memory_limit" or "upload_max_filesize" is set to low in "php.ini". 
	Open "php.ini" file, and search for "memory_limit" or "upload_max_filesize" limit 
	and set them adequately, also check "post_max_size".
	*/
	//check if this is an ajax request
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	//Is file size is less than allowed size.
	if ($_FILES["FileInput"]["size"] > 52428800) {
		die("File size is too big!");
	}
	$file_details = new FileDetails();
        $UploadDirectory=$UploadDirectory.$type.'/';
        $file_details->setCategory_details_id($_POST["category_details_id"]);
        $file_details->setTitle($_POST["title"]);  
        $file_details->setDiscription($_POST["discription"]);  
	//allowed file type Server side check
        
	switch(strtolower($_FILES['FileInput']['type'])){
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html': //html file
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
			case 'audio/mp3':
				break;
			default:
				die('Unsupported File!'); //output error
	}
	$File_Name          = strtolower($_FILES['FileInput']['name']);
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); //get file extention
	$Random_Number      = rand(0, 9999999999); //Random number to be added to name.
	$NewFileName 		= $Random_Number.$File_Ext; //new file name
        if($type=="Youtube"){
                $file_details->setPath($_POST['you_tube_path']);   
                $file_detailsMgr = new FileDetailsMgr();
                if ($file_detailsMgr->insFileDetails($file_details)) {
                    die('FileDetails inserted Successfully.');
                } else {
                    die('Server Error');
                }
        }else{
            if(move_uploaded_file($_FILES['FileInput']['tmp_name'], $UploadDirectory.$NewFileName )){
                $file_details->setPath($UploadDirectory.$NewFileName);   
                $file_detailsMgr = new FileDetailsMgr();
                if ($file_detailsMgr->insFileDetails($file_details)) {
                    die('FileDetails inserted Successfully.');
                } else {
                    die('Server Error');
                }
            }else{
                die('error uploading File!');
            }
        }
	
}else if($type=="Youtube"){
        $file_details = new FileDetails();
        $file_details->setCategory_details_id($_POST["category_details_id"]);
        $file_details->setTitle($_POST["title"]);  
        $file_details->setDiscription($_POST["discription"]);  
        $file_details->setPath($_POST['you_tube_path']);   
                $file_detailsMgr = new FileDetailsMgr();
                if ($file_detailsMgr->insFileDetails($file_details)) {
                    die('FileDetails inserted Successfully.');
                } else {
                    die('Server Error');
                }
}else{
	die('Something wrong with upload! Is "upload_max_filesize" set correctly?');
}
    
?>