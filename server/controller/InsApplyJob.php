<?php
    include_once '../models/ApplyJob.php'; 
    include_once '../managers/ApplyJobMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $apply_job = new ApplyJob();
    $apply_job->setCareer_id($_POST["career_id"]);
    $apply_job->setName($_POST["name"]);  
    $apply_job->setEmail($_POST["email"]);   
    $apply_job->setContact_no($_POST["contact_no"]);   
    $apply_job->setMessage($_POST["message"]);   
    $apply_job->setDate($date->format('d-m-Y'));   
    $upload_dir = "pdf/";
    $file = $upload_dir.time().".pdf";
    if(!empty($_POST['pdf'])){
        $pdf = $_POST["pdf"];
        $pdf = str_replace('data:application/pdf;base64,', '', $pdf);
        $pdf = str_replace(' ', '+', $pdf);
        $data = base64_decode($pdf);
        file_put_contents( $file, $data );
        } else {
            echo "No Data Sent";
        }
    $apply_job->setPdf($file);   
    $apply_jobMgr = new ApplyJobMgr();
    if ($apply_jobMgr->insApplyJob($apply_job)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
    exit();
?>