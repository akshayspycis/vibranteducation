    <?php
    include_once '../models/Selections.php';
    include_once '../managers/SelectionsMgr.php';
    $obj = new SelectionsMgr();
        
    $selections = $obj->selSelections();
    $str = array();    
    while($row = $selections->fetch()){
    $arr = array(
    'id' => $row['id'], 
    'name' => $row['name'],             
    'designation' => $row['designation'],             
    'orgname' => $row['orgname'],             
    'image' => $row['image'],             
    'date' => $row['date'],             
    
    );
    array_push($str, $arr); 
    }
        
    echo json_encode($str);
    ?>