<?php
    include_once '../models/ExamsSections.php'; 
    include_once '../managers/ExamsSectionsMgr.php'; 
    $examssections = new ExamsSections();
    $examssections->setExam_section_name($_POST["exam_section_name"]);
    $examssections->setExam_phase_id($_POST["selExamPhase"]);
    $examssections->setExam_sub_cat_id($_POST["selExamSubCat"]);
    $examssections->setExam_id($_POST["selExamName"]);
    $examssectionsMgr = new ExamsSectionsMgr();
    if ($examssectionsMgr->insExamsSections($examssections)) {
        echo 'Exams Section inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>


