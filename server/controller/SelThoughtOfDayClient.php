<?php
     include_once '../models/ThoughtOfDay.php';
    include_once '../managers/ThoughtOfDayMgr.php';
    $obj = new ThoughtOfDayMgr();
    $thought_of_day = $obj->SelThoughtOfDayClient();
    $str = array();    
    while($row = $thought_of_day->fetch()){
       $arr = array(
            'thoughtofday_id' => $row['thoughtofday_id'], 
            'content' => $row['content'], 
            'author' => $row['author'], 
            'user_name' => $row['user_name'], 
            'pic_path' => $row['pic_path'], 
            'posteddate' => $row['posteddate'], 
            'status' => $row['status'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>