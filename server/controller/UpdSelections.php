 <?php
    include_once '../models/Selections.php';
    include_once '../managers/SelectionsMgr.php';
    $selections = new Selections();    
    $selections->setId($_POST["id"]);
    $selections->setName($_POST["name"]);
    $selections->setDesignation($_POST["designation"]);  
    $selections->setOrgname($_POST["orgname"]); 
    $selectionsMgr = new SelectionsMgr();    
    if ($selectionsMgr->updateSelections($selections)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>