<?php include 'includes/session.php'; ?>    
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Vibrant | Exam Pattern Analysis</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?php
            $exam_sub_cat_id;
            $exam_id;
            if( isset ( $_GET["exam_id"]) && isset ($_GET["exam_sub_cat_id"]) ){
                $exam_id=$_GET["exam_id"];
                $exam_sub_cat_id=$_GET["exam_sub_cat_id"];
            }  else {?>
            <script>
                window.location="index.php;
            </script>    
            <?php } 
            ?>
             <?php include 'includes/csslinks.php';?>
            <style>
                #preExamTable tbody tr td{
                    font-weight:bold;
                    text-align:center;
                }
                #preExamTable thead tr th{
                    font-weight:bold;
                    text-align:center;
                    background:#1da913;;
                }
                #thead{
                    
                    background:#1da913;;
                }
                #examAnalysis-head{
                    position:relative;top:-16px;
                    font-weight:bold;
                    text-align:center;
                }
                #tfoot{
                    background:#1da913;
                    color:white;
                    font-weight:bold;
                    text-align:center;
                }
                #DesExamTable tr th{
                    font-weight:bold;
                    text-align:center;
                }
                #DesExamTable tr td{
                    font-weight:bold;
                    text-align:center;
                }
            </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('exam-pattern')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
                            
                        <div class="banner dark-translucent-bg" style="background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
			<div id="page-start"></div>
                       
			<!-- section start -->
			<!-- ================ -->
                           <div class="clearfix"></div>
			<!-- section end -->
                        <!-- section start -->
			<!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" >
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                   <?php
                        include_once './includes/db.php';
                        $sql_query="SELECT *,(select exam_name from exams where exam_id=ep.exam_id) as exam_name,(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=ep.exam_sub_cat_id) as exam_sub_cat_name FROM examsphases ep where ep.exam_id=".$exam_id." and ep.exam_sub_cat_id=".$exam_sub_cat_id;
                        $result= mysqli_query($conn,$sql_query);
                        $index=0;
                        while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ 
                            if($index==0){
                            ?>
                                <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong><?php echo $row['exam_name']; ?></strong> <?php echo  $row['exam_sub_cat_name'];?></h1>
                                <div class="separator"></div>
                                  <div class="clearfix"></div>
                            <?php } 
                            $exam_phase_name=$row['exam_phase_name'];
                            ?>
                                  <p>&nbsp;</p>
                            <h3 id="heading-font" class="text-center"><?php echo  $row['exam_phase_name'];?></h3>
                            <div class="separator"></div>
                            <?php 
                                if($exam_phase_name=="Preliminary Exams"){
                                     $sql_query= "SELECT *"
                                                . ",(select exam_section_name from examssections where exam_section_id=pe.exam_section_id) as exam_section_name"
                                                . " FROM prelimsexam as pe where pe.exam_id=".$exam_id." and pe.exam_sub_cat_id=".$exam_sub_cat_id;
                                     $result1= mysqli_query($conn,$sql_query);
                                     $counter=1;
                                     $no_of_qus=0; 
                                     $max=0;
                                     $Min=0;
                                     ?>
                                                   
                                                     <div class="table-responsive">
                                                         <table class="table table-bordered table-striped table-colored table-hover" id="preExamTable">
                                                             <thead id="thead">
                                                                 <tr>
                                                                     
                                                                     <th rowspan="2"><span id="examAnalysis-head">S.no.</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">Sections</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">No. of Questions</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head" >Max. Marks</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">Duration in Min.</span></th>
                                                                     <th colspan="6">Category Wise Cut Off</th>
                                                                     <th rowspan="2"><b style="color:yellow">Year</b></th>
                                                                                    
                                                                 </tr>
                                                                 <tr>
                                                                    <th>GEN</th>
                                                                    <th>OBC</th>
                                                                    <th>SC</th>
                                                                    <th>ST</th>
                                                                    <th>PH</th>
                                                                    <th>VH</th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                                 
                                     <?php while($row = mysqli_fetch_array($result1, MYSQLI_ASSOC)){ ?>
                                                       <tr>
                                                           <td><?php echo $counter;?></td>
                                                                     <td><?php echo $row['exam_section_name'];?></td>
                                                                     <td><?php echo $row['no_of_questions'];?></td>
                                                                     <td><?php echo $row['max_marks'];?></td>
                                                                     <td >Composite Duration of <?php echo $row['duration'];?> Min.</td>
                                                                     <td><?php echo $row['gen'];?></td>
                                                                     <td><?php echo $row['obc'];?></td>
                                                                     <td><?php echo $row['sc'];?></td>
                                                                     <td><?php echo $row['st'];?></td>
                                                                     <td><?php echo $row['ph'];?></td>
                                                                     <td><?php echo $row['vh'];?></td>
                                                                     <td><?php echo $row['year'];?></td>
                                                                 </tr>          
                            
                                <?php 
                                        $no_of_qus=$no_of_qus+$row['no_of_questions'];
                                        $max=$max+$row['max_marks'];
                                        $Min=$Min+$row['duration'];
                                        $counter++;
                                     } 
                                     ?>
                             </tbody>
                                                             <tfoot id="tfoot">
                                                             <td></td>
                                                             <td>Total</td>
                                                             <td><?php echo $no_of_qus; ?></td>
                                                             <td><?php echo $max; ?></td>
                                                             <td><?php echo $Min;?> Min.</td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             </tfoot>
                                                         </table> 
                                                     </div>
                                              
                   <?php
                   $index++;
                        }
                                if($exam_phase_name=="Main Exams"){
                                     $sql_query= "SELECT * ,(select exam_section_name from examssections where exam_section_id=me.exam_section_id) as exam_section_name "
                                                    . " FROM mainsexam as me where me.exam_id=".$exam_id." and me.exam_sub_cat_id=".$exam_sub_cat_id;
                                     $result2= mysqli_query($conn,$sql_query);
                                     $counter=1;
                                     $no_of_qus=0; 
                                     $max=0;
                                     $Min=0;
                                     ?>
                                                     <div class="table-responsive">
                                                         <table class="table table-bordered table-striped table-colored table-hover" id="preExamTable">
                                                             <thead id="thead">
                                                                 <tr>
                                                                     
                                                                     <th rowspan="2"><span id="examAnalysis-head">S.no.</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">Sections</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">No. of Questions</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head" >Max. Marks</span></th>
                                                                     <th rowspan="2"><span id="examAnalysis-head">Duration in Min.</span></th>
                                                                     <th colspan="6">Category Wise Cut Off</th>
                                                                     <th rowspan="2"><b style="color:yellow">Year</b></th>
                                                                                    
                                                                 </tr>
                                                                 <tr>
                                                                    <th>GEN</th>
                                                                    <th>OBC</th>
                                                                    <th>SC</th>
                                                                    <th>ST</th>
                                                                    <th>PH</th>
                                                                    <th>VH</th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                                 
                                     <?php while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)){ ?>
                                                       <tr>
                                                           <td><?php echo $counter;?></td>
                                                                     <td><?php echo $row['exam_section_name'];?></td>
                                                                     <td><?php echo $row['no_of_questions'];?></td>
                                                                     <td><?php echo $row['max_marks'];?></td>
                                                                     <td ><?php echo $row['duration'];?> Min.</td>
                                                                     <td><?php echo $row['gen'];?></td>
                                                                     <td><?php echo $row['obc'];?></td>
                                                                     <td><?php echo $row['sc'];?></td>
                                                                     <td><?php echo $row['st'];?></td>
                                                                     <td><?php echo $row['ph'];?></td>
                                                                     <td><?php echo $row['vh'];?></td>
                                                                     <td><?php echo $row['year'];?></td>
                                                                 </tr>          
                            
                                <?php 
                                        $no_of_qus=$no_of_qus+$row['no_of_questions'];
                                        $max=$max+$row['max_marks'];
                                        $Min=$Min+$row['duration'];
                                        $counter++;
                                     } 
                                     ?>
                             </tbody>
                                                             <tfoot id="tfoot">
                                                             <td></td>
                                                             <td>Total</td>
                                                             <td><?php echo $no_of_qus; ?></td>
                                                             <td><?php echo $max; ?></td>
                                                             <td><?php echo $Min;?> Min.</td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             <td></td>
                                                             </tfoot>
                                                         </table> 
                                                   
                                                </div>
                   <?php
                   $index++;
                        }
                                if($exam_phase_name=="Discriptive Type Test"){
                                     $sql_query= "SELECT * FROM discriptivetest as dt where dt.exam_id=".$exam_id." and dt.exam_sub_cat_id=".$exam_sub_cat_id;
                                     $result2= mysqli_query($conn,$sql_query);
                                     $counter=1;
                                     ?>
                                                     <div class="table-responsive">
                                                         <table class="table table-bordered table-striped table-colored table-hover" id="preExamTable">
                                                     <thead id="thead">
                                                                 <tr>
                                                                     <th>S.no.</th>
                                                                     <th>Name of  Paper</th>
                                                                     <th>Type of  Paper</th>
                                                                     <th>Duration in Min.</th>
                                                                     <th>Max. Marks</th>
    <th ><b style="color:yellow">Year</b></th>
                                                                 </tr>
                                                                
                                                             </thead>
                                                             <tbody>
                                     <?php while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)){ ?>
                                                       <tr>
                                                           <td><?php echo $counter;?></td>
                                                                     <td><?php echo $row['papername'];?></td>
                                                                     <td><?php echo $row['papertype'];?></td>
                                                                     <td><?php echo $row['duration'];?></td>
                                                                     <td><?php echo $row['maxmarks'];?></td>
                                                                     <td><?php echo $row['year'];?></td>
                                                                 </tr>          
                            
                                <?php 
                                    
                                     } 
                                     ?>
                             </tbody>
                                                         </table> 
                                                     </div>
                                                
                   <?php
                   $index++;
                        }
                                if($exam_phase_name=="Group Discussion"){
                                     $sql_query= "SELECT * FROM groupdiscussion as gd where gd.exam_id=".$exam_id." and gd.exam_sub_cat_id=".$exam_sub_cat_id;
                                     $result2= mysqli_query($conn,$sql_query);
                                     ?>
                                     <?php while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)){ ?>
                                        <div class="col-md-12" style="background:white;padding-top:15px;border-left: 4px solid #1da913;box-shadow:0 2px 10px rgba(0,0,0,0.2);" >
                                                      <p id="heading-font" class="text-justify" style="font-weight:bold;"><?php echo $row['content'];?></p>
					</div>
                                                                     
                               <div class="clearfix"></div>
                                <?php 
                                    
                                     } 
                                     ?>

                   <?php
                   $index++;
                        }
                                if($exam_phase_name=="Personal Interview"){
                                     $sql_query= "SELECT * FROM personalinterview as gd where gd.exam_id=".$exam_id." and gd.exam_sub_cat_id=".$exam_sub_cat_id;
                                     $result2= mysqli_query($conn,$sql_query);
                                     ?>
                                     <?php while($row = mysqli_fetch_array($result2, MYSQLI_ASSOC)){ ?>
                                        <div class="col-md-12" style="background:white;padding-top:15px;border-left: 4px solid #1da913;box-shadow:0 2px 10px rgba(0,0,0,0.2);" >
                                           <p id="heading-font" class="text-justify" style="font-weight:bold;"><?php echo $row['content'];?></p>
					</div>
                                                                     
                            
                                <?php 
                                    
                                     } 
                                     ?>

                   <?php
                   $index++;
                        }
                        
                        }
                   ?>
                                            
					</div>
                                   
                                 </div>
                        </section>
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
	</body>
</html>
