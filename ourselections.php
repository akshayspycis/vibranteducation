<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Our Selections </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
            <style>
.profile-card {
 
  margin: 80px auto 80px;
  background:#fff;
  padding: 0 20px 20px;
  border-radius: 2px;
  -webkit-border-radius: 2px;
  -moz-border-radius: 2px;
  box-shadow: 0 2px 8px rgba(0,0,0,0.75);
  -webkit-box-shadow: 0 2px 8px rgba(0,0,0,0.75);
  -moz-box-shadow: 0 2px 8px rgba(0,0,0,0.75);
  text-align: center;
  color:white;
}
.profile-card:hover .avatar-flip {
   
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
}
.profile-card:hover .avatar-flip img:first-child {
  opacity: 0;
}
.profile-card:hover .avatar-flip img:last-child {
  opacity: 1;
}
.avatar-flip {
  background: rgba(255,255,255,.2);  
  border-radius: 100px;
  overflow: hidden;
  height: 100px;
  width: 100px;
  position: relative;
  margin: auto;
  top: -60px;
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  box-shadow: 0 0 0 8px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 0 0 8px rgba(0,0,0,.3);
  -moz-box-shadow: 0 0 0 8px rgba(0,0,0,.3);
}
.avatar-flip img {
  position: absolute;
  z-index:-1;
  left: 0;
  top: 0;
  border-radius: 100px;
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  
  
}
.avatar-flip img:first-child {
  z-index: -1;
}
.avatar-flip img:last-child {
  z-index: 0;
  transform: rotateY(180deg);
  -webkit-transform: rotateY(180deg);
  opacity: 0;
}
.profile-card h3{
     font-family:"Gotham Rounded SSm A", "Gotham Rounded SSm B", Helvetica;
     position: relative;
     top:-30px;
     color:#0d5995;
     font-weight:bold;
     text-shadow: 0 0px 1px rgba(0,0,0,.2);
}
.profile-card h5{
     font-family:"Gotham Rounded SSm A", "Gotham Rounded SSm B", Helvetica;
     position: relative;
     top:-20px;
     color:#0d5995;
     
     
}
.profile-card p{
     font-family:"Gotham Rounded SSm A", "Gotham Rounded SSm B", Helvetica;
     position: relative;
     top:-17px;
     color:#0d5995;
     font-weight:bold;
     
     
}
    
</style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
 
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					
					<div class="container">
						
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;">Our <strong>Selections</strong> </h1>
                                                    <div class="separator"></div>
                                                  <div class="row" id="selOurSelection">
                                                      
                                                      


					</div>
				</div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelOurSelections.js"></script>
                <script type="text/javascript">
                    $(document).ready(function() {
                        SelOurSelections();
                    });
                   
                </script>
	</body>
</html>
