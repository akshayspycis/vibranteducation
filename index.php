<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Vibrant Education Services</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <?php include 'includes/csslinks.php';?>
            <style> 
                .owl-carousel .owl-item {
                   margin-left: 5;
                }
            </style>
        </head>
       <!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('home')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
                                <div class="banner clearfix">
                                    
                                    <!-- slideshow start -->
                                    <!-- ================ -->
                                    <div class="slideshow">
                                    
                                        <!-- slider revolution start -->
                                        <!-- ================ -->
                                        
                                        <div class="slider-banner-container" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
                                            <div class="slider-banner-fullwidth-big-height">
                                                <ul class="slides">
                                                    <!-- slide 1 start -->
                                                    <!-- ================ -->
                                                    <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from banners order by banners_id desc";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    $i=0;
                                                                    while($row = $course_batch_details->fetch()){
                                                                                $class="item";
                                                                                if($i==0){
                                                                                    $class=$class." active";
                                                                                }
                                                                                
                                                                        ?>
                                                    <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Vibrant Education">
        <img src="server/controller/<?php echo $row['path'];?>" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
             <div class="tp-caption sfb fadeout large_dark"
                                                             data-x="left"
                                                             data-y="220"
                                                             data-speed="500"
                                                             data-start="1000"
                                                             data-easing="easeOutQuad">
                 <span id="newFont2" style="color: white;"><?php echo $row['content'];?></span>
                 </div>
      </li>
                                                                <?php    $i++;}
                                                                ?>
                                    
                                                        <!-- Transparent Background -->
                                                        <!-- LAYER NR. 1 -->
                                                        
                                                        <!-- LAYER NR. 2 -->
                                                        <!-- LAYER NR. 3 -->
                                                    
                                                    <!-- slide 1 end -->
                                                    <!-- slide 2 start -->
                                                    <!-- ================ -->
                                                    
                                                    <!-- slide 2 end -->
                                    
                                                </ul>
                                                <div class="tp-bannertimer"></div>
                                            </div>
                                        </div>
                                        <!-- slider revolution end -->
                                    
                                    </div>
                                    <!-- slideshow end -->
                                    
                                </div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>
                        <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
                                    <p id="para-font">&nbsp;
                                        <!--<b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>-->
                                     </p>
                                </div>
                            <div class="col-md-10">

   <!--                                <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" id="newsMarquee">
                                    <span style="color:white;font-weight:bold" >IBPS 2017-18 Tentative calender is out. IBPS to hold two-tier examination for recruitment of Specialist Officers.<i class="fa fa-caret-right pr-5 pl-10"></i> New batches for SSC, MTS/CGL,  BANK POs/CLERKs, starting on 23rd, 25th, and 27th, January 2017.</span> 
                                </marquee>-->
                            </div>
                            </div>
                             
                        </div>
			<!-- section start -->
			<!-- ================ -->
                           <div class="clearfix"></div>
			<!-- section end -->
                        <!-- section start -->
			<!-- ================ -->
                          <section class="section clearfix" id="homeRow3">
				<div class="container-fluid">
                                                <center>
                                                    <a href="youtube.php"><h3 id="heading-font"><span class="text-default">You Tube</span></h3></a>
							<div class="separator-2"></div>
						</center>
                                                            
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='Youtube' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                                        <div class="image-box shadow text-center">
							<div class="overlay-container">
                                                                <?php echo str_replace('width="560"','',$row['path']); ?>
                                                                
								<div class="overlay-top">
									<div class="text">
										<h3><a href=""><?php echo $row['title']; ?></a></h3>
										
									</div>
								</div>
								<div class="overlay-bottom">
									<div class="links">
                                                                            <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#youtube_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="youtube_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                                <?php echo $row['path']; ?>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>

									</div>
								</div>
							</div>
                                                    </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
                                                <center>
                                                    <a href="mp3.php"><h3 id="heading-font"><span class="text-default">Radio Link</span></h3></a>
							<div class="separator-2"></div>
						</center>
                                                            
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='Mp3' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                    
                                                                        <div class="image-box shadow text-center">
                                                                            <div class="image-box team-member style-2 bordered dark-bg mb-20 text-center">
										<div class="overlay-container overlay-visible">
											<center><img src="assets/images/mp3.png"/></center>
											<div class="overlay-bottom">
												<p class="small margin-clear"><?php echo $row['title']; ?></p>
											</div>
										</div>
										<div class="body">
											<audio controls><source src="server/controller/<?php echo $row['path']; ?>" type="audio/mpeg"></audio>
                                                                                        <div class="separator mt-10"></div>
                                                                                        <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#mp3_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="mp3_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                              <center><audio controls><source src="server/controller/<?php echo $row['path']; ?>" type="audio/mpeg"></audio></center>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>
										</div>
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
                                                <center>
                                                    <a href="mp4.php"><h3 id="heading-font"><span class="text-default">Video Link</span></h3></a>
							<div class="separator-2"></div>
						</center>
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='Mp4' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                                        <div class="image-box shadow text-center">
                                                                            <div class="image-box team-member style-2 bordered dark-bg mb-20 text-center">
										<div class="overlay-container overlay-visible">
                                                                                        <video width="400" controls><source src="server/controller/<?php echo $row['path']; ?>" type="audio/mpeg"></video>
										</div>
										<div class="body">
											<p class="small margin-clear"><?php echo $row['title']; ?></p>
                                                                                        <div class="separator mt-10"></div>
                                                                                                          <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#mp4_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="mp4_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                                <video width="560" controls><source src="server/controller/<?php echo $row['path']; ?>" type="audio/mpeg"></video>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>
										</div>
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
                                                <center>
                                                    <a href="ppt.php"><h3 id="heading-font"><span class="text-default">PPT</span></h3></a>
							<div class="separator-2"></div>
						</center>
                                                            
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='Ppt' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                                        <div class="image-box shadow text-center">
                                                                            <div class="image-box team-member style-2 bordered dark-bg mb-20 text-center">
        										<div class="overlay-container overlay-visible">
                                                                                        <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:300px; height:300px;" frameborder="0"></iframe>
										</div>
										<div class="body">
											<p class="small margin-clear"><?php echo $row['title']; ?></p>
                                                                                        <div class="separator mt-10"></div>
                                                                                                                      <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#PPT_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="PPT_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                                <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:560px; height:300px;" frameborder="0"></iframe>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>
										</div>
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
                                    
                                                <center>
                                                    <a href="pdf.php"><h3 id="heading-font"><span class="text-default">PDF</span></h3></a>
							<div class="separator-2"></div>
						</center>
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='PDF' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                                        <div class="image-box shadow text-center">
                                                                            <div class="image-box team-member style-2 bordered dark-bg mb-20 text-center">
        										<div class="overlay-container overlay-visible">
                                                                                        <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:300px; height:300px;" frameborder="0"></iframe>
										</div>
										<div class="body">
											<p class="small margin-clear"><?php echo $row['title']; ?></p>
                                                                                        <div class="separator mt-10"></div>
                                                                                                                      <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#PPT_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="PPT_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                                <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:560px; height:300px;" frameborder="0"></iframe>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>
										</div>
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
                                                <center>
                                                    <a href="msword.php"><h3 id="heading-font"><span class="text-default">Ms Word</span></h3></a>
							<div class="separator-2"></div>
						</center>
        					<div class="owl-carousel carousel">
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from file_details fd inner join category_details cd on cd.category_details_id=fd.category_details_id where cd.type='msword' and fd.status='Enable' order by file_details_id desc limit 5";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                                                    ?>
                                                                        <div class="image-box shadow text-center">
                                                                            <div class="image-box team-member style-2 bordered dark-bg mb-20 text-center">
        										<div class="overlay-container overlay-visible">
                                                                                        <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:300px; height:300px;" frameborder="0"></iframe>
										</div>
										<div class="body">
											<p class="small margin-clear"><?php echo $row['title']; ?></p>
                                                                                        <div class="separator mt-10"></div>
                                                                                                                      <a  class="btn btn-gray-transparent btn-animated" data-toggle="modal" data-target="#PPT_model<?php echo $row['file_details_id']; ?>">View Details <i class="pl-10 fa fa-arrow-right"></i></a>
                                                                            <div id="PPT_model<?php echo $row['file_details_id']; ?>" class="modal fade" role="dialog">
                                                                      <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content" style="    border-radius: 30px;">
                                                                            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
                                                                                    background-color: #444b65;">
                                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $row['title']; ?></h4>
                                                                                </div>
                                                                          <div class="modal-body">
                                                                                <iframe src="http://docs.google.com/gview?url=http://vibrantcareer.com/server/controller/<?php echo $row['path']; ?>&embedded=true" style="width:560px; height:300px;" frameborder="0"></iframe>
                                                                                <p ><?php echo $row['discription']; ?>.</p>
                                                                          </div>
                                                                          <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                        </div>

                                                                      </div>
                                                                    </div>
										</div>
                                                                            </div>
                                                                        </div>
                                                                <?php
                                                                    }
                                                                ?>
                                                </div>
				</div>
			</section>
                          <section class="section clearfix" id="homeRow3">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3 id="heading-font">Latest <span class="text-default">Notifications</span></h3>
							<div class="separator-2"></div>
							<div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="notificationMarquee">
                                                                 
                                                                
                                                            </marquee>   
                                                            <div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
								</div>
							
							</div>
						</div>
						<div class="col-md-3">
							<h3 id="heading-font">Notice <span class="text-default">Board</span></h3>
							<div class="separator-2"></div>
                                                        <div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="noticeboardMarquee">
                                                                
                                                                
                                                            </marquee>   
                                                            <div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
                                                            </div>
							
							</div>
							<!-- accordion start -->
							<!-- ================ -->
							
							<!-- accordion end -->
						</div>
                                            <div class="col-md-3">
							<h3 id="heading-font">Latest <span class="text-default">News</span></h3>
							<div class="separator-2"></div>
							<div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="newsMarquee">
                                                                 
                                                                
                                                            </marquee>   
                                                            <div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
								</div>
							
							</div>
						</div>
                                            <div class="col-md-3">
							<h3 id="heading-font">Our <span class="text-default">Selections</span></h3>
							<div class="separator-2"></div>
                                                        <div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="selectionsMarquee">
                                                                 
<!--                                                                <div class="media margin-clear" id="newsAlert-wrap">
                                                                      <div class="media-left" id="newsAlert-left3">
                                                                          <div class="testimonial-image">
                                                                              <img src="assets/images/testimonials/Aman_Indian_BankPO.jpg" style="height:70px;width:70px;" alt="" title="" class="img-circle">
                                                                          </div>	
									</div>
                                                                       <div class="media-body">
                                                                            <h5 class="" id="heading-font">Aman Trivedi</h5>
                                                                            <p id="Alumini-Post">PO SBI</p>
                                                                        </div>
									<div class="separator-2"></div>
								</div>-->
                                                                
                                                            </marquee>   
                                                            
                                                         
								
								
								<div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
								</div>
							
							</div>

						</div>
                                       
						
					</div>
				</div>
			</section>
                        <section class="section clearfix" id="homeRow1" style="box-shadow:inset 1px 1px 10px #000;">
				<div class="container">
					<div class="row">
						    <div class="col-md-6">
							<h3 id="heading-font">Thought of the <span class="text-default">Day</span></h3>
							<div class="separator-2"></div>
                                                       
                                                           
                                                                <div  id="qDay">
                                                                    <blockquote class="margin-clear" id="thought_of_day">
                                                                        <h5 id="heading-font" class="text-justify" >"By the study of different religions we find that in essence they are one. –"</h5>
                                                                        <small style="float:left;font-weight:bold">Swami Vivekananda</small><br/><br/>
                                                                        <small style="float:left;">Posted By</small>
                                                                        <br/>
                                                                        <div id="tposted">
                                                                            <img src="server/controller/upload/1490341951.png" height="50" width="50" id="thought-image"/>
                                                                            <p style="position:relative;left:10px;top:10px;font-weight:bold;color:#000">Lalit Pastor</p>
                                                                        </div>
                                                                       
                                                                            
                                                                         
                                                                    </blockquote>
                                                                    
                                                                   
                                                                </div>
                                                                
                                                           
                                                  
                                                       
                                                        
							<!-- accordion start -->
							<!-- ================ -->
                                                        <p>&nbsp;</p>
                                                        <?php   
                                                            if(isset($_SESSION['user_id'])){
                                                             if($_SESSION['user_type']==='normal'){ ?>
                                                                <a href="registration_form.php" class="btn square btn-default btn-lg" ><b>Class Registration form</b></a>
                                                            <?php     } 
                                                            
                                                             }else{ ?>
                                                                  <button href="#" class="btn square btn-default btn-lg" data-toggle="modal" data-target="#demoRegistration"><b>Registration for Demo</b></button>
                                                         <?php    } 
                                                            ?>
                                                        
                                                         
							<!-- accordion end -->
						</div>
						    <div class="col-md-6">
							<h3 id="heading-font">Question of the <span class="text-default">Day</span></h3>
							<div class="separator-2"></div>
                                                        <div  id="qDay">
                                                            <blockquote class="margin-clear" id="question_of_day">
                                                                <h5 id="heading-font">If one number of a Pythagorean triplet is 16, find the other two numbers ?</h5>
                                                            </blockquote>
                                                            
							
							</div>
							<!-- accordion start -->
							<!-- ================ -->
                                                        <p>&nbsp;</p>
                                                         
							<!-- accordion end -->
						</div>
                                            
                                     
                                       
						
					</div>
				</div>
			</section>
			<section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-8">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Vibrant </strong> Education Services </h1>
                                                    <div class="separator"></div>
                                                         <p id="cpara">We strive to deliver comprehensive and continuously enhanced quality education through on established quality management by leveraging over core competence and thereby enhancing values of our students.</p>
                                                         <p id="cpara">Informally <b>VIBRANT EDUCATION SERVICES </b>started in 2001 with a view to provide an educational platform to students so that they can transform their potential in grabbing a desired goal. Hence today <b>VIBRANT EDUCATION SERVICES </b>has become a premier Institute in the region and consistently striving to become leader in education in India apparently by maintaining its quality conventions. </p>
                                                         <p>&nbsp;</p>
                                                        
						</div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/DSCN1593.JPG" />
                                                        <a href="assets/images/DSCN1593.JPG" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			<!-- section end -->
                     
			
			<div class="clearfix"></div>
			<!-- section end -->
			 <section class="pv-30  padding-bottom-clear dark-translucent-bg " id="row-testimonials"style="background:url('assets/images/bg/slide_3_bg.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="space-bottom">
                                <div class="owl-carousel content-slider" id="SelTestimonials">
                                    <?php include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from feedback";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    while($row = $course_batch_details->fetch()){
                                    ?>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="server/controller/<?php echo $row['image']; ?>" style="height:70px;width:70px;" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p id="cpara"><?php echo $row['message']; ?></p>
                                                        </blockquote>
                                                        <div class="cpara3"><?php echo $row['name']; ?></div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               <?php  }   ?>
                                  
                                </div>
                                    
                            </div>
                        </section> 
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

                <script type="text/javascript">
                    $(document).ready(function() {
                        onLoadNotifications();
                        onLoadNoticeboard();
                        onLoadSelSelections();
                        onLoadSelNews();
                        onLoadSelQuestionOfDay();
                        onLoadSelThoughtOfDay();
//                        onLoadSelTestimonials();
setTimeout(function (){
    <?php   if(!isset($_SESSION['user_id'])){ ?>
        showForm();
    <?php } 
    ?>
},10000);
//window.onbeforeunload = function() {
//    showForm();
//    return "Do you want to fill the Forms for demo class.";
//}

});
function showForm() {
    if(check_demo_reg_form)
    $("#demoRegistration").modal("show");
}                    


                </script>
                 <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelNotifications.js"></script>
                <script type="text/javascript" src="ajax/SelNoticeboard.js"></script>
                <script type="text/javascript" src="ajax/SelSelections.js"></script>
                <script type="text/javascript" src="ajax/SelNews.js"></script>
                <script type="text/javascript" src="ajax/SelQuestionOfDay.js"></script>
                <script type="text/javascript" src="ajax/SelThoughtOfDay.js"></script>
<!--                <script type="text/javascript" src="ajax/SelTestimonials.js"></script>-->
               
	</body>
</html>
