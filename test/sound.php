<?php
//App credentials. Create one at http://soundcloud.com/you/apps
define('API_URL', 'https://api.soundcloud.com');
define('CLIENT_ID', '');
define('CLIENT_SECRET', '');

//User credentials
define('EMAIL', 'vibrant232@gmail.com');
define('PASSWORD', 'Vibrant@2323');

//Path to file to upload
define('FILE', 'xyz.mp3');

class SoundcloudAPI {
    private $url;
    private $clientID;
    private $secret;
    private $accessToken;

    public function __construct($url, $clientID, $secret) {
        $this->url = $url;
        $this->clientID = $clientID;
        $this->secret = $secret;
    }

    public function auth($username, $password) {
        $url = $this->url . '/oauth2/token';
        $data = array(
            'client_id' => $this->clientID,
            'client_secret' => $this->secret,
            'grant_type' => 'password',
            'username' => $username,
            'password' => $password
        );

        $result = $this->request($url, $data, 'POST');
        $this->accessToken = $result->access_token;
        return $result;
    }

    public function upload($title, $path) {
        $url = $this->url . '/tracks';
        $data = array(
            'oauth_token' => $this->accessToken,
            'track[title]' => $title,
            'track[asset_data]' => new CurlFile(realpath($path), 'audio/mpeg')
        );

        $result = $this->request($url, $data, 'POST');
        return $result;
    }

    private function request($url, $data, $method) {
        $curl = curl_init();
        if ($method === 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        } else {
            $url .= '?' . http_build_query($data);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $result = json_decode(curl_exec($curl));
        curl_close($curl);

        return $result;
    }
}

$soundCloud = new SoundcloudAPI(API_URL, CLIENT_ID, CLIENT_SECRET);
$soundCloud->auth(EMAIL, PASSWORD);
$result = $soundCloud->upload('My Test File', FILE);

echo '<pre>';
print_r($result);
echo '</pre>';