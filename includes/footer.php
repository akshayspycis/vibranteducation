<footer id="footer" class="clearfix ">
                            <div class="container-fluid" id="footer-top"></div>
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" id="footer-middle">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <div class="logo-footer"><img id="logo-footer" src="assets/images/vibr.png" alt=""></div>
										<p id="cpara">Banking Industry in India has always been one of the most preferred choice of employment. In the last 10-15 years industry has emerged as a sunrise sector in Indian economy. The banking industry is recruiting in a big way with a new pattern. According to the sources, in the next five six years about 10 lakh vacancies are to be filled in the banks itself.... <a href="page-about.html">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<nav>
											<ul class="nav nav-pills nav-stacked" >
                                                                                            <li><a target="_blank" href="terms-and-conditions.php">Terms, Conditions & Privacy</a></li>
											</ul>
										</nav>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content" >
										<h2 class="title">Latest From Blog</h2>
										<div class="separator-2"></div>
                                                                                <div id="blog_details_client_in_footer"></div>
                                                                                <center><div class="space-top">
                                                                                    <a href="blog.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
                                                                                 </center>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Portfolio Gallery</h2>
										<div class="separator-2"></div>
                                                                                
										<div class="row grid-space-10">
                                                                                    <div id="gallery_details_client_in_footer"></div>
											
											
										</div>
										<div class="text-right space-top">
                                                                                    <a href="gallery.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>	
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
										<h2 class="title">Find Us</h2>
										<!--<div class="separator-2"></div>-->
										<!--<p>Find us on Social Media</p>-->
										<ul class="social-links circle animated-effect-1">
<!--											<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
											<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>-->
										</ul>
										<div class="separator-2"></div>
										<ul class="list-icons">
                                                                                    <li><i class="fa fa-map-marker pr-10 text-default"></i> Vibrant Education Services,<br> 164, I & II floor,
        Samanvay Nagar <br>(In front of ondoor ), <br> Awadhpuri, Bhopal (M.P.)</li>
											<li><i class="fa fa-phone pr-10 text-default"></i>98-262-262-99</li>
											<li><a href="mailto:info@theproject.com"><i class="fa fa-envelope-o pr-10"></i>contactus@vibrantcareer.com</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="footer-top">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p  id="cpara2">Copyright © 2017 Vibrant Education Services. All Rights Reserved</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
<script>
            onload_blog();
            function onload_blog(){
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetails.php",
                    data:{'category_id':""},
                    success: function(data) {
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client_in_footer").empty();
                        var count=0;
                        $.each(duce, function (index, article) {
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                $("#blog_details_client_in_footer").append($("<div>").addClass("media margin-clear")
                                                .append($("<div>").addClass("media-left")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}))
                                                        .append($("<a>").addClass("overlay-link small")))
                                                 )
                                                .append($("<div>").addClass("media-body")
                                                        .append($("<h6>").addClass("media-heading heading-font")
                                                            .append($("<a>").append(article.title)).click(function (){
                                                                 window.location="blogpostdetail.php?id="+article.blog_details_id;
                                                            }).css({'cursor':'pointer'}))
                                                            .append($("<p>").addClass("small margin-clear")
                                                                 .append($("<i>").addClass("fa fa-calendar pr-10"))
                                                                 .append(article.date)
                                                            )
                                                )
                                                .append($("<hr>"))
                                                );
                           }
                         count++;
                         if(count==3){
                             return false;
                         }
                        });
                    }
                });
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelGallery.php",
                    data:{'category_id':""},
                    success: function(data) {
//                    alert(data)
                       var duce = jQuery.parseJSON(data);
                        $("#gallery_details_client_in_footer").empty();
                        var count=0;
                        $.each(duce, function (index, article) {
                           var img= "server/controller/"+article.ipath;
                           var btn_class="btn-danger";
                                $("#gallery_details_client_in_footer").append($('<div class="col-xs-4 col-md-6">'
												+'<div class="overlay-container mb-10">'
                                                                                                    +'<img src="'+img+'" alt="">'
													+'<a class="overlay-link small">'
														+'<i class="fa fa-link"></i>'
													+'</a>'
												+'</div>'
											+'</div>'));
                         count++;
                         if(count==6){
                             return false;
                         }
                        });
                    }
                });
                }
            
            
</script>