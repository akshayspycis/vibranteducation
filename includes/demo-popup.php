<!--<a href="#" class="btn btn-default btn-sm btn-hvr hvr-shutter-out-horizontal margin-clear" data-toggle="modal" data-target="#demoPopUp">Registered for Demo<i class="fa fa-arrow-right pl-10"></i></a>-->
					

<div class="modal fade" id="demoPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Please Fill the form</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="intDemo" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" id="career_id" name="career_id" placeholder="">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder=" Contact No">
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control" rows="3" id="message" name="message"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Upload Resume (In Pdf Format)</label>
                        <input type="file" id="pdf_temp" name="pdf_temp">
                        <input type="hidden" id="pdf" name="pdf">
                    </div>
                    <button type="submit" class="btn btn-default">Send</button>
                </form>
            </div>
            <div class="modal-footer">
                <div id="successMsg" class="success pull-left" style="font-weight:bold;"></div>
                <div id="errorMsg" class="validate pull-left" style="font-weight:bold;"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#intDemo').submit(function() { 
        
        if(this.name.value === "") {
                  $("#demoPopUp").find("#errorMsg").empty(); 
                  $("#demoPopUp").find("#errorMsg").append("Please choose valid 'Name'.") ;
                  this.name.focus();
                  return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
            $("#demoPopUp").find("#errorMsg").empty(); 
            $("#demoPopUp").find("#errorMsg").append("Please choose valid 'Email'.") ;
            this.email.focus();
            return false;
        }
        else if(this.contact_no.value == ""|| !contactnofilter.test(this.contact_no.value)) {
            $("#demoPopUp").find("#errorMsg").empty(); 
            $("#demoPopUp").find("#errorMsg").append("Please choose valid 'Contact Number'.") ;
            this.contact_no.focus();
            return false;
        }
        else if (this.pdf_temp.value == "") {
            $("#demoPopUp").find("#errorMsg").empty(); 
            $("#demoPopUp").find("#errorMsg").append("Please attached resume") ;
            this.pdf_temp.focus();
            return false;
        }
        else {
               var selectedFile = document.getElementById("pdf_temp").files;
             if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            var base64;
            // Onload of file read the file content
            fileReader.onload = function(fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                
                $("#pdf").val(base64);
               send();
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        }

        }
       return false;
    });
});

function send(){
         $.ajax({
                type:"post",
                url:"server/controller/InsApplyJob.php",
                data:$('#intDemo').serialize(),
                success: function(data){
                    $("#demoPopUp").find("#successMsg").html("Your Application is applied sucessfully.") ;
                    setTimeout(function (){
                        location.reload(true)
                    },2000)
                 }
               
            });
}

</script>
            