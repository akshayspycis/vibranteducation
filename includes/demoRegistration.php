<div class="modal fade" id="demoRegistration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " >
        <div class="modal-content" style="    border-radius: 30px;">
            <div class="modal-header" style="    border-top-left-radius: 22px;border-top-right-radius:22px;background-image: radial-gradient(ellipse farthest-corner at 0px 45px , #444b65 0%, rgba(0, 0, 255, 0) 3%, #2095fc 100%);
    background-color: #444b65;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Please fill the Forms for demo class</h4>
            </div>
            <div class="modal-body">
                <div class=" pull-left p-30 light-gray-bg border-clear" style="width:90%">
                   
                    <form class="form-horizontal userSignUp" id="userDemoRegistration" method="post" role="form" enctype="multipart/form-data">
                        
                        <div class="form-group has-feedback">
                           <div class="col-sm-4">
                                
                            </div>
                            <div class="col-sm-8">
                                <div id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></div>
                                <div id="successMsg" style="color:green;font-family:verdana;font-weight:bold"></div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputName" class="col-sm-4 control-label">Full Name <span class="text-danger small">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="demo_user_name" name="demo_user_name" placeholder="Your Name" >
                                <i class="fa fa-pencil form-control-feedback"></i>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="Gender" class="col-sm-4 control-label">Gender<span class="text-danger small">*</span></label>
                            <div class="col-sm-6">
                                <div class="radio col-sm-6">
                                    <label>
                                        <input type="radio" name="demo_user_gender" id="demo_user_gender" value="male" >
                                        Female
                                    </label>
                                </div>
                                <div class="radio col-sm-6">
                                    <label>
                                        <input type="radio" name="demo_user_gender" id="demo_user_gender" value="female" >
                                        Male
                                    </label>
                                </div>
                               
                            </div>
                        </div>
                        
                        <div class="form-group has-feedback">
                            <label for="inputEmail" class="col-sm-4 control-label">Email <span class="text-danger small">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="demo_user_email" name="demo_user_email" placeholder="Email" >
                                <i class="fa fa-envelope form-control-feedback"></i>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="inputContact" class="col-sm-4 control-label">Contact <span class="text-danger small">*</span>
                                <br>
                                <small style="font-size: 10px; color: brown;">You will get Otp on this Number.</small>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="demo_user_contact" name="demo_user_contact" placeholder="Contact No." >
                                <i class="fa fa-phone form-control-feedback"></i>
                            </div>
                        </div>
                        <div class="form-group has-feedback" >
                            <label for="inputContact" class="col-sm-4 control-label">Class</label>
                            <div class="col-sm-7" id="demo_class_button">
                                
                            </div>
                            <div class="pull-right col-sm-1" id="loading_date">
                                <img src="assets/images/fb_ty.gif" style="margin-top: 22px; display: none;height: 11px;">
                            </div>
                            
                        </div>
                        
                        <div class="form-group has-feedback">
                            <label for="inputPassword" class="col-sm-4 control-label">Select Date <span class="text-danger small">*</span></label>
                            <div class="col-sm-8">
                                <select class="form-control" name="demo_user_select_date" id="demo_user_select_date">
                                </select>
                            </div>
                        </div>
                         
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"    name="terms" id="terms" > Accept our <a href="terms-and-conditions.php" target="_blank">privacy policy</a> and <a href="terms-and-conditions.php" target="_blank">customer agreement</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button type="submit" name="submit" id="submit" class="btn btn-group btn-default btn-animated">Next <i class="fa fa-check"></i></button>
                                <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                      
                    </form>
                    
                </div>
                <div class="clearfix"></div>
            </div>
            
        </div>
    </div>
</div>
<div id="checkOtp" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter six digit code sent on your mobile.</h4>
      </div>
        <form class="form-horizontal userSignUp" id="otpConfirm_form">
        <div class="modal-body" style="height:87px">
                <div class="row">
                    <p>&nbsp;</p>
                    <label for="inputCode" class="col-sm-4 control-label"> Enter Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="confirmCode" name="confirmCode" placeholder="Please Enter Six Digit Code" >
                    </div>
                </div>
            <!--</form>-->    
          
      </div>
        <div class="modal-footer" style="border-top: none;">
            <p id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></p>
            <button type="submit" id="otp_submit" class="btn btn-default" >Submit</button>
            <button type="button" id="otp_resend" class="btn btn-primary" style="margin-top: 4px;">Re send</button>
        </div>
      </form>
    </div>

  </div>
</div>
<div id="success_msg_demo" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Success</h4>
        </div>
        <div class="modal-body" style="height:150px">
            <h5>Thank you for registration. </h5>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="ajax/demoRegistration.js"></script>
