<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/vibr.png">
<!-- Web Fonts -->
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7598692/697684/css/fonts.css">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>
<!-- Bootstrap core CSS -->
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- Fontello CSS -->
<link href="assets/fonts/fontello/css/fontello.css" rel="stylesheet">
<!-- Plugins -->
<link href="assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="assets/plugins/rs-plugin/css/settings.css" rel="stylesheet">
<link href="assets/css/animations.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
<link href="assets/plugins/hover/hover-min.css" rel="stylesheet">		
<!-- the project core CSS file -->
<link href="assets/css/style.css" rel="stylesheet" >
<!-- Custom css --> 
<link href="assets/css/custom.css" rel="stylesheet">
<link href="assets/plugins/morphext/morphext.css" rel="stylesheet">
<script type="text/javascript" src="assets/plugins/jquery.min.js"></script>