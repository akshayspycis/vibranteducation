<div class="modal fade" id="mySignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="form-block center-block p-30 light-gray-bg border-clear">
                <h2 class="title">Sign Up</h2>
                <form class="form-horizontal userSignUp" id="UserRegistration" method="post" role="form">
                    <div class="form-group has-feedback">
                        <label for="inputName" class="col-sm-3 control-label">Full Name <span class="text-danger small">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Your Name" >
                            <i class="fa fa-pencil form-control-feedback"></i>
                        </div>
                    </div>
                   
                    <div class="form-group has-feedback">
                        <label for="inputEmail" class="col-sm-3 control-label">Email <span class="text-danger small">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" >
                            <i class="fa fa-envelope form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="inputEmail" class="col-sm-3 control-label">Contact <span class="text-danger small">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No." >
                            <i class="fa fa-phone form-control-feedback"></i>
                        </div>
                    </div>
                    
                    <div class="form-group has-feedback">
                        <label for="inputPassword" class="col-sm-3 control-label">Password <span class="text-danger small">*</span></label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" >
                            <i class="fa fa-lock form-control-feedback"></i>
                        </div>
                    </div>
                     
<!--                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked name="terms" > Accept our <a href="#">privacy policy</a> and <a href="#">customer agreement</a>
                                </label>
                            </div>
                        </div>
                    </div>-->
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button type="submit" name="submit" id="submit" class="btn btn-group btn-default btn-animated">Sign Up <i class="fa fa-check"></i></button>
                            <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
                <div id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></div>
                <div id="successMsg" style="color:green;font-family:verdana;font-weight:bold"></div>
            </div>
                <div class="clearfix"></div>
            </div>
     </div>
</div>
<div class="modal fade" id="mySignIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="form-block center-block p-30 light-gray-bg border-clear">
                <h2 class="title">Login</h2>
                <form class="form-horizontal" id="userLogin">
                    <div class="form-group has-feedback">
                        <label for="inputUserName" class="col-sm-3 control-label">Your Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Your Email" >
                            <i class="fa fa-user form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="inputPassword" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" >
                            <i class="fa fa-lock form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <!--<input type="checkbox" > Remember me.-->
                                </label>
                            </div>											
                            <button type="submit" class="btn btn-group btn-default btn-animated">Log In <i class="fa fa-user"></i></button>
                            <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                            <ul class="space-top">
                                <li><a href="#" id="forgotpassword_button">Forgot your password?</a></li>
                            </ul>
<!--                            <span class="text-center text-muted">Login with</span>
                            <ul class="social-links colored circle clearfix">
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                            </ul>-->
                        </div>
                    </div>
                </form>
                                                                    
                <div id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></div>
                <div id="successMsg" style="color:green;font-family:verdana;font-weight:bold"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="forgotpassword_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-body">
            <div class="form-block center-block p-30 light-gray-bg border-clear">
                <h2 class="title">Forgot Password</h2>
                <form class="form-horizontal" id="forgotpassword">
                    <div class="form-group has-feedback">
                        <label for="inputUserName" class="col-sm-3 control-label">Your Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="forgotemail" name="forgotemail" placeholder="Your Email" >
                            <i class="fa fa-user form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <div id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></div>
                            <div id="successMsg" style="color:green;font-family:verdana;font-weight:bold"></div>
                              <button type="submit" class="btn btn-group btn-default btn-animated">Reset Password <i class="fa fa-user"></i></button>
                              <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
                                                                    
                
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="ajax/forgotpassword.js"></script>
<script type="text/javascript" src="ajax/UserSignup.js"></script>
<script type="text/javascript" src="ajax/UserLogin.js"></script>