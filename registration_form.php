<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Class Registration Form Section </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>
      
        
<style>
        .uploadArea1,.uploadArea2,.uploadArea3,.uploadArea4{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
         .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
 
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                              <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Registration Form</strong> </h1>
                                                    <div class="separator"></div>
                                            </div>
                                    <p>&nbsp;</p>
                                 </div>
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="process">
								<!-- Nav tabs -->
								<ul class="nav nav-pills" role="tablist">
									<li class="active"><a href="#pill-pr-1" role="tab" data-toggle="tab" title="Step 1"><i class="fa fa-dot-circle-o pr-5"></i> Step 1</a></li>
                                                                        <li class="disabled"><a href="#pill-pr-2" role="tab" title="Step 2"><i class="fa fa-dot-circle-o pr-5"></i> Step 2</a></li>
									<li class="disabled"><a href="#pill-pr-3" role="tab"  title="Step 3"><i class="fa fa-dot-circle-o pr-5"></i> Step 3</a></li>
									<li class="disabled"><a href="#pill-pr-4" role="tab" title="Step 4"><i class="fa fa-dot-circle-o pr-5"></i> Step 4</a></li>
									<li class="disabled"> <a href="#pill-pr-5" role="tab" title="Step 5"><i class="fa fa-dot-circle-o pr-5"></i> Step 5</a></li>
									<li class="disabled"> <a href="#pill-pr-6" role="tab" title="Step 6"><i class="fa fa-dot-circle-o pr-5"></i> Step 6</a></li>
									<li class="disabled"><a href="#pill-pr-7" role="tab" title="Step 7"><i class="fa fa-dot-circle-o pr-5"></i> Step 7</a></li>
									<li class="disabled"><a href="#pill-pr-8" role="tab" title="Step 8"><i class="fa fa-dot-circle-o pr-5"></i> Step 8</a></li>
								</ul>
								<!-- Tab panes -->
                                                                <div class="tab-content clear-style">
                                                                    <div class="tab-pane active" id="pill-pr-1">
                                                                       
                                                                                <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Personal Details of the Candidate.</h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        <form role="form" id="pill-pr-form-1">
                                                                                        
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Name </label>
                                                                                            <input type="text" class="form-control" id="user_name" name="user_name" disabled="true">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Gender</label>
                                                                                            <div class="form-group has-feedback">
                                                                                                <label>
                                                                                                    <input type="radio" name="gender" id="gender" value="male" >
                                                                                                    Female
                                                                                                </label>
                                                                                                <label>
                                                                                                    <input type="radio" name="gender" id="gender" value="female">
                                                                                                    Male
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Contact no</label>
                                                                                            <input type="number" class="form-control" id="contact_no" name="contact_no" disabled="true">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Email</label>
                                                                                            <input type="email" class="form-control" id="email" name="email" disabled="true">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputPassword1">Date Of Birth</label>
                                                                                            <input type="date" class="form-control" id="dob" name="dob" >
                                                                                        </div>
                                                                                            <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="submit" class="btn btn-default pull-right">Next</button>
                                                                                        </form>
                                                                                    </div>
                                                                                    
                                                                                
                                                                                
                                                                                </div>
                                                                     
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-2">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Guardian Details</h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                                 <form role="form" id="pill-pr-form-2">
                                                                                        
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Father’s name/Guardian/Husband’s name </label>
                                                                                            <input type="text" class="form-control" id="relative_name" name="relative_name" >
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Contact number Guardian’s (Do not prefix 91)  </label>
                                                                                            <input type="number" class="form-control" id="relative_contact_no" name="relative_contact_no" >
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Father’s occupation/Guardian’s occupation/Husband’s occupation </label>
                                                                                            <input type="text" class="form-control" id="relative_occupation" name="relative_occupation" >
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Applicant’s occupation </label>
                                                                                            <input type="text" class="form-control" id="applicant_occupation" name="applicant_occupation" >
                                                                                        </div>
                                                                                        
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">School </label>
                                                                                            <input type="text" class="form-control" id="school" name="school">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Collage </label>
                                                                                            <input type="text" class="form-control" id="college" name="college" >
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="exampleInputEmail1">Name and Address of Institution/Company where Applicant is studying/working </label>
                                                                                            <input type="text" class="form-control" id="address_of_institiute_or_company" name="address_of_institiute_or_company" >
                                                                                        </div>
                                                                                         <div class="form-group">
                                                                                             <label for="exampleInputEmail1">Referal Name</label>
                                                                                             <input type="text" class="form-control" id="referal_name" name="referal_name" >
                                                                                         </div>
                                                                                         <div class="form-group">
                                                                                             <label for="exampleInputEmail1">Referal Contact (Do not prefix 91)  </label>
                                                                                             <input type="number" class="form-control" id="referal_contact_no" name="referal_contact_no" >
                                                                                         </div>
                                                                                        <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                                 </form>
                                                                                    </div>
                                                                                    
                                                                                
                                                                                
                                                                                </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-3">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Current Corresponding address</h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        <form role="form" id="pill-pr-form-3">
                                                                                        
                                                                                        <p>&nbsp;</p>
                                                                                        <label for="exampleInputEmail1">Local Address(Temporary) </label>
                                                                                        <p>&nbsp;</p>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputEmail1">Address</label>
                                                                                            <input type="text" class="form-control" id="address_l" name="address_l" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputEmail1">Street</label>
                                                                                            <input type="text" class="form-control" id="street_l" name="street_l" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">City</label>
                                                                                            <input type="text" class="form-control" id="city_l" name="city_l" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">Pincode</label>
                                                                                            <input type="text" class="form-control" id="pincode_l" name="pincode_l" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">State</label>
                                                                                            <select class="form-control" name="state_l" id="state_l">
                                                                                                <option value="">Select State</option>
<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Orissa">Orissa</option>
<option value="Pondicherry">Pondicherry</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Tripura">Tripura</option>
<option value="Uttaranchal">Uttaranchal</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="West Bengal">West Bengal</option>
                                                                                            </select>
                                                                                        </div>
                                                                                         <p>&nbsp;</p>
                                                                                        <label for="exampleInputEmail1">Permanent Address</label>
                                                                                        <label class="pull-right"><input type="checkbox" name="same_local_per" id="same_local_per"  > &nbsp;&nbsp;&nbsp;<small>Same as Local Address</small></label>
                                                                                        <p>&nbsp;</p>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputEmail1">Address</label>
                                                                                            <input type="text" class="form-control" id="address_p" name="address_p" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-6">
                                                                                            <label for="exampleInputEmail1">Street</label>
                                                                                            <input type="text" class="form-control" id="street_p" name="street_p" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">City</label>
                                                                                            <input type="text" class="form-control" id="city_p" name="city_p" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">Pincode</label>
                                                                                            <input type="text" class="form-control" id="pincode_p" name="pincode_p" >
                                                                                        </div>
                                                                                        <div class="form-group col-md-4">
                                                                                            <label for="exampleInputEmail1">State</label>
                                                                                            <select class="form-control" name="state_p" id="state_p">
                                                                                                <option value="">Select State</option>
<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Orissa">Orissa</option>
<option value="Pondicherry">Pondicherry</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Tripura">Tripura</option>
<option value="Uttaranchal">Uttaranchal</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="West Bengal">West Bengal</option>
                                                                                            </select>
                                                                                        </div>
                                                                                            <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                                 </form>
                                                                                    </div>
                                                                                    
                                                                                
                                                                                
                                                                                </div>
                                                                    </div>
                                                                     <div class="tab-pane" id="pill-pr-4">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Academic qualification </h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-1"></div>
                                                                                    <div class="col-md-9">
                                                                                        <form role="form" id="pill-pr-form-4">
                                                                                        
                                                                                        <div class="table-responsive">
								<table class="table" >
									<thead>
										<tr>
											<th>Class / Graduation</th>
											<th>School/College</th>
											<th>Board/University</th>
											<th>Year of passing</th>
											<th>Percentage</th>
										</tr>
									</thead>
                                                                        <tbody id="qualification_details">
										
										<tr>
                                                                                    <td><input type="text" class="form-control" id="class" name="class" value="10th" disabled="true"></td>
											<td><input type="text" class="form-control" id="school_college" name="school_college" ></td>
											<td><input type="text" class="form-control" id="board_university" name="board_university" ></td>
											<td><input type="text" class="form-control" id="year_of_passing" name="year_of_passing" ></td>
											<td><input type="text" class="form-control" id="percentage" name="percentage" ></td>
										</tr>
										<tr>
											<td><input type="text" class="form-control" id="class" name="class" value="12th" disabled="true"></td>
											<td><input type="text" class="form-control" id="school_college" name="school_college" ></td>
											<td><input type="text" class="form-control" id="board_university" name="board_university" ></td>
											<td><input type="text" class="form-control" id="year_of_passing" name="year_of_passing" ></td>
											<td><input type="text" class="form-control" id="percentage" name="percentage" ></td>
										</tr>
										<tr>
                                                                                    <td><input type="text" class="form-control" id="relative_name" name="relative_name" placeholder="U G" ></td>
											<td><input type="text" class="form-control" id="school_college" name="school_college" ></td>
											<td><input type="text" class="form-control" id="board_university" name="board_university" ></td>
											<td><input type="text" class="form-control" id="year_of_passing" name="year_of_passing" ></td>
											<td><input type="text" class="form-control" id="percentage" name="percentage" ></td>
										</tr>
										<tr>
											<td><input type="text" class="form-control" id="class" name="class" placeholder="Post G" ></td>
											<td><input type="text" class="form-control" id="school_college" name="school_college" ></td>
											<td><input type="text" class="form-control" id="board_university" name="board_university" ></td>
											<td><input type="text" class="form-control" id="year_of_passing" name="year_of_passing" ></td>
											<td><input type="text" class="form-control" id="percentage" name="percentage" ></td>
										</tr>
										
									</tbody>
								</table>
							</div>
                                                                                      
                                                                                        <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                                 </form>
                                                                                    </div>
                                                                                    
                                                                                
                                                                                
                                                                                </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-5">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Other Details</h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        <form role="form" id="pill-pr-form-5">
                                                                                            <div class="form-group">
                                                                                                <label for="exampleInputEmail1">Additional Qualification </label>
                                                                                                <input type="text" class="form-control" id="additional_qualification" name="additional_qualification" >
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label for="exampleInputEmail1">Computer literacy </label>
                                                                                                <input type="text" class="form-control" id="computer_literacy" name="computer_literacy" >
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label for="exampleInputEmail1">Language Proficiency (Please tick √ )</label>
                                                                                                <table class="table">
                                                                                                    <thead>
                                                                                                            <tr>
                                                                                                                    <th>Language</th>
                                                                                                                    <th>Read</th>
                                                                                                                    <th>Write</th>
                                                                                                                    <th>Speak</th>
                                                                                                            </tr>
                                                                                                    </thead>
                                                                                                    <tbody id="language_proficiency">
                                                                                                <tr>
                                                                                                    <td><input type="text" class="form-control" id="language" name="language" value="English" disabled="true"></td>
                                                                                                    <td><input type="checkbox" name="read" id="read"  value="true"></td>
                                                                                                    <td><input type="checkbox" name="write" id="write" value="true" ></td>
                                                                                                    <td><input type="checkbox" name="speak" id="speak" value="true" ></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><input type="text" class="form-control" id="language" name="language" value="Hindi" disabled="true"></td>
                                                                                                    <td><input type="checkbox" name="read" id="read"  value="true"></td>
                                                                                                    <td><input type="checkbox" name="write" id="write"  value="true"></td>
                                                                                                    <td><input type="checkbox" name="speak" id="speak"  value="true"></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td><input type="text" class="form-control" id="language" name="language" placeholder ="Other (specify)" ></td>
                                                                                                    <td><input type="checkbox" name="read" id="read"  value="true"></td>
                                                                                                    <td><input type="checkbox" name="write" id="write"  value="true"></td>
                                                                                                    <td><input type="checkbox" name="speak" id="speak"  value="true"></td>
                                                                                                </tr>
                                                                            </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                            <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-6">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        <form role="form" id="pill-pr-form-6">
                                                                                        <h3 class="heading-font text-center">Work Experience (If any) Organisation</h3>
                                                                                        <div class="form-group">
                                                                                            <table class="table">
									<thead>
										<tr>
											<th>Organisation</th>
											<th>Experience<br>(in months till date)</th>
											<th>Job Profile or Designation</th>
										</tr>
									</thead>
                                                                        <tbody id="work_experience">
										<tr>
                                                                                    <td><input type="text" class="form-control" id="organization" name="organization" placeholder="Organisation Name"></td>
											<td><input type="text" class="form-control" id="time" name="time" ></td>
											<td><input type="text" class="form-control" id="designation" name="designation" ></td>
										</tr>
									</tbody>
                                                                        <tfoot class="">
                                                                            <tr>
                                                                              <td></td>
                                                                              <td></td>
                                                                              <td><button type="button" class="btn btn-default pull-right" id="add_work_experience">Add +</button></td>
                                                                            </tr>
                                                                          </tfoot>
								</table>
                                                                                        </div>
                                                                                        <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                        </form>
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-7">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Course applicant wish to enroll for (Please tick)</h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        <form role="form" id="pill-pr-form-7">
                                                                                        <div class="form-group">
                                                                                            <table class="table">
									<thead>
										<tr>
											<th>S.No.</th>
											<th>Course Opted</th>
											<th>Tick</th>
										</tr>
									</thead>
                                                                        <tbody id="course_opted_details">
										<tr>
                                                                                        <td>1</td>
											<td>Bank PO/MT and Clerk</td>
											<td><input type="checkbox" name="course_po_mt_and_clerk" id="course_po_mt_and_clerk" value="true"></td>
										</tr>
										<tr>
                                                                                    <td>2</td>
											<td>Advanced Communicative English</td>
											<td><input type="checkbox" name="advance_comm_english" id="advance_comm_english" value="true"></td>
										</tr>
										<tr>
                                                                                    <td>3</td>
											<td>SSC and Railways</td>
											<td><input type="checkbox" name="ssc_and_railways" id="ssc_and_railways" value="true"></td>
										</tr>
										<tr>
                                                                                    <td>4</td>
											<td>English for competitive Exams</td>
											<td><input type="checkbox" name="english_for_competitive_exams" id="english_for_competitive_exams" value="true"></td>
										</tr>
										<tr>
                                                                                    <td>5</td>
											<td>Personal Interview</td>
											<td><input type="checkbox" name="personal_interviews" id="personal_interviews" value="true"></td>
										</tr>
										<tr>
                                                                                    <td>6</td>
											<td>MBA entrance exams</td>
                                                                                        <td><input type="checkbox" name="mba_entrance_exam" id="mba_entrance_exam" value="true"></td>
										</tr>
									</tbody>
								</table>
                                                                                        </div>
                                                                                        <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                            <button type="text" class="btn btn-default pull-right">Next</button>
                                                                                        </form>
                                                                                    </div>
                                                                                
                                                                                </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="pill-pr-8">
                                                                                 <div class="media margin-clear" id="registration-form" >
                                                                                     <h3 class="heading-font text-center">Bear witness  to </h3>
                                                                                     <div class="separator"></div>
                                                                                     <div class="col-md-2"></div>
                                                                                    <div class="col-md-8">
                                                                                        
                                                                                        <div class="form-group">
                                                                                            <table class="table">
									<thead>
										<tr>
											<th>S.No.</th>
											<th>Attestation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
											<th>Upload Area</th>
										</tr>
									</thead>
                                                                        <tbody id="bear_witness">
										<tr>
                                                                                        <td >1</td>
                                                                                        <td >Photograph</td>
                                                                                        
											<td width="50%"><div class="col-sm-9 pull-right">
                                                                                             <form role="form" id="pill-pr-form-upload-img-1" enctype="multipart/form-data">    
                                                                                                <div class="image-box shadow text-center mb-20">
                                                                                                        <div class="overlay-container">
                                                                                                            <div class="uploadArea1" id="dragAndDropFiles1" style="min-height: 60px;">
                                                                                                                <img src="assets/images/bg_upload.jpg" width="">
                                                                                                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload1" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                             </form>
                                                                                            </div>
                                                                                        </td>
                                                                                        
										</tr>
										<tr>
                                                                                    <td>2</td>
											<td>Id Proof</td>
                                                                                        <td width="50%"><div class="col-sm-9 pull-right">
                                                                                                <form role="form" id="pill-pr-form-upload-img-2" enctype="multipart/form-data">    
                                                                                                <div class="image-box shadow text-center mb-20">
                                                                                                        <div class="overlay-container">
                                                                                                            <div class="uploadArea2" id="dragAndDropFiles2" style="min-height: 60px;">
                                                                                                                <img src="assets/images/bg_upload.jpg" width="">
                                                                                                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload2" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </td>
										</tr>
										<tr>
                                                                                    <td>3</td>
											<td>Address proof</td>
                                                                                        <td width="50%"><div class="col-sm-9 pull-right">
                                                                                                <form role="form" id="pill-pr-form-upload-img-3" enctype="multipart/form-data">    
                                                                                                <div class="image-box shadow text-center mb-20">
                                                                                                        <div class="overlay-container">
                                                                                                            <div class="uploadArea3" id="dragAndDropFiles3" style="min-height: 60px;">
                                                                                                                <img src="assets/images/bg_upload.jpg" width="">
                                                                                                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload3" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                </form>
                                                                                            </div>
                                                                                        </td>
										</tr>
										<tr>
                                                                                    <td>4</td>
                                                                                    <td><input type="text" class="form-control" id="specify" name="specify" placeholder="Other(s); specify" style="margin-left: -3px;"></td>
                                                                                    <td width="50%"><div class="col-sm-9 pull-right">
                                                                                            <form role="form" id="pill-pr-form-upload-img-4" enctype="multipart/form-data">    
                                                                                                <div class="image-box shadow text-center mb-20">
                                                                                                        <div class="overlay-container">
                                                                                                            <div class="uploadArea4" id="dragAndDropFiles4" style="min-height: 60px;">
                                                                                                                <img src="assets/images/bg_upload.jpg" width="">
                                                                                                                <input type="file" data-maxwidth="620" data-maxheight="620" name="file[]" id="multiUpload4" name="nimage" style="width: 0px; height: 0px; overflow: hidden;">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                            </form>
                                                                                            </div>
                                                                                        </td>
                                                                                        
										</tr>
									</tbody>
								</table>
                                                                                        </div>
                                                                                        <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                                        <button type="text" class="btn btn-default pull-right" id="final_save">Save</button>
                                                                                    </div>
                                                                                
                                                                                </div>
                                                                    </div>
                                                                </div>
							</div>
                                    </div>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                  
                                    
                                </div>
                                 </div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

<div id="password_code" class="modal fade" role="dialog">
  <div class="modal-dialog">
   
   
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
         <h4 class="modal-title">Please Enter code Provided by Vibrant Education.</h4>
        </div>
        
          <div class="modal-body" style="height:87px">
                <div class="row">
                    <p>&nbsp;</p>
                    <label for="inputCode" class="col-sm-4 control-label"> Enter Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="registered_temp_code" name="registered_temp_code" placeholder="Please Enter here.." autocomplete="on">
                    </div>
                </div>
          </div>
        <div class="modal-footer" style="border-top: none;">
            <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
            <button type="submit" id="password_code_form_submit" class="btn btn-default" >Submit</button>
        </div>
        
    </div>

  </div>
</div>		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <script type="text/javascript" src="ajax/registration_form.js"></script>
                <script type="text/javascript" src="ajax/UploadImg.js"></script>
	</body>
        <div id="success_msg_demo" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
        <div class="modal-body" style="height:150px">
            <h5>Thank you for registration. Your Receipt will be send on your relevant email with in 24 Hrs.</h5>
          
      </div>
      
    </div>

  </div>
</div>
</html>
