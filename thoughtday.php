<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->



<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        
        <head>
            <script>
        var user_id="";
        <?php
        if(isset($_SESSION['user_id']))
        { ?>
        user_id=<?php echo $_SESSION['user_id']; ?>;
        <?php    }?>
        if(user_id==""){
        window.location="index.php";
        }

        </script>
            <meta charset="utf-8">
            <title>Thought of the Day</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>
<style>
        .uploadArea1,.uploadArea2,.uploadArea3,.uploadArea4{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
         .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
 
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                              <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Thought of the Day</strong> </h1>
                                                    <div class="separator"></div>
                                            </div>
                                    <p>&nbsp;</p>
                                 </div>
                                <div class="row">
                                    <div class="col-md-11">
                                                        <div class="process">
                                                            <div class="tab-pane active" id="pill-pr-1">
                                                                <div class="media margin-clear" id="registration-form" >
                                                                    <h3 class="heading-font text-center">&nbsp;</h3>
                                                                     <div class="separator"></div>
                                                                     <div class="col-md-2"></div>
                                                                     <div class="col-md-8">
                                                                         <form role="form" id ="thoughtofdayform" enctype="multipart/form-data"">
                                                                             <label style="font-family:verdana;font-weight:bold">Author's Name</label>
                                                                             <div class="form-group">
                                                                                 <input type="text" class="form-control" id="author" name="author" value="" >
                                                                             </div>
                                                                             <div class="form-group">
                                                                                 <label style="font-family:verdana;font-weight:bold">Enter Content</label>
                                                                                
                                                                                 <textarea class="form-control" rows="5" id="content_tmp"  name="content_tmp" style="resize:none" ></textarea>
                                                                             </div>
                                                                             <div class="form-group">
                                                                                 <input type="hidden" class="form-control" id="content" name="content" value="">
                                                                             </div>
                                                                                 
                                                                             <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
                                                                             <button type="submit" class="btn btn-default pull-right">Save</button>
                                                                         </form>
                                                                     </div>
                                                                     <div class="clearfix"></div>
                                                                      <div class="separator"></div>
                                                                </div>
                                                            </div>
							</div>
                                    </div>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                  
                                    
                                </div>
                                 </div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

<div id="password_code" class="modal fade" role="dialog">
  <div class="modal-dialog">
   
   
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
         <h4 class="modal-title">Please Enter code Provided by Vibrant Education.</h4>
        </div>
        
          <div class="modal-body" style="height:87px">
                <div class="row">
                    <p>&nbsp;</p>
                    <label for="inputCode" class="col-sm-4 control-label"> Enter Code </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="registered_temp_code" name="registered_temp_code" placeholder="Please Enter here.." autocomplete="on">
                    </div>
                </div>
          </div>
        <div class="modal-footer" style="border-top: none;">
            <div id="errorMsg" class="pull-left" style="color:red;font-family:verdana;font-weight:bold"></div>
            <button type="submit" id="password_code_form_submit" class="btn btn-default" >Submit</button>
        </div>
        
    </div>

  </div>
</div>		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <script type="text/javascript" src="ajax/ThoughtofDay.js"></script>
                <!--<script type="text/javascript" src="ajax/UploadImg.js"></script>-->
	</body>
        <div id="success_msg_demo" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
        <div class="modal-body" style="height:150px">
            <h5>Thank you for sharing Thought.</h5>
          
      </div>
      
    </div>

  </div>
</div>
</html>
