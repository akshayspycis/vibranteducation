<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Vibrant Education Services</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header "  onload="loadHTML('about-us')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
                            
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
			<div id="page-start"></div>
                       
			<!-- section start -->
			<!-- ================ -->
                           <div class="clearfix"></div>
			<!-- section end -->
                        <!-- section start -->
			<!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-8">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Vibrant </strong> Education Services </h1>
                                                    <div class="separator"></div>
                                                         <p id="cpara">We strive to deliver comprehensive and continuously enhanced quality education through on established quality management by leveraging over core competence and thereby enhancing values of our students.</p>
                                                         <p id="cpara">Informally <b>VIBRANT EDUCATION SERVICES </b>started in 2001 with a view to provide an educational platform to students so that they can transform their potential in grabbing a desired goal. Hence today <b>VIBRANT EDUCATION SERVICES </b>has become a premier Institute in the region and consistently striving to become leader in education in India apparently by maintaining its quality conventions. </p>
                                                         <p>&nbsp;</p>
                                                        
						</div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/DSCN1593.JPG" />
                                                        <a href="assets/images/DSCN1593.JPG" class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			<section class="full-width-section">
					<div class="full-image-container default-bg border-bottom-clear">
                                            <img class="to-right-block" src="assets/images/center/vibrant1.JPG" alt="">
						<div class="full-image-overlay text-right">
							
						</div>
					</div>
					<div class="full-text-container default-bg border-bottom-clear" style="background:#1da913 url('assets/images/bg/overlay.png')">
						<h2 id="heading-font">Our Mission</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">To provide PSUs and Private Sector dynamic employees by imparting world-class Quality Education and to become premier institute in the India.</p>
						<div class="separator-3 visible-lg"></div>
						
					</div>
				</section>
                     <section class="full-width-section">
					<div class="full-text-container bg-primary border-bottom-clear" style="background:#0d5995 url('assets/images/bg/overlay.png')">
                                            <h2 id="heading-font" style="color:white">Our Vision</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">Endowment of Quality Education to transform human life and the world forever.  </p>
						<div class="separator-3 visible-lg"></div>
						
					</div>
                                      <div class="full-image-container default-bg border-bottom-clear">
                                            <img class="to-right-block" src="assets/images/center/vibrant2.JPG" alt="">
						<div class="full-image-overlay text-right">
							
						</div>
					</div>
					
				</section>
			<section class="full-width-section">
                            <div class="full-image-container default-bg border-bottom-clear" >
                                            <img class="to-right-block" src="assets/images/center/vibrant1.JPG" alt="">
						<div class="full-image-overlay text-right">
							
						</div>
					</div>
					<div class="full-text-container default-bg border-bottom-clear" style="background:#d78600 url('assets/images/bg/overlay.png')">
						<h2 id="heading-font">Our Core Values</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">Here in <b>VIBRANT EDUCATION SERVICES</b>, we are committed not only to provide Quality Education but also to develop robust perception so that students can serve their employers at their best.  </p>
						<div class="separator-3 visible-lg"></div>
					</div>
				</section>
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
	</body>
</html>
