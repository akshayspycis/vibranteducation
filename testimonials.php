<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Students Feedbacks </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
 
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Testimonials</strong> </h1>
                                                    <div class="separator"></div>
                                                    <div class="media margin-clear" id="selFeedbacks" >
                                                    
                                                        
<!--                                                         <div class="testimonial" id="student-feedbacks">
                                                            <div class="testimonial-body" >
                                                                <div class="col-md-1">
                                                                    <div class="testimonial-image">
                                                                        <img src="assets/images/testimonials/Aman_Indian_BankPO.jpg" style="height:90px;width:90px;" alt="" title="" class="img-circle">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-11">
                                                                    <p id="cpara">Very good for banking students.This test series contains all the topic based questions especially designed on the current pattern and syllabus of PO/MT exam.</p>
                                                                <h5 class="text-center" id="heading-font">Aman Trivedi</h5>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                    </div>
					
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelFeedback.js"></script>
                <script type="text/javascript">
                   onLoadFeedback();
                </script>
	</body>
</html>
