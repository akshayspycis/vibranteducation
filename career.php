<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Career  |  Vibrant Education</title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
              <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header " onload="loadHTML('career')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">

				</div>
				<!-- breadcrumb end -->
				<div class="container">
					
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 ;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="container">
                                <div class="row" id="post_container">
                                    <h2 class="text-center heading-font" style="text-transform:none;">Current <strong> Openings </strong>  </h2>
                                    <div id="careers_details">
                                    <div class="separator"></div>
<!--                                    <div class="row" id="vacancies_post">
                                        <div class="col-md-2">
                                            <div  style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);margin-top:15px;">
                                                <img src="images/job.png" />
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <h3 class="heading-font">Hadoop Developer with 3+ years of experience</h3>
                                            <p><small>Posted on : <b>16 Dec 2016</b> </small>  </p>
                                            <p style="font-family:verdana">3+yrs experience in Bigdata Hadoop Development as well as in Production Support.
                                                
                                                Must Have working experience on Java related projects
                                                Must have hands on experience on followings – Map Reduce, Hbase, Hive,
                                                Spark, Scala, Oozie, Sqoop
                                                Good knowledge on Troubleshooting issues with data ingestion, management
                                                & transformation.
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>&nbsp;</p>
                                            <button class="btn btn-warning">Apply Now</button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="separator"></div>
                                    </div>-->
                                  </div>
                                
                                </div>
                                <p>&nbsp;</p>
                                <div class="separator"></div>
                                
                                <h2 class="text-center heading-font object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="text-transform:none;">Contact Us <strong>98-262-262-99</strong></h2>
                                <div class="separator"></div>
                                <p>&nbsp;</p>
                            </div>
                        </section>
			   
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		 <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
	</body>
        <script type="text/javascript">
            career={};
           $(document).ready(function(){
                 $.ajax({
                    type:"post",
                    url:"server/controller/SelCareers.php",
                    success: function(data) {
                        career={};
                       var duce = jQuery.parseJSON(data);
                        $('#careers_details').empty();
                        $.each(duce, function (index, article) {
                            career[article.career_id]={};
                            career[article.career_id]["heading"]=article.heading;
                            career[article.career_id]["content"]=article.content;
                            career[article.career_id]["city"]=article.city;
                            career[article.career_id]["date"]=article.date;
                            career[article.career_id]["posted_on"]=article.posted_on;
                            career[article.career_id]["image"]=article.image;
                            var img= "server/controller/"+article.image;
                            $('#careers_details').append($("<div>").addClass("row").attr({'id':'vacancies_post'})
                                            .append($("<div>").addClass("col-md-2")
                                                .append($("<div>").css({'box-shadow':'0 6px 10px rgba(0,0,0, 0.25)','margin-top':'15px'})
                                                        .append($("<img>").attr({'src':img,'height':'200','width':'200'}))
                                                       )
                                                   )
                                            .append($("<div>").addClass("col-md-8")
                                                .append($("<h3>").addClass("heading-font")
                                                        .append(article.heading)
                                                       )
                                                .append($("<p>")
                                                        .append($("<small>").append("Posted on : ").append($("<b>").append(article.date)))
                                                       )
                                                .append($("<p>").css({'font-family':'verdana','text-align':'justify'})
                                                        .append(article.content)
                                                       )
                                               )
                                            .append($("<div>").addClass("col-md-2")
                                                .append($("<p>"))
                                                .append($("<button>").addClass("btn btn-warning")
                                                    .append("Apply Now")
                                                    .click(function (){
                                                        showApplyModel(article.career_id)
                                                     }))
                                                )
                                            .append($("<div>").addClass("clearfix"))
                                    )
                        });
                    }
                });
          });  
         function showApplyModel (career_id){
                    $("#demoPopUp").modal("show");
                    $("#demoPopUp").find("form").each(function(){
                            this.reset();
                    });
                    $("#demoPopUp").find("#career_id").val(career_id);
                }
        </script>

        	<!-- header-container start -->
			<?php include 'includes/demo-popup.php' ;  ?>
                 <script src="ajax/SelCareers.js"></script>
</html>

