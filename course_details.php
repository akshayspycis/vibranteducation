<?php include 'includes/session.php'; ?>    
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
            <meta charset="utf-8">
            <title>Course Details  |  Vibrant Education</title>
            <meta name="title" content="Big Data Hadoop Online Training | Hadoop Certification Course | Prwatech" />
            <meta name="keywords" content="hadoop training, online hadoop training, hadoop training classes, hadoop course online, big data training, big data course, big data online course, hadoop tutorial, HDFS training, Yarn training, MapReduce training, Pig training, Hive training, HBase training" />
            <meta name="description" content="Our Big Data Hadoop Certification Training helps you master HDFS, Yarn, MapReduce, Pig, Hive, HBase with use cases on Retail, Social Media, Aviation, Finance, Tourism domain" />
            <meta name="description" content="PrwaTech provides big data Hadoop Training Classes for beginners and developers with versatile carrier options. Know more about courses visit our website.">
            <meta name="author" content="prwatech">
            <meta name="google-site-verification" content="fSzBVSEUMu0l5lnD0qBVGv5F_16zaI6xiUZFMm-iMqQ" /> 
              <?php include 'includes/csslinks.php';?>
                <style>
                    body p{
                        text-align:justify;
                    }
                </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header ">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/page-course-banner-1.jpg'); background-position: 50% 27%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					
				</div>
				<!-- breadcrumb end -->
				
			</div>
			<div id="page-start"></div>
                        <section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                     <div class="row">
                                         <div class="col-md-8 col-md-offset-2" id="blog_details">
                                            
                                         </div>
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
			<div class="clearfix"></div>
			
			<!-- ================ -->
		
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		<?php include 'includes/jslinks.php';?>
                
		<!-- Color Switcher End -->
                <script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
	</body>

        <script>
             $(document).ready(function(){
                var cta_id="";
                   if(urlParam('course_details_id')==null){
                    window.location="index.php"
                   }else{
                        onlad(urlParam('course_details_id'));
                   }
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
  function onlad(course_details_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelClientCouserDetailsFull.php",
                    data:{'course_details_id':course_details_id},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
                       var duce = jQuery.parseJSON(data);
                      $("#blog_details").empty();
                        $.each(duce, function (index, article) {
                            blog[article.course_details_id]=article;
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                                $("#blog_details").append($("<div>").addClass("timeline-item")
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                        .append($("<header>")
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        )
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img}).css({'width':'100%','height':'250px'}))
                                                        )
                                                    .append($("<p>&nbsp;</p>"))
                                                    .append($("<div>").addClass("blogpost-content").append(decodeURIComponent(article.long_description)))
                                                ));
                           }
                          
                        });
                    }
                });
                }
        </script>
</html>



